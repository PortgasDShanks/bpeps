-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 07, 2020 at 10:42 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpeps_2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cake`
--

CREATE TABLE `tbl_cake` (
  `cake_id` int(11) NOT NULL,
  `cake_img` varchar(50) NOT NULL DEFAULT '',
  `cake_name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cake`
--

INSERT INTO `tbl_cake` (`cake_id`, `cake_img`, `cake_name`) VALUES
(1, '4.jpeg', 'wonderful cake'),
(2, '2.jpg', 'wonderful cake 2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cake_sizes`
--

CREATE TABLE `tbl_cake_sizes` (
  `size_id` int(11) NOT NULL,
  `size` varchar(10) NOT NULL DEFAULT '',
  `cake_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_cake_sizes`
--

INSERT INTO `tbl_cake_sizes` (`size_id`, `size`, `cake_id`) VALUES
(1, '6x9', 2),
(2, '6x9', 2),
(3, '9x6', 1),
(4, '9x7', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_design_colors`
--

CREATE TABLE `tbl_design_colors` (
  `color_id` int(11) NOT NULL,
  `design_id` int(11) NOT NULL,
  `color` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_design_colors`
--

INSERT INTO `tbl_design_colors` (`color_id`, `design_id`, `color`) VALUES
(2, 1, '#1a29ff'),
(3, 1, '#ffdd00'),
(4, 1, '#0074f0'),
(5, 2, '#0ee11c'),
(6, 2, '#efff0f');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entertainment`
--

CREATE TABLE `tbl_entertainment` (
  `ent_id` int(11) NOT NULL,
  `ent_type` varchar(50) NOT NULL DEFAULT '',
  `ent_fee` decimal(12,3) NOT NULL,
  `ent_hrs` int(11) NOT NULL,
  `ent_add_fee` decimal(12,3) NOT NULL,
  `ent_add_fee_hrs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_entertainment`
--

INSERT INTO `tbl_entertainment` (`ent_id`, `ent_type`, `ent_fee`, `ent_hrs`, `ent_add_fee`, `ent_add_fee_hrs`) VALUES
(1, 'Clowns', '4000.000', 1, '1.000', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equipments`
--

CREATE TABLE `tbl_equipments` (
  `equipment_id` int(11) NOT NULL,
  `equip_name` varchar(100) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_equipments`
--

INSERT INTO `tbl_equipments` (`equipment_id`, `equip_name`, `cat_id`, `date_added`, `added_by`) VALUES
(1, 'Fork', 1, '2020-09-29 21:31:43', 1),
(2, 'Tables', 2, '2020-10-07 13:39:38', 1),
(3, 'chair', 2, '2020-10-07 13:42:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equipment_category`
--

CREATE TABLE `tbl_equipment_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_equipment_category`
--

INSERT INTO `tbl_equipment_category` (`cat_id`, `cat_name`) VALUES
(1, 'test'),
(2, 'furnitures');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equipment_designs`
--

CREATE TABLE `tbl_equipment_designs` (
  `design_id` int(11) NOT NULL,
  `design_img` varchar(50) NOT NULL DEFAULT '',
  `design_name` varchar(50) NOT NULL DEFAULT '',
  `design_desc` text NOT NULL,
  `equipment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_equipment_designs`
--

INSERT INTO `tbl_equipment_designs` (`design_id`, `design_img`, `design_name`, `design_desc`, `equipment_id`) VALUES
(1, 'c.jpg', 'chair 1', 'asdasdas', 1),
(2, 'i.jpg', 'test', 'sadasdas', 1),
(3, 'e.jpg', 'Simple Table 1', 'asdasdas', 2),
(4, 'g.jpg', 'Simple chair 1', 'dasdasdas', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_freebies`
--

CREATE TABLE `tbl_freebies` (
  `fb_id` int(11) NOT NULL,
  `fb_name` varchar(100) NOT NULL DEFAULT '',
  `fb_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_freebies`
--

INSERT INTO `tbl_freebies` (`fb_id`, `fb_name`, `fb_img`) VALUES
(3, 'asdasdasdas', 'c.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_giveaways`
--

CREATE TABLE `tbl_giveaways` (
  `giveaway_id` int(11) NOT NULL,
  `giveaway_name` varchar(50) NOT NULL DEFAULT '',
  `giveaway_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_giveaways`
--

INSERT INTO `tbl_giveaways` (`giveaway_id`, `giveaway_name`, `giveaway_img`) VALUES
(2, 'Giveaway 1', 'i.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invitational_cards`
--

CREATE TABLE `tbl_invitational_cards` (
  `ic_id` int(11) NOT NULL,
  `ic_name` varchar(50) NOT NULL DEFAULT '',
  `ic_image` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_invitational_cards`
--

INSERT INTO `tbl_invitational_cards` (`ic_id`, `ic_name`, `ic_image`) VALUES
(3, 'Invitational card 2', 'd.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_content` text NOT NULL,
  `message_datetime` datetime NOT NULL,
  `message_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_messages`
--

INSERT INTO `tbl_messages` (`message_id`, `sender_id`, `receiver_id`, `message_content`, `message_datetime`, `message_status`) VALUES
(28, 5, 1, 'rwsrasdas das das das das', '2020-09-21 23:10:00', 0),
(29, 1, 5, 'rwsrasdas das das das das', '2020-09-21 23:10:00', 0),
(30, 5, 1, 'dasdasdasdasdas', '2020-09-21 23:22:16', 0),
(31, 5, 1, 'asdasdasdasdsa', '2020-09-21 23:22:20', 0),
(32, 5, 1, 'asdasdasdasdas', '2020-09-21 23:22:31', 0),
(33, 5, 1, 's ada dsa das das dasdas d', '2020-09-21 23:22:43', 0),
(34, 1, 5, 'SDASDASDASDAS', '2020-09-21 23:40:48', 0),
(35, 5, 1, 'Test Message', '2020-09-21 23:47:07', 0),
(36, 5, 1, 'dsadasdas', '2020-09-30 16:12:28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_details`
--

CREATE TABLE `tbl_package_details` (
  `package_detail_id` int(11) NOT NULL,
  `package_header_id` int(11) NOT NULL,
  `package_item` int(11) NOT NULL,
  `item_qntty` int(11) NOT NULL,
  `color` varchar(20) NOT NULL DEFAULT '',
  `cat` varchar(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_package_details`
--

INSERT INTO `tbl_package_details` (`package_detail_id`, `package_header_id`, `package_item`, `item_qntty`, `color`, `cat`) VALUES
(6, 2, 1, 450, '', 'I'),
(7, 2, 2, 30, '', 'G'),
(8, 2, 3, 30, '', 'IC'),
(9, 2, 3, 1, '', 'C'),
(10, 2, 3, 1, '', 'F'),
(11, 2, 2, 1, '', 'E'),
(12, 3, 1, 520, '', 'I'),
(13, 3, 0, 30, '', 'G'),
(14, 3, 0, 30, '', 'IC'),
(15, 3, 0, 1, '', 'C'),
(16, 3, 0, 1, '', 'F'),
(17, 3, 0, 1, '', 'E'),
(18, 4, 1, 20, '2', 'I');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_header`
--

CREATE TABLE `tbl_package_header` (
  `package_header_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `package_desc` text NOT NULL,
  `package_venue` int(11) NOT NULL,
  `package_price` decimal(12,3) NOT NULL,
  `package_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_package_header`
--

INSERT INTO `tbl_package_header` (`package_header_id`, `theme_id`, `package_desc`, `package_venue`, `package_price`, `package_img`) VALUES
(2, 1, 'sadasdas', 1, '150000.000', 'h.jpg'),
(3, 1, 'asdasdsa', 1, '190000.000', 'c.jpg'),
(4, 1, 'sadasdas', 1, '1312312.000', 'h.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pricing`
--

CREATE TABLE `tbl_pricing` (
  `price_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price_amount` decimal(12,3) NOT NULL,
  `item_cat` varchar(2) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pricing`
--

INSERT INTO `tbl_pricing` (`price_id`, `item_id`, `price_amount`, `item_cat`) VALUES
(1, 1, '50.000', 'E'),
(2, 2, '20.000', 'E'),
(3, 3, '50.000', 'E'),
(4, 4, '20.000', 'E'),
(5, 1, '10000.000', 'V'),
(6, 2, '10.000', 'I'),
(7, 3, '10.000', 'I'),
(8, 2, '10.000', 'G'),
(9, 3, '10.000', 'G'),
(10, 3, '1000.000', 'C'),
(11, 4, '2000.000', 'C'),
(12, 1, '2000.000', 'C'),
(13, 2, '2000.000', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_themes`
--

CREATE TABLE `tbl_themes` (
  `theme_id` int(11) NOT NULL,
  `theme_name` varchar(100) NOT NULL DEFAULT '',
  `theme_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_themes`
--

INSERT INTO `tbl_themes` (`theme_id`, `theme_name`, `theme_img`) VALUES
(1, 'Theme 2', 'a.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactions`
--

CREATE TABLE `tbl_transactions` (
  `trans_id` int(11) NOT NULL,
  `ref_number` varchar(20) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `trans_time_from` time NOT NULL,
  `trans_time_to` time NOT NULL,
  `venue` int(11) NOT NULL,
  `theme` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `mode_of_payment` varchar(20) NOT NULL DEFAULT '',
  `is_package` int(1) NOT NULL,
  `package_id` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '',
  `total_amount` decimal(12,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transactions`
--

INSERT INTO `tbl_transactions` (`trans_id`, `ref_number`, `user_id`, `trans_date`, `trans_time_from`, `trans_time_to`, `venue`, `theme`, `date_added`, `mode_of_payment`, `is_package`, `package_id`, `status`, `total_amount`) VALUES
(1, ' REF-20200922223454', 5, '2020-08-30', '17:00:00', '22:00:00', 1, 0, '2020-09-22 22:35:19', '', 0, 0, 'C', '100000.000'),
(2, ' REF-20200922223651', 5, '2020-08-30', '13:00:00', '17:00:00', 1, 0, '2020-09-22 22:37:18', '', 0, 0, 'F', '250000.000'),
(3, ' REF-20200922223743', 5, '2020-07-30', '13:00:00', '17:00:00', 1, 0, '2020-09-22 22:38:03', '', 0, 0, 'F', '30000.000'),
(4, ' REF-20200922223835', 5, '2020-07-30', '13:00:00', '17:00:00', 1, 0, '2020-09-22 22:38:56', '', 0, 0, 'F', '240000.000'),
(5, ' REF-20200922224225', 5, '2020-09-30', '13:00:00', '17:00:00', 1, 0, '2020-09-22 22:42:49', '', 0, 0, 'F', '50000.000'),
(6, ' REF-20200922224623', 5, '2020-09-30', '13:00:00', '17:00:00', 1, 0, '2020-09-22 22:46:36', '', 0, 0, 'F', '60000.000'),
(7, ' REF-20200922225235', 5, '2020-10-30', '13:00:00', '17:00:00', 1, 0, '2020-09-22 22:53:16', 'Bank', 0, 0, 'F', '26000.000'),
(8, ' REF-20200929211804', 5, '2020-10-30', '13:00:00', '17:00:00', 0, 0, '2020-09-29 21:18:25', '', 0, 0, 'F', '27000.000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_detail`
--

CREATE TABLE `tbl_transaction_detail` (
  `trans_detail_id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `quantity` decimal(12,3) NOT NULL,
  `total` decimal(12,3) NOT NULL,
  `arrival_time_from` time NOT NULL,
  `arrival_from_to` time NOT NULL,
  `item_cat` varchar(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transaction_detail`
--

INSERT INTO `tbl_transaction_detail` (`trans_detail_id`, `trans_id`, `item_id`, `price`, `quantity`, `total`, `arrival_time_from`, `arrival_from_to`, `item_cat`) VALUES
(1, 6, 1, '2000.000', '2.000', '4000.000', '00:00:00', '00:00:00', 'C'),
(2, 7, 1, '50.000', '100.000', '5000.000', '00:00:00', '00:00:00', 'E'),
(3, 7, 1, '2000.000', '1.000', '2000.000', '00:00:00', '00:00:00', 'C'),
(4, 7, 2, '3000.000', '1.000', '3000.000', '14:00:00', '17:00:00', 'ET');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `contact_no` varchar(11) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `category` varchar(2) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `firstname`, `middlename`, `lastname`, `contact_no`, `address`, `email`, `username`, `password`, `category`, `is_active`) VALUES
(1, 'admin', ' ', 'admin', ' ', ' ', ' ', 'admin', 'a', '0', 0),
(2, 'Natsu', 'Tobongbanua', 'Dragneel', '1234123', 'Brgy. Tangub Bacolod City', 'a@gmail.com', '', '12345', '1', 0),
(3, 'Natsu', 'Tobongbanua', 'Dragneel', '1234123', 'Brgy. Tangub Bacolod City', 'a@gmail.com', '', '12345', '1', 0),
(4, 'Natsu', 'Tobongbanua', 'Dragneel', '1234123', 'Brgy. Tangub Bacolod City', 'a@gmail.com', 'gray', '12345', '1', 0),
(5, 'Gray', 'Tobongbanua', 'Fullbuster', '3123123', 'Brgy. Tangub Bacolod City', 'sd@asasd', 'gray', 'a', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_venue`
--

CREATE TABLE `tbl_venue` (
  `venue_id` int(11) NOT NULL,
  `venue_name` varchar(50) NOT NULL DEFAULT '',
  `venue_address` text NOT NULL,
  `latitude` varchar(30) NOT NULL DEFAULT '',
  `longitude` varchar(30) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_venue`
--

INSERT INTO `tbl_venue` (`venue_id`, `venue_name`, `venue_address`, `latitude`, `longitude`) VALUES
(1, 'L\'Fisher Hotels', '14 Lacson St. Bacolod City, 6100 Negros Occidentals', '10.6794712', '122.9544351');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cake`
--
ALTER TABLE `tbl_cake`
  ADD PRIMARY KEY (`cake_id`);

--
-- Indexes for table `tbl_cake_sizes`
--
ALTER TABLE `tbl_cake_sizes`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `tbl_design_colors`
--
ALTER TABLE `tbl_design_colors`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `tbl_entertainment`
--
ALTER TABLE `tbl_entertainment`
  ADD PRIMARY KEY (`ent_id`);

--
-- Indexes for table `tbl_equipments`
--
ALTER TABLE `tbl_equipments`
  ADD PRIMARY KEY (`equipment_id`);

--
-- Indexes for table `tbl_equipment_category`
--
ALTER TABLE `tbl_equipment_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_equipment_designs`
--
ALTER TABLE `tbl_equipment_designs`
  ADD PRIMARY KEY (`design_id`);

--
-- Indexes for table `tbl_freebies`
--
ALTER TABLE `tbl_freebies`
  ADD PRIMARY KEY (`fb_id`);

--
-- Indexes for table `tbl_giveaways`
--
ALTER TABLE `tbl_giveaways`
  ADD PRIMARY KEY (`giveaway_id`);

--
-- Indexes for table `tbl_invitational_cards`
--
ALTER TABLE `tbl_invitational_cards`
  ADD PRIMARY KEY (`ic_id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `tbl_package_details`
--
ALTER TABLE `tbl_package_details`
  ADD PRIMARY KEY (`package_detail_id`);

--
-- Indexes for table `tbl_package_header`
--
ALTER TABLE `tbl_package_header`
  ADD PRIMARY KEY (`package_header_id`);

--
-- Indexes for table `tbl_pricing`
--
ALTER TABLE `tbl_pricing`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `tbl_themes`
--
ALTER TABLE `tbl_themes`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  ADD PRIMARY KEY (`trans_id`);

--
-- Indexes for table `tbl_transaction_detail`
--
ALTER TABLE `tbl_transaction_detail`
  ADD PRIMARY KEY (`trans_detail_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_venue`
--
ALTER TABLE `tbl_venue`
  ADD PRIMARY KEY (`venue_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cake`
--
ALTER TABLE `tbl_cake`
  MODIFY `cake_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_cake_sizes`
--
ALTER TABLE `tbl_cake_sizes`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_design_colors`
--
ALTER TABLE `tbl_design_colors`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_entertainment`
--
ALTER TABLE `tbl_entertainment`
  MODIFY `ent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_equipments`
--
ALTER TABLE `tbl_equipments`
  MODIFY `equipment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_equipment_category`
--
ALTER TABLE `tbl_equipment_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_equipment_designs`
--
ALTER TABLE `tbl_equipment_designs`
  MODIFY `design_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_freebies`
--
ALTER TABLE `tbl_freebies`
  MODIFY `fb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_giveaways`
--
ALTER TABLE `tbl_giveaways`
  MODIFY `giveaway_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_invitational_cards`
--
ALTER TABLE `tbl_invitational_cards`
  MODIFY `ic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `tbl_package_details`
--
ALTER TABLE `tbl_package_details`
  MODIFY `package_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_package_header`
--
ALTER TABLE `tbl_package_header`
  MODIFY `package_header_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_pricing`
--
ALTER TABLE `tbl_pricing`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_themes`
--
ALTER TABLE `tbl_themes`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_transaction_detail`
--
ALTER TABLE `tbl_transaction_detail`
  MODIFY `trans_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_venue`
--
ALTER TABLE `tbl_venue`
  MODIFY `venue_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
