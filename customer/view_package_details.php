<?php 
$id = $_GET['id'];
$user_id = $_SESSION['cust_user_id'];

$getDetails = mysql_fetch_array(mysql_query("SELECT * FROM `tbl_booking_cart` as c, tbl_transactions as t WHERE c.transaction_id = t.trans_id AND t.user_id = '$user_id' AND t.trans_id = '$id' AND item_type = ''"));

$is_package = $getDetails['is_package'];
$item_id = $getDetails['item_id'];
$item_type = $getDetails['item_type'];
$total_amount = $getDetails['total_amount'] + 3000;

$sql = "SELECT * FROM tbl_package_header as p, tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.theme_id = t.theme_id AND p.package_header_id = '$item_id' AND p.package_header_id = h.package_header_id AND p.package_venue = v.venue_id";

$package_details = mysql_fetch_array(mysql_query($sql));
$venue_stat = ($getDetails['venue_status'] == 0)?'<span style="color: orange">Pending</span>':(($getDetails['venue_status'] == 1)?'<span style="color: green">Approved</span>':'<span style="color: red">N/A</span>');
?>
<div class="col-md-12 animated flipInX" style="margin-top: 10px;">
   <h4>Transaction Details</h4>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<div class="col-md-6 animated slideInLeft" style="padding: 10px">
	<img src="../assets/images/<?=$package_details['package_img']?>" style='width: 100%;border-radius: 5%'>
</div>
<div class="col-md-6 animated slideInLeft" style="padding: 10px">
	<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
        Package Details</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse in">
      <div class="panel-body">
      	<table class="table table-bordered">
		<tbody>
			<tr>
				<td>Theme Name</td>
				<td><?=$package_details['theme_name']?></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><?=$package_details['package_desc']?></td>
			</tr>
			<tr>
				<td>Venue</td>
				<td><?=$package_details['venue_name']?> / <?=$venue_stat?></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><?=$package_details['package_price']?></td>
			</tr>
		</tbody>
	</table>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
        Inclusions</a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">
      	<label><h6>Equipments</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_equipment_designs as d WHERE h.package_item = d.design_id AND package_header_id = '$item_id' AND cat = 'I'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['design_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Invitational Cards</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_invitational_cards as d WHERE h.package_item = d.ic_id AND package_header_id = '$item_id' AND cat = 'IC'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['ic_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Giveaways</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_giveaways as d WHERE h.package_item = d.giveaway_id AND package_header_id = '$item_id' AND cat = 'G'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['giveaway_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Cakes</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
					<td>Size</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_cake as d, tbl_cake_sizes as s WHERE h.package_item = s.size_id AND package_header_id = '$item_id' AND cat = 'C' AND d.cake_id = s.cake_id");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['cake_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
					<td><?=$equip_row['size']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>freebies</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_freebies as d WHERE h.package_item = d.fb_id AND package_header_id = '$item_id' AND cat = 'F'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['fb_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Entertainments</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_entertainment as d WHERE h.package_item = d.ent_id AND package_header_id = '$item_id' AND cat = 'E'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['ent_type']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
      </div>
    </div>
  </div>
</div>
</div>

<!-- ADDITIONAL -->
<div class="col-md-12 animated slideInLeft" >
	<h4 style="text-align: center;">Additional Items</h4>
	<div class="row" style="padding: 10px">
	<?php 
	$additional = mysql_query("SELECT * FROM `tbl_booking_cart` as c, tbl_transactions as t WHERE c.transaction_id = t.trans_id AND t.user_id = '$user_id' AND t.trans_id = '$id' AND item_type != ''");
	$count = mysql_num_rows($additional);
	if($count > 0){
		while($addition_row = mysql_fetch_array($additional)){
			$item_id = $addition_row['item_id'];
			$query = ($addition_row['item_type'] == 'I')?"SELECT design_img,design_name,design_desc FROM tbl_equipment_designs WHERE design_id = '$item_id'":(($addition_row['item_type'] == 'IC')?"SELECT ic_image,ic_name FROM tbl_invitational_cards WHERE ic_id = '$item_id'":(($addition_row['item_type'] == 'G')?"SELECT giveaway_img,giveaway_name FROM tbl_giveaways WHERE giveaway_id = '$item_id'":"SELECT cake_img,cake_name,size FROM tbl_cake as c, tbl_cake_sizes as s WHERE c.cake_id = s.cake_id AND s.size_id = '$item_id'"));

			$sql = mysql_fetch_array(mysql_query($query));

			$desc = ($addition_row['item_type'] == 'I')?$sql[2]:"";
			$size = ($addition_row['item_type'] == 'C')?$sql[2]:"";
	?>
		<div class="col-md-6" style="border: 1px solid #a2a2a26e;padding: 10px;">
			<div class="row">
				<div class="col-md-5">
					<img src="../assets/images/<?=$sql[0]?>" style='width: 100%;border-radius: 5%;    height: 100px;object-fit: contain;'>
				</div>
				<div class="col-md-7">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Name</td>
								<td><?=$sql[1]?></td>
							</tr>
							<tr>
								<td>Description</td>
								<td><?=$desc?></td>
							</tr>
							<tr>
								<td>Price</td>
								<td><?=$addition_row['item_price']?></td>
							</tr>
							<tr>
								<td>Size</td>
								<td><?=$size?></td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td>
									<div class="input-group">
										<div class="input-group-prepend">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"minus")' class="fa fa-minus" style='cursor: pointer;'></span></span>
										  </div>
										  <input type="number" id="itemqntty" value="<?=$addition_row['item_qntty']?>" readonly class="form-control">
										  <div class="input-group-append">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"add")' class="fa fa-plus" style='cursor: pointer;'></span></span>
										  </div>
									</div>
								</td>
							</tr>
							<tr>
								<td>Total</td>
								<td><?=$addition_row['item_price']*$addition_row['item_qntty']?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	
		<?php } ?>
	</div>
	<?php }else{ ?>
		<div class="col-md-12">
			<h5 style=""> No Additional Items Added. <span style="text-decoration: underline;cursor: pointer;" onclick='window.location="index.php?view=add-additional-items"'>Do you want to add?</span> </h5>
		</div>
	<?php } ?>
		<div class="col-md-12">
			<div class="alert alert-success">
				<h6>Package Price: &#8369; <?=number_format($package_details['package_price'], 2)?></h6>
				<h6>Additional Item Price: &#8369; <?=number_format(getTotalPrice_additional($id), 2)?></h6>
				<h6>Package Price: &#8369; <?=number_format(3000, 2)?></h6>
				<hr>
				<h6>Total Price: &#8369; <?=number_format($total_amount, 2)?></h6>
			</div>
		</div>
</div>