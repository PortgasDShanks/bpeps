<?php
	$receiver = mysql_fetch_array(mysql_query("SELECT user_id FROM tbl_users WHERE category = 0"));
?>
<style>
	    .cg :hover {
      background-color: #80868e;
  }

*,
*::before,
*::after {
  margin: 0;
  border: 0;
  padding: 0;
  word-wrap: break-word;
  box-sizing: border-box;
}

.message-sent,
.message-received {
  clear: both;
}
.message-sent::before,
.message-received::before,
.message-sent::after,
.message-received::after {
  content: '';
  display: table;
}

[class^='grid-'] {
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
  -webkit-flex-wrap: wrap;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}
[class^='grid-'] {
  -webkit-flex-direction: row;
  -ms-flex-direction: row;
  flex-direction: row;
}
.grid-message >[class^='col-'] {
  margin-top: 1em;
  margin-right: 1em;
}
.grid-message >[class^='col-']:nth-child(-n + 1) {
  margin-top: 0;
}
.grid-message >[class^='col-']:nth-child(1n) {
  margin-right: 0;
}
.col-message-sent {
  margin-left: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-received {
  margin-right: calc(8.33333333% + 0.08333333em) !important;
}
.col-message-sent,
.col-message-received {
  width: calc(91.66666667% - 0.08235677em);
}
.message-sent,
.message-received {
  margin-top: 0.0625em;
  margin-bottom: 0.0625em;
  padding: 0.25em 1em;
}
.message-sent p,
.message-received p {
  margin: 0;
  line-height: 1.5;
}
.message-sent {
  float: right;
  color: white;
  background-color: gray;/*dodgerblue;*/
  border-radius: 1em 0.25em 0.25em 1em;
}
.message-sent:first-child {
  border-radius: 1em 1em 0.25em 1em;
}
.message-sent:last-child {
  border-radius: 1em 0.25em 1em 1em;
}
.message-sent:only-child {
  border-radius: 1em;
}
.message-received {
  float: left;
  color: black;
  background-color: #c5c5c5;
  border-radius: 0.25em 1em 1em 0.25em;
}
.message-received:first-child {
  border-radius: 1em 1em 1em 0.25em;
}
.message-received:last-child {
  border-radius: 0.25em 1em 1em 1em;
}
.message-received:only-child {
  border-radius: 1em;
}
.col-message-sent {
  margin-top: 0.25em !important;
}
.col-message-received {
  margin-top: 0.25em !important;
}
.message {
  min-height: 53.33203125em;
  max-width: 30em; /* !!!!!!!!!! COMMENT OUT THIS LINE TO MAKE IT FULL WIDTH !!!!!!!!!! */
  display: -webkit-flex;
  display: -ms-flexbox;
  display: flex;
}
.btn_hover:hover{
  background-color: #80868e;
  color: white;
}
.fstMultipleMode.fstActive, .fstMultipleMode.fstActive .fstResults {
    box-shadow: 0 0.2em 0.2em rgba(0, 0, 0, 0.1);
    border-radius: 5px 5px 5px 5px;
    width: 77%;
}
.fstElement {
    display: inline-block;
    position: relative;
    border: 1px solid #D7D7D7;
    box-sizing: border-box;
    color: #232323;
    font-size: 1.1em;
    background-color: #fff;
    border-radius: 5px 5px 5px 5px;
    width: 77%;
  }
  .fstResults{
        width: 100%;
  }
  .fs-wrap{
    width: 77%;
  }
  .fs-label-wrap{
    height: 38px;
  }
  .fs-dropdown{
    width: 77%;
  }
  .fs-label-wrap .fs-label {
    padding: 10px 22px 11px 15px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    text-align: center;
  }
</style>
<div class="transactions jarallax" id="transactions">
		<h3 class="heading-agileinfo" data-aos="zoom-in">Transactions<span class="thr">Events Is A Professionally Managed Event</span></h3>
	<div class="container" data-aos="fade-right">
		<div class='row'>
			<div class='col-sm-12' style='box-shadow: 2px 3px 7px 2px #e708c169'>
				<div class="tab">
					<button class="tablinks" onclick="openCity(event, 'transact')" id="defaultOpen">Transactions</button>
					<button class="tablinks" onclick="openCity(event, 'notif')">Messages</button>
					<!-- <button class="tablinks" onclick="openCity(event, 'profile')">Profile</button> -->
					</div>

					<div id="transact" class="tabcontent">
						<div class='row'>
							<div class='col-sm-12'>
							<div class='col-sm-12' style='padding-top: 10px'>
								<button class='btn btn-sm btn-primary pull-right' data-toggle="modal" data-target="#transact_cust" class="scroll"><span class='fa fa-ticket'></span> Book Event </button>
							</div>
							<div class='col-sm-12' style='margin-top: 10px;'>
								<table id='bookings' class="table" style='margin-top:10px;width: 100%;'>
									<thead>
										<tr>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>REFERENCE #</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>DATE</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>TIME FROM</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>TIME TO</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>TYPE</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>DATE ADDED</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>VENUE STATUS</th>
											<th style='background-color: rgb(34 45 50);color: #ffffff;'>STATUS</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>

							</div>
						</div>
					</div>

					<div id="notif" class="tabcontent">
						<div class="col-md-12" style='margin-top: 15px;'>
							<div class="" style="height:409px;padding: 0px;border: 1px solid #ccc;">
								<!-- DIRECT CHAT -->
								<div class="box box-warning direct-chat direct-chat-warning" style="height: 100%;border-top: 0px;">
								<!-- /.box-header -->
								<div class="box-body" style="height: 100%;">
									<div class='col-md-12' style="background-color: #c5c5c5;"><center><label id='names'></label></center></div>
									<!-- Conversations are loaded here -->
									<div class="grid-message" style="overflow-x: auto;height: 90%;padding: 10px;" id="msg">

									</div>
									<!--/.direct-chat-messages-->
								</div>
								<!-- /.box-body -->
								<div class="box-footer">
									<div class="input-group">
										<input type="hidden" id="sender_id" value="<?php echo $userID;?>">
										<input type="hidden" id="receiver_id" value='<?php echo $receiver[0]; ?>'>
										<input type="text" id="chatContent" row="3" name="message"  autocomplete="off" placeholder="Type a message ..." class="form-control" onclick='markedAllMessage()'>
										<span class="input-group-btn">
											
											<button type="button" class="btn btn-primary btn-flat" id="send_btn" onclick="sendMsg()">Send</button>
										</span>
									</div>
								</div>
								<!-- /.box-footer-->
								</div>
							</div>
						</div>
					</div>

					<!-- <div id="profile" class="tabcontent">
					<h3>Tokyo</h3>
					<p>Tokyo is the capital of Japan.</p>
				</div> -->
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready( function(){
		var receiver_id = $("#receiver_id").val();
		loadChat(receiver_id);
	});
	$(document).keypress(function(e) {
		if(e.which == 13) {
			sendMsg();
		}
		var receiver_id = $("#receiver_id").val();
		loadChat(receiver_id);
	});
	function scrolling(){
		var objDiv = document.getElementById("msg");
		objDiv.scrollTop = objDiv.scrollHeight;
	}
	function loadChat(r_id){
	// var interval = setInterval( function(){
		viewMsg(r_id);
		setTimeout(scrolling, 500);
		//}, 10000);
	}
	function viewMsg(rec_id){
		var sender_id = $("#sender_id").val();
		$.post('admin/ajax/getMessage.php', {
		rec_id: rec_id,
		sender_id:sender_id
		},
		function(data){

		var msg = JSON.parse(data);
		$("#msg").html(msg.msg_content);
		});
	}

	function sendMsg(){
		var sender_id = $("#sender_id").val();
		var receiver_id = $("#receiver_id").val();
		var chatContent = $("#chatContent").val();
		$("#send_btn").prop("disabled", true);
		$("#send_btn").html("<span class='fa fa-spin fa-spinner'></span>");
		if(sender_id == '' || receiver_id == '' || chatContent == ''){
		alert('Failed');
		}else{
		$.post('admin/ajax/send_message.php', {
		sender_id: sender_id,
		receiver_id: receiver_id,
		chatContent: chatContent
		}, function(data){
		if(data != 0){
			$("#chatContent").val("");
			loadChat(data);
		}else{
			alert('Failed');
		}
		setTimeout(scrolling, 10);
		$("#send_btn").prop("disabled", false);
		$("#send_btn").html("<span class=''></span> Send");
		});
		}
	}


	function updateScroll(){
		var element = document.getElementById("msg");
		element.scrollTop = element.scrollHeight;
	}
	// $(document).ready( function(){
		
	// 	$("#bookings").DataTable().destroy();
    //     $('#bookings').dataTable({
    //     	"processing":true, 
	// 		"ajax":{
    //         "url":"../admin/ajax/datatables/getAllTrans_customer.php",
    //         "dataSrc":"data"
	// 		},
	// 		"columns":[
	// 			{
	// 				"mRender": function(data,type,row){
	// 					return "<input type='checkbox' name='checkbox_equip' value='"+row.trans_id+"'>";		
	// 				}
	// 			},
	// 			{
	// 				"data":"count"
	// 			},
	// 			{
	// 				"data":"ref_number"
	// 			},
	// 			{
	// 				"data":"trans_date"
	// 			},
	// 			{
	// 				"data":"trans_time_from"
	// 			},
	// 			{
	// 				"data":"trans_time_to"
	// 			},
	// 			{
	// 				"data":"type"
	// 			},
	// 			{
	// 				"data":"date_added"
	// 			}
				
	// 		]   
    //     });
	// });
</script>