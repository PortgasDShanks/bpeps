<div class="footer">
	<div class="container">
		<div class="f-bg-w3l">
		<div class="col-md-4 w3layouts_footer_grid" data-aos="fade-right">
					<h2>Contact Information</h2>
					    <ul class="con_inner_text">
							<li><span class="fa fa-map-marker" aria-hidden="true"></span>Bacolod City, <label> Philippines.</label></li>
							<li><span class="fa fa-envelope-o" aria-hidden="true"></span> <a href="mailto:info@example.com">robz_cutie06@yahoo.com</a></li>
							<li><span class="fa fa-phone" aria-hidden="true"></span> (034) 704 2485 </li>
						</ul>

					<ul class="social_agileinfo">
						<li><a href="#" class="w3_facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" class="w3_twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" class="w3_instagram"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#" class="w3_google"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
				<!-- <div class="col-md-4 w3layouts_footer_grid" data-aos="fade-down">
					<h2>Subscribe Newsletter</h2>
					<p>By subscribing to our mailing list you will always get latest news from us.</p>
					<form action="#" method="post">
						<input type="email" name="Email" placeholder="Enter your email..." required="">
						<button class="btn1"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
						<div class="clearfix"> </div>
					</form>
				</div> -->
				<div class="col-md-4 w3layouts_footer_grid" data-aos="fade-left">
					<h3>Recent Events</h3>
					  <ul class="con_inner_text midimg">
						<!-- <li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g2.jpg" alt="" class="img-responsive" /></a></li>
					    <li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g3.jpg" alt="" class="img-responsive" /></a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g4.jpg" alt="" class="img-responsive" /></a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g5.jpg" alt="" class="img-responsive" /></a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g6.jpg" alt="" class="img-responsive" /></a></li>
					    <li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g7.jpg" alt="" class="img-responsive" /></a></li>
						 <li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g8.jpg" alt="" class="img-responsive" /></a></li>
						<li><a href="#" data-toggle="modal" data-target="#myModal"><img src="images/g1.jpg" alt="" class="img-responsive" /></a></li> -->
				     </ul>
					
				</div>
				<div class="clearfix"> </div>
			</div>
			</div>
			<p class="copyright" data-aos="">© 2020 Blu Party. All Rights Reserved | Design by <a href="" target="_blank">BPEPS Team</a></p>
	</div>