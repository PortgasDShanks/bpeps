<div class="modal about-modal fade" id="finalStep" tabindex="-1" role="dialog" data-backdrop='static'>
		<div class="modal-dialog modal-sm" style='width: 370px;' role="document">
			<div class="modal-content">
				<div class="modal-header"> 
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						<h5 class="modal-title">Finish Your Booking</h5>
				</div> 
				<div class="modal-body">
					<div class='row'>
                        <div class='col-md-12'>
							<div class='input-group'>
								<span class='input-group-addon'>
									Amount
								</span>
								<input type="number" readonly class='form-control' name="totalPaymnt" id="totalPaymnt">
							</div>
						</div>
						<div class='col-md-12' style='margin-top: 10px'>
							<div class='input-group'>
								<span class='input-group-addon'>
									Mode of Payment
								</span>
								<select name="mop" id="mop" class='form-control'>
                                    <option value=""> &mdash; Please Choose &mdash; </option>
                                    <option value="Gcash"> Gcash </option>
                                    <option value="Bank"> Bank </option>
                                    <option value="Cash"> Cash </option>
                                </select>
							</div>
						</div>	
						<br>
						<div class='col-md-12' style='margin-top: 10px'>
							<input onchange='checkAgreement()' type="checkbox" id='aggremnt'> I Agree 
						</div>
						<div class='col-md-12' style='margin-top: 10px'>
							<button type='button' disabled id='fnish' onclick="finishBooking()" class='btn btn-sm btn-primary pull-right'><span class='fa fa-check-circle'></span> Finish</button>
						</div>	
					</div>			
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->