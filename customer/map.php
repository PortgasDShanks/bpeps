<style type="text/css">
  .datepicker-inline{
    display: none !important;
  }
   #map {
        height: 600px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
</style>
<div class="event-time " id="packages">
    <div class="container">
        <h3 class="heading-agileinfo aos-init aos-animate" data-aos="zoom-in"> Available Venues <span>Events Is A Professionally Managed Event</span></h3>
        <div id="map"></div>
    </div>
</div>
<script type="text/javascript">
function initMap() {
    var locations = [<?php 
                        $getAlllocations = mysql_query("SELECT * FROM `tbl_venue`");
                        while($fetchrow = mysql_fetch_array($getAlllocations)){
                            $response = array();
                            $response[] = "<strong>Location: </strong>".$fetchrow["venue_address"]."<br><br>"."<strong>Venue Name: </strong> ".$fetchrow["venue_name"];
                            $response[] = $fetchrow["latitude"];
                            $response[] = $fetchrow["longitude"];
                            echo json_encode($response).",";
                        }
                        ?>];
    //alert(locations);
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      center: new google.maps.LatLng(10.6840,122.9563),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
    }
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback=initMap"></script>