<style type="text/css">
   #map {
  height: 450px;;  /* The height is 400 pixels */
  width: 100%;  /* The width is the width of the web page */
 }
</style>
<div class="col-md-12 animated flipInX">
	<h5 class="card-title">Finish Booking</h5>
</div>
<div class="col-md-12 animated slideInLeft" style="margin-top: 10px;"></div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
  <div class="col-md-12 animated slideInLeft" style="padding-top: 10px;">
    <div class="row">
      <div class="col-md-12">
        <h6 class="alert alert-danger">Note: Selecting provided venue doesn't mean it's all done. We'll try to coordinate first with your selected venue if we can use it. We'll contact you through chat or we'll send you an update through email and sms</h6>
      </div>
      <div class="col-md-6" style="padding-top: 10px">
        <div class="input-group">
          <div class="input-group-prepend">
               <span class="input-group-text"> Reference # </span>
          </div>
          <input type="text" readonly value="BPEPS-<?=date("YmdHis", strtotime(getCurrentDate()))?>" id="refNum" class="form-control">
          
        </div>
      </div>
      <div class="col-md-6" style="padding-top: 10px">
        <div class="input-group">
          <div class="input-group-prepend">
              <span class="input-group-text"> Venue: </span>
          </div>
            <select class="form-control" id="venueSelect" onchange="getVenueSelector()">
              <option value="">&mdash; Please Choose &mdash; </option>
              <option value="p"> Use Provided Venues </option>
              <option value="o"> Use My Own Venue </option>
            </select>
        </div>
      </div>
      <div class="col-md-12" id="venue_cont">
        
      </div>
      <div class="col-md-6" style="padding-top: 10px">
        <div class="input-group">
          <div class="input-group-prepend">
               <span class="input-group-text"> Event Date </span>
          </div>
          <input type="date" id="eventDate" onchange='checkDateifAvail()' class="form-control">
          
        </div>
      </div>
      <input type="hidden" id="lat" name="">
      <input type="hidden" id="long" name="">
      <div class="col-md-6" style="padding-top: 10px">
        <div class="input-group">
          <div class="input-group-prepend">
               <span class="input-group-text"> Event Time </span>
          </div>
          <input type="time" id="eventtimeFrom" class="form-control">
            <div class="input-group-prepend">
               <span class="input-group-text"> to </span>
          </div>
          <input type="time" id="eventtimeTo" class="form-control">
        </div>
      </div>
      <div class="col-md-6" style="padding-top: 10px">
        <div class="input-group">
          <div class="input-group-prepend">
               <span class="input-group-text"> Payment Mode </span>
          </div>
          <select class="form-control" id="payment_mode">
            <option value="">&mdash; Please Choose &mdash; </option>
            <option value="Gcash"> Gcash </option>
            <option value="Remittance"> Remittance </option>
          </select>
        </div>
      </div>
      <input type="hidden" id="totalPricePackage" value="<?=getTotalPrice_package($userID)?>" name="">
      <div class="col-md-12" style="padding-top: 10px">
       <div class="alert alert-success">
          <h6>Venue Service Fee (If you use provided venue): &#8369; <span id="vPrice"></span></h6>
          
          <h6>Package Price: &#8369; <?=number_format(getTotalPrice_package($userID), 2)?></h6>
          <input type="hidden" id="totalPricePackage" value="<?=getTotalPrice_package($userID)?>" name=""> 
          <hr>
          <h6>Total Price: &#8369; <span id="totalAmount"></span></h6>
          <input type="hidden" id="totalAmout_save" name=""> 
        </div>
      </div>
      <div class="col-md-6" style="padding-top: 10px">
        <button class="btn btn-sm btn-success pull-right" onclick='window.location="index.php?view=view-package-cart"'><span class="fa fa-shopping-cart"></span> Go Back to Cart </button>
        <button class="btn btn-sm btn-success pull-right" id='finishedBook' onclick='finishBooking()'><span class="fa fa-check-circle"></span> Finish My Booking </button>
      
        
      </div>
    </div>
    
  </div>
</div>
<script type="text/javascript">
  function getVenuePrice(){
    var venueSelect = $("#venueSelect").val();
    var venueID = $("#venueID").val();
    var totalPricePackage = $("#totalPricePackage").val();
    $.post("../admin/ajax/getVenuePrice.php", {
      venueID: venueID
    }, function(data){
      var amount = data.split('-');
      $("#vPrice").text("3,000.00");
      $("#totalvPrice").val(3000);
      getTotalPrice(totalPricePackage,3000);
    });
  }
  function getTotalPrice(totalPricePackage,vservice){
    $.post("../admin/ajax/getFormattedTotal_package.php", {
      totalPricePackage: totalPricePackage,
      vservice: vservice
    }, function(data){
      var total_amnt = data.split('-');
      $("#totalAmount").text(total_amnt[0]);
      $("#totalAmout_save").val(total_amnt[1]);
    })
  }
  function getVenueSelector(){
    var venueSelect = $("#venueSelect").val();
    var totalPricePackage = $("#totalPricePackage").val();
    var serviceFee = $("#serviceFee").val();
    $.post("../admin/ajax/venueSelect.php", {
      venueSelect: venueSelect
    }, function(data){
      if(venueSelect == 'p'){
        getTotalPrice(totalPricePackage,3000);
        $("#lat").val("");
        $("#long").val("");
        $("#vPrice").text("3,000.00");
      }else{
        $("#venueID").val("");
        $("#vPrice").text("");
        $("#totalvPrice").val("");
        getTotalPrice(totalPricePackage,0);
        $("#vPrice").text("");
      }
     
      $("#venue_cont").html(data);
    })
  }
  function finishBooking(){
    var venueSelect = $("#venueSelect").val();
    var refNum = $("#refNum").val();
    var eventDate = $("#eventDate").val();
    var eventtimeFrom = $("#eventtimeFrom").val();
    var eventtimeTo = $("#eventtimeTo").val();
    var payment_mode = $("#payment_mode").val();
    var totalPricePackage = $("#totalAmout_save").val();
    var venue = (venueSelect == 'p') ? $("#venueID").val() : 0;

    var lat= $("#lat").val();
    var long = $("#long").val();
    $("#book").prop("disabled", true);
    $("#book").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("../admin/ajax/finishBooking.php", {
      refNum: refNum,
      eventDate: eventDate,
      eventtimeFrom: eventtimeFrom,
      eventtimeTo:eventtimeTo,
      payment_mode: payment_mode,
      totalPricePackage: totalPricePackage,
      venue: venue,
      lat:lat,
      long:long
    }, function(data){
      if(data > 0){
        swal({
              title: "All Good!",
              text: "Your Booking was successfully reserved, We will send sms and email for the status of your booking. Thank you!",
              type: "success"
          }, function(){
           window.location = 'index.php?view=transactions';
          });
      }else{
        swal("Something went wrong, Please try again later.");
      }
      
    })
  }
function viewBookingCart(){
    window.location = 'index.php?view=view-package-cart';
  }
  function checkDateifAvail(){
    var eventDate = $("#eventDate").val();
    $.post("../admin/ajax/checkdateAvailability.php", {
      eventDate: eventDate
    }, function(data){
      if(data < 3){
        swal({
              title: "All Good!",
              text: "Selected Date is Available",
              type: "success"
          }, function(){
           // window.location = 'index.php?view=transactions';
          });
      }else{
        swal({
          title: "Selected date is fully booked. If you still select this date, we can't assure you for accepting your reservation. Still Continue?",
          text: "",
          type: "info",
          showCancelButton: true,
          confirmButtonClass: "btn-primary",
          confirmButtonText: "Continue",
          cancelButtonText: "Cancel",
          closeOnConfirm: false,
          closeOnCancel: false
          },
          function(isConfirm) {
          if (isConfirm) {
              swal.close();
          } else {
              $("#eventDate").val("");
              swal.close();
          }
          });
      }
    })
     
  }
</script>