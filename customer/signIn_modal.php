<div class="modal about-modal fade" id="signIN" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-sm" role="document">
			<div class="modal-content">
				<div class="modal-header"> 
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						<h5 class="modal-title">Please Sign in</h5>
				</div> 
				<div class="modal-body">
					<div class='row'>
						<div class='col-md-12'>
							<div class='input-group'>
								<span class='input-group-addon'>
									<span class='fa fa-user'></span>
								</span>
								<input type="text" id='un_username' class='form-control' placeholder='Username'>
							</div>
						</div>	
						<br>
						<div class='col-md-12' style='margin-top: 10px'>
							<div class='input-group'>
								<span class='input-group-addon'>
									<span class='fa fa-key'></span>
								</span>
								<input type="password" id='pw_password' class='form-control' placeholder='Password'>
							</div>
						</div>
						<div class='col-md-12' style='margin-top: 10px'>
							<button type='button' onclick="signInUser()" class='btn btn-sm btn-primary pull-right'><span class='fa fa-check-circle'></span> Sign in</button>
						</div>	
					</div>			
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->