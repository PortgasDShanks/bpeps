<?php 
$id = $_GET['id'];
$details = mysql_fetch_array(mysql_query("SELECT * FROM tbl_transactions WHERE trans_id = '$id'"));
?>
<div class="col-md-12 animated flipInX" style="margin-top: 10px;">
   <h4>Transaction Details</h4>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>

<div class="col-md-12 animated slideInLeft" >
	<div class="row" style="padding: 10px">
	<?php 
	$additional = mysql_query("SELECT * FROM `tbl_booking_cart` as c, tbl_transactions as t WHERE c.transaction_id = t.trans_id AND t.user_id = '$userID' AND t.trans_id = '$id' AND item_type != ''");
		while($addition_row = mysql_fetch_array($additional)){
			$item_id = $addition_row['item_id'];
			$query = ($addition_row['item_type'] == 'I')?"SELECT design_img,design_name,design_desc FROM tbl_equipment_designs WHERE design_id = '$item_id'":(($addition_row['item_type'] == 'IC')?"SELECT ic_image,ic_name FROM tbl_invitational_cards WHERE ic_id = '$item_id'":(($addition_row['item_type'] == 'G')?"SELECT giveaway_img,giveaway_name FROM tbl_giveaways WHERE giveaway_id = '$item_id'":"SELECT cake_img,cake_name,size FROM tbl_cake as c, tbl_cake_sizes as s WHERE c.cake_id = s.cake_id AND s.size_id = '$item_id'"));

			$sql = mysql_fetch_array(mysql_query($query));

			$desc = ($addition_row['item_type'] == 'I')?$sql[2]:"";
			$size = ($addition_row['item_type'] == 'C')?$sql[2]:"";
	?>
		<div class="col-md-6" style="border: 1px solid #a2a2a26e;padding: 10px;">
			<div class="row">
				<div class="col-md-5">
					<img src="../assets/images/<?=$sql[0]?>" style='width: 100%;border-radius: 5%;    height: 100px;object-fit: contain;'>
				</div>
				<div class="col-md-7">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Name</td>
								<td><?=$sql[1]?></td>
							</tr>
							<tr>
								<td>Description</td>
								<td><?=$desc?></td>
							</tr>
							<tr>
								<td>Price</td>
								<td><?=$addition_row['item_price']?></td>
							</tr>
							<tr>
								<td>Size</td>
								<td><?=$size?></td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td>
									<div class="input-group">
										<div class="input-group-prepend">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"minus")' class="fa fa-minus" style='cursor: pointer;'></span></span>
										  </div>
										  <input type="number" id="itemqntty" value="<?=$addition_row['item_qntty']?>" readonly class="form-control">
										  <div class="input-group-append">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"add")' class="fa fa-plus" style='cursor: pointer;'></span></span>
										  </div>
									</div>
								</td>
							</tr>
							<tr>
								<td>Total</td>
								<td><?=$addition_row['item_price']*$addition_row['item_qntty']?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	
		<?php } ?>
	</div>
	
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
					<div class="input-group">
						<div class="input-group-prepend">
						    <span class="input-group-text"> Themes: </span>
						</div>
						  <select class="form-control" id="themeID" disabled>
						  	<option value="">&mdash; Please Choose &mdash; </option>
						  	<?php 
						  	$theme = mysql_query("SELECT * FROM tbl_themes");
						  	while($row_t = mysql_fetch_array($theme)){
						  		$selected = ($details['theme'] == $row_t['theme_id'])?"selected":"";
						  	?>
						  	<option <?=$selected?> value="<?=$row_t['theme_id']?>"><?=$row_t['theme_name']?></option>
						  	<?php } ?>
						  </select>
						  <div class="input-group-append">
						    <span class="input-group-text"><span onclick='viewImage(<?=$details['theme']?>)' class="fa fa-eye" style='cursor: pointer;'></span></span>
						  </div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group">
						<div class="input-group-prepend">
						    <span class="input-group-text"> Venue: </span>
						</div>
						  <select class="form-control" id="venueID" disabled>
						  	<option value="">&mdash; Please Choose &mdash; </option>
						  	<?php 
						  	$v = mysql_query("SELECT * FROM tbl_venue");
						  	while($row_v = mysql_fetch_array($v)){
						  		$selected2 = ($details['venue'] == $row_v['venue_id'])?"selected":"";
						  	?>
						  	<option <?=$selected2?> value="<?=$row_v['venue_id']?>"><?=$row_v['venue_name']?></option>
						  	<?php } ?>
						  </select>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12" style="margin-top: 10px">
			<div class="alert alert-success">
				<h6>Item Price: &#8369; <?=number_format(getTotalPrice_costumize_view($id), 2)?></h6>
				<h6>Venue Price: &#8369; <?=number_format(getVenuePrice($details['venue']), 2)?></h6>
				<h6>Service Fee: &#8369; <?=number_format(6000, 2)?></h6>
				<hr>
				<h6>Total Price: &#8369; <?=number_format($details['total_amount'], 2)?></h6>
				<input type="hidden" id="totalAmout_save" name="">
			</div>
		</div>
	
</div>
<div class="modal fade" id="view_img">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="fa fa-eye"></span> View </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      	<div class="col-md-12">
      		<img id="imgView" style="width: 100%">
      	</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Close </button>
      </div>

    </div>
  </div>
</div>	
<script type="text/javascript">
	function viewImage(themeID){
		if(themeID == ''){
			alert("No Theme Selected");
		}else{
			$.post("../admin/ajax/getThemeImg.php", {
				themeID: themeID
			}, function(data){
				$("#view_img").modal();
				$("#imgView").attr("src","../assets/images/"+data);
			})
		}
		
	}
</script>