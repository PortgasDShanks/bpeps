<?php 
$profile_details = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users WHERE user_id = '$userID'"));

$avatar = ($profile_details['avatar'] == '')?"<img id='img_wrap' alt='Recent 2x2 Picture' class='previewImage01 image-wrap' src='../assets/images/avatar.png' style='object-fit: cover;border:3px solid #b7b1b1;width:2in;height:2in;border-radius: 50%;'>":"<img id='img_wrap' alt='Recent 2x2 Picture' class='previewImage01 image-wrap' src='../assets/images/".$profile_details['avatar']."' style='object-fit: cover;border:3px solid #b7b1b1;width:2in;height:2in;border-radius: 50%;'>";
?>

	<div class="col-md-12 animated flipInX">
		<h5 class="card-title">Profile</h5>
	</div>
	<div class="col-md-4 animated slideInLeft" style="padding: 9%">
		<div>
			<h2>Update your information including your avatar</h2>
		</div>
	</div>
	<div class="col-md-8 animated slideInLeft" style="padding-top: 1%;padding-bottom: 1%;">
		<form id="editProfile" method="POST" action="" enctype="multipart/form-data">
		<center>

		<?=$avatar?>
		
		<div class="image-upload" style="margin-top: 5px;">
		    <input type="file" style="display: none;" name="avatar" id="files" class="btn-inputfile share"/>
		      <label for="files" class="btn default" style="font-size: 16px;"><i class="fa fa-file-image-o"></i> Change</label>
	  	</div>
	  	</center>
		<div class="input-group">
		    <div class="input-group-prepend">
		      <span class="input-group-text">Firstname</span>
		    </div>
		    <input type="text" class="form-control" name='fname' value="<?=$profile_details['firstname']?>">
		</div>
		<br>
		<div class="input-group">
		    <div class="input-group-prepend">
		      <span class="input-group-text">Middlename</span>
		    </div>
		    <input type="text" class="form-control" name='mname' value="<?=$profile_details['middlename']?>">
		</div>
		<br>
		<div class="input-group">
		    <div class="input-group-prepend">
		      <span class="input-group-text">Lastname</span>
		    </div>
		    <input type="text" class="form-control" name='lname' value="<?=$profile_details['lastname']?>">
		</div>
		<br>
		<div class="input-group">
		    <div class="input-group-prepend">
		      <span class="input-group-text">Contact #</span>
		    </div>
		    <input type="text" class="form-control" name='connum' value="<?=$profile_details['contact_no']?>">
		</div>
		<br>
		<div class="input-group">
		    <div class="input-group-prepend">
		      <span class="input-group-text">Address</span>
		    </div>
		    <textarea class="form-control" style="resize: none;" rows="4" name='address'><?=$profile_details['address']?></textarea>
		</div>
		<br>
		<div class="input-group">
		    <div class="input-group-prepend">
		      <span class="input-group-text">Email Address</span>
		    </div>
		    <input type="email" name='email' class="form-control" value="<?=$profile_details['email']?>">
		</div>
		<CENTER><label style="text-align: center;padding-top: 1%;font-weight: bold;">&mdash; Credentials &mdash;</label></CENTER>
		<div class="input-group">
		    <div class="input-group-prepend">
		      <span class="input-group-text">Old Password</span>
		    </div>
		    <input type="password" onkeyup='passwordChecker()' name='password' id='password' class="form-control" value="">
		    <div class="input-group-prepend">
	          <span class="input-group-text"><strong><span id='icon'></span></strong></span>
	        </div>
		</div>
		<div class="input-group" style='margin-top: 10px;'>
	        <div class="input-group-prepend">
	          <span class="input-group-text"><strong>New Password:</strong></span>
	        </div>
	        <input type="password" readonly class="form-control" id="new_pass" name="new_pass">
	      </div>

	      <div class="input-group" style='margin-top: 10px;'>
	        <div class="input-group-prepend">
	          <span class="input-group-text"><strong>Confirm Password:</strong></span>
	        </div>
	        <input type="password" readonly onkeyup='confirmPassword()' class="form-control" id="conf_pass" name="conf_pass">
	        <div class="input-group-prepend">
	          <span class="input-group-text"><strong><span id='iconConf'></span></strong></span>
	        </div>
	      </div>
	      <br>
	      <button type="submit" id="btn-edit" class="btn btn-primary pull-right btn-sm"><span class="fa fa-check"></span> Save Changes</button>
		</form>
	</div>
<script type="text/javascript">
	$(document).ready( function(){
		$("#editProfile").on('submit',(function(e) {
		    e.preventDefault();
		        $.ajax({
		         url:"../admin/ajax/update_profile.php",
		         type: "POST",
		         data:  new FormData(this),
		         beforeSend: function(){},
		         contentType: false,
		         cache: false,
		         processData:false,
		         success: function(data)
		            { 
		              alert(data);
		          
		            },
		             error: function() 
		         {
		         	alert("Error")
		         }           
		       });
		        
		    }));
	});
	function confirmPassword(){
	    var newPass = $("#new_pass").val();
	    var conf = $("#conf_pass").val();
	    if(conf != newPass){
	      $("#iconConf").html("<span class='fa fa-close' style='color:red'></span> Did Not Match");
	      $("#btn-edit").attr('disabled', true);
	    }else{
	      $("#iconConf").html("<span class='fa fa-check-circle' style='color:green'></span> Matched");
	      $("#btn-edit").attr('disabled', false);
	    }
	  }
	function passwordChecker(){
		var password = $("#password").val();
		$.post("../admin/ajax/passwordChecker.php", {
			password: password
		}, function(data){
			if(data > 0){
				$("#icon").html("<span class='fa fa-check-circle' style='color:green'></span>");
		        $("#new_pass").attr("readonly", false);
		        $("#conf_pass").attr("readonly", false);
			}else{
				$("#icon").html("<span class='fa fa-close' style='color:red'></span>");
		        $("#new_pass").attr("readonly", true);
		        $("#conf_pass").attr("readonly", true);
			}
		})
	}
	$(".btn-inputfile").change(function () {
      $("#btn-edit").prop("disabled", false);
      var input = document.getElementById('files');
      previewFile(input);
  });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
</script>


