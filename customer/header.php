<div class="w3-banner jarallax">
		<div class="wthree-different-dot">
			<div class="head">
				<div class="container">
					<div class="navbar-top">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
								 <div class="navbar-brand logo ">
									<h1><a href="#"><img src="assets/images/logo.png" style="height: 60px;width: 60px;object-fit: cover"></a></h1>
								</div>

							</div>

							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							 <ul class="nav navbar-nav link-effect-4">
								<li class="active first-list"><a href="index.html">Home</a></li>
								<li><a href="#services" class="scroll">Services</a></li>
								<li><a href="#packages" class="scroll">Packages</a></li>
								<li><a href="#custom" class="scroll">Customize</a></li> 
								<li style='<?php echo $signin; ?>'><a href="#signin_signup" class="scroll">Sign In/Sign Up</a></li> 
								<li style='<?php echo $display; ?>'><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span data-letters="Pages">Hi , <?php echo $_SESSION['firstname']; ?></span><span class="caret"></span></a>
									<ul class="dropdown-menu" style='margin-left: -53px;'> 
										<li><a href="#" onclick='window.location="customer/index.php?view=transactions"' class="scroll">Transactions</a></li>
										<li><a href="#" onclick='window.location="customer/index.php?view=messages"' class="scroll">Messages</a></li>
										<li><a href="#transactions" class="scroll">Profile</a></li>
										<li><a href="#" onclick="logout()" class="scroll">Logout</a></li>
									</ul>
								</li>
							  </ul>
							</div><!-- /.navbar-collapse -->
						</div>
				</div>
			</div>
			<!-- banner -->
			<div class="banner">
				<div class="container">
					<div class="slider">
						
						<script src="assets/customer_assets/js/responsiveslides.min.js"></script>
						<script>
								// You can also use "$(window).load(function() {"
								$(function () {
								// Slideshow 4
									$("#slider3").responsiveSlides({
										auto: true,
										pager: true,
										nav: true,
										speed: 500,
										namespace: "callbacks",
										before: function () {
											$('.events').append("<li>before event fired.</li>");
										},
										after: function () {
											$('.events').append("<li>after event fired.</li>");
										}
									 });				
								});
						</script>
						<div  id="top" class="callbacks_container-wrap">
							<ul class="rslides" id="slider3">
								<li>
									<div class="slider-info" data-aos="fade-left">
									<h6>PARTY OF THIS YEAR</h6>
										<h3>Happy Birthday</h3>
										<p >We assure your best birthday celebration.</p>
									</div>
								</li>
								<li>
									<div class="slider-info" data-aos="fade-left">
									<h6>PARTY OF THIS YEAR</h6>
										<h3>Happy Wedding Day</h3>
										<p>We assure your best wedding celebration.</p>
									</div>
								</li>
								<li>
									<div class="slider-info" data-aos="fade-left">
									<h6>PARTY OF THIS YEAR</h6>
										<h3> LIGHT YOUR NIGHT</h3>
										<p>Our Company will do everything.</p>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- //banner -->
		</div>
	</div>