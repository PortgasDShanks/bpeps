<div class="services jarallax" id="services">
		<h3 class="heading-agileinfo" data-aos="zoom-in">Overall Ratings<span class="thr">Events Is A Professionally Managed Event</span></h3>
	<div class="container">
        <div class="col-md-12 ser-1" data-aos="fade-right">
            <h1><div id="stars" style="text-align: center;"></div><h1>
        </div>
        <div class="clearfix"></div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready( function(){
        averageRatings();
    });
    

    function averageRatings(){
        $.post("admin/ajax/getAverageRatings.php",{

        }, function(data){
           document.getElementById("stars").innerHTML = getStars(data);
        })
    }
    function getStars(rating) {

      // Round to nearest half
      rating = Math.round(rating * 2) / 2;
      let output = [];

      // Append all the filled whole stars
      for (var i = rating; i >= 1; i--)
        output.push('<i class="fa fa-star" aria-hidden="true" style="color: gold;"></i>&nbsp;');

      // If there is a half a star, append it
      if (i == .5) output.push('<i class="fa fa-star-half-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');

      // Fill the empty stars
      for (let i = (5 - rating); i >= 1; i--)
        output.push('<i class="fa fa-star-o" aria-hidden="true" style="color: gold;"></i>&nbsp;');

      return output.join('');

    }
</script>