<div class="register-sec-w3l" id="signin_signup">
	<div class='row'>
		<div class='col-md-6'>
			<div class='col-md-12'>
			<h3 class="heading-agileinfo" data-aos="zoom-in">Sign up<span class="thr">Events Is A Professionally Managed Event</span></h3>
				<div class="book-appointment" data-aos="zoom-in">
						<div class='row'>
							<div class='col-md-4'>
								<div class="gaps">
									<p></p>
									<input type="text" name="fname" id='fname' placeholder="First Name" required="" />
								</div>
							</div>
							<div class='col-md-4'>
								<div class="gaps">
									<p></p>
									<input type="text" name="mname" id='mname' placeholder="Middle Name" required="" />
								</div>
							</div>
							<div class='col-md-4'>
								<div class="gaps">
									<p></p>
									<input type="text" name="lname" id='lname' placeholder="Last Name" required="" />
								</div>
							</div>
							<div class='col-md-4'>
								<div class="gaps">
									<p></p>
									<input type="text" name="contactNo" id='contactNo' placeholder="Contact #" required="" />
								</div>
							</div>
							<div class='col-md-4'>
								<div class="gaps">
									<p></p>
									<input type="text" name="address" id='address' placeholder="Address" required="" />
								</div>
							</div>
							<div class='col-md-4'>
								<div class="gaps">
									<p></p>
									<input type="email" name="emailadd" id='emailadd' placeholder="Email Address" required="" />
								</div>
							</div>
							<div class='col-md-6'>
								<div class="gaps">
									<p></p>
									<input type="text" name="un" id='un' placeholder="Username" required="" />
								</div>
							</div>
							<div class='col-md-6'>
								<div class="gaps">
									<p></p>
									<input type="password" name="password" id='password' placeholder="Password" required="" />
								</div>
							</div>
							<div class='col-md-12'>
								<div class="gaps">
									<div class="col-md-12">
										<button type="button" id="termBtn" class="pull-left"> Terms and Agreement </button>
										
									</div>
									
									<div id="terms" class="col-md-12" style="background-color: floralwhite;padding: 10px;margin-top: 5px;display: none">
										<ol>
											<li>Lorem Ipsum dolor Lorem Ipsum dolor Lorem Ipsum dolor Lorem Ipsum dolor</li>
											<li>Lorem Ipsum dolor Lorem Ipsum dolor Lorem Ipsum dolor Lorem Ipsum dolor</li>
											<li>Lorem Ipsum dolor Lorem Ipsum dolor Lorem Ipsum dolor Lorem Ipsum dolor</li>
										</ol>
										<hr>
										<input type="checkbox" id="termsandcondition" onchange='termsAgree()' name=""> I agree with the terms and conditions.
									</div>

									<button type="button" id="signBtn" class="pull-right" onclick="signUpClient()"> Sign Up</button>
								</div>
							</div>
						</div>
						
				</div>
			</div>
		</div>
		<div class='col-md-6'>
			<div class='col-md-12'>
			<h3 class="heading-agileinfo" data-aos="zoom-in">Sign In<span class="thr">Events Is A Professionally Managed Event</span></h3>
				<div class="book-appointment" data-aos="zoom-in">
						<div class='row'>
							<div class='col-md-12'>
								<div class="gaps">
									<p></p>
									<input type="text" name="un_username" id='un_username' placeholder="Username" required="" />
								</div>
							</div>
							<div class='col-md-12'>
								<div class="gaps">
									<p></p>
									<input type="password" name="pw_password" id='pw_password' placeholder="Password" required="" />
								</div>
							</div>
							<div class='col-md-12' style='margin-top: 10px'>
								<button type='button' onclick="signInUser()" class='btn btn-sm btn-primary pull-right'><span class='fa fa-check-circle'></span> Sign in</button>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</div>