<?php 
$user_id = $_SESSION['cust_user_id'];

// $getDetails = mysql_fetch_array(mysql_query("SELECT * FROM tbl_booking_cart  WHERE user_id = '$user_id' AND status = 0 AND is_package = 1 AND item_type = ''"));

// $is_package = $getDetails['is_package'];
// $item_id = $getDetails['item_id'];
// $item_type = $getDetails['item_type'];

// 	if($item_type == ''){
// 		$sql = "SELECT * FROM tbl_package_header as p, tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.theme_id = t.theme_id AND p.package_header_id = '$item_id' AND p.package_header_id = h.package_header_id AND p.package_venue = v.venue_id";
// 	}
	


// $package_details = mysql_fetch_array(mysql_query($sql));
?>

<div class="col-md-12 animated flipInX" style="margin-top: 10px;">
   <h4>My Booking Cart</h4>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<?php 
if(countCartContent_package($userID) > 0){ 
	$package = mysql_query("SELECT * FROM tbl_booking_cart  WHERE user_id = '$user_id' AND status = 0 AND is_package = 1 AND item_type = ''");
	while($pkg_row = mysql_fetch_array($package)){
		$cartID = $pkg_row['cart_id'];
		$pkg_id = $pkg_row['item_id'];
		$details = mysql_fetch_array(mysql_query("SELECT * FROM tbl_package_header as p, tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.theme_id = t.theme_id AND p.package_header_id = '$pkg_id' AND p.package_header_id = h.package_header_id AND p.package_venue = v.venue_id"))
?>
<div class="col-md-6 animated slideInLeft" style="padding: 10px">
	<img src="../assets/images/<?=$details['package_img']?>" style='width: 100%;border-radius: 5%'>
	<button id='deletetocart<?=$pkg_row["cart_id"]?>' onclick='deleteItemToCart(<?=$pkg_row['cart_id']?>)' class="btn btn-sm btn-block btn-danger"><span class="fa fa-trash"></span> Delete</button>
</div>
<!-- ADDITIONAL -->

<div class="col-md-6 animated slideInLeft" style="padding: 10px">
	<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$cartID?>">
        Package Details</a>
      </h4>
    </div>
    <div id="collapse<?=$cartID?>" class="panel-collapse collapse in">
      <div class="panel-body">
      	<table class="table table-bordered">
		<tbody>
			<tr>
				<td>Theme Name</td>
				<td><?=$details['theme_name']?></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><?=$details['package_desc']?></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><?=$details['package_price']?></td>
			</tr>
		</tbody>
	</table>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapsed<?=$cartID?>">
        Inclusions</a>
      </h4>
    </div>
    <div id="collapsed<?=$cartID?>" class="panel-collapse collapse">
      <div class="panel-body">
      	<label><h6>Equipments</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_equipment_designs as d WHERE h.package_item = d.design_id AND package_header_id = '$pkg_id' AND cat = 'I'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['design_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Invitational Cards</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_invitational_cards as d WHERE h.package_item = d.ic_id AND package_header_id = '$pkg_id' AND cat = 'IC'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['ic_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Giveaways</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_giveaways as d WHERE h.package_item = d.giveaway_id AND package_header_id = '$pkg_id' AND cat = 'G'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['giveaway_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Cakes</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
					<td>Size</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_cake as d, tbl_cake_sizes as s WHERE h.package_item = s.size_id AND package_header_id = '$pkg_id' AND cat = 'C' AND d.cake_id = s.cake_id");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['cake_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
					<td><?=$equip_row['size']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>freebies</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_freebies as d WHERE h.package_item = d.fb_id AND package_header_id = '$pkg_id' AND cat = 'F'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['fb_name']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
		<label><h6>Entertainments</h6></label>
		<table class="table table-bordered">
			<thead>
				<tr>
					<td></td>
					<td>Quantity</td>
				</tr>
			</thead>
			<tbody>
				<?php 
					$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_entertainment as d WHERE h.package_item = d.ent_id AND package_header_id = '$pkg_id' AND cat = 'E'");
					while($equip_row = mysql_fetch_array($equip)){
				?>
				<tr>
					<td><?=$equip_row['ent_type']?></td>
					<td><?=$equip_row['item_qntty']?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
      </div>
    </div>
  </div>
</div>
</div>
<?php } ?>
<div class="col-md-12 animated slideInLeft" >
	<h4 style="text-align: center;">Additional Items</h4>
	<div class="row" style="padding: 10px">
	<?php 
	$additional = mysql_query("SELECT * FROM tbl_booking_cart WHERE user_id = '$user_id' AND item_type != '' AND status = 0 AND is_package = 1");
	$count = mysql_num_rows($additional);
	if($count > 0){
		while($addition_row = mysql_fetch_array($additional)){
			$item_id = $addition_row['item_id'];
			$query = ($addition_row['item_type'] == 'I')?"SELECT design_img,design_name,design_desc FROM tbl_equipment_designs WHERE design_id = '$item_id'":(($addition_row['item_type'] == 'IC')?"SELECT ic_image,ic_name FROM tbl_invitational_cards WHERE ic_id = '$item_id'":(($addition_row['item_type'] == 'G')?"SELECT giveaway_img,giveaway_name FROM tbl_giveaways WHERE giveaway_id = '$item_id'":"SELECT cake_img,cake_name,size FROM tbl_cake as c, tbl_cake_sizes as s WHERE c.cake_id = s.cake_id AND s.size_id = '$item_id'"));

			$sql = mysql_fetch_array(mysql_query($query));

			$desc = ($addition_row['item_type'] == 'I')?$sql[2]:"";
			$size = ($addition_row['item_type'] == 'C')?$sql[2]:"";
	?>
		<div class="col-md-6" style="border: 1px solid #a2a2a26e;padding: 10px;">
			<div class="row">
				<div class="col-md-5">
					<img src="../assets/images/<?=$sql[0]?>" style='width: 100%;border-radius: 5%;    height: 100px;object-fit: contain;'>
				</div>
				<div class="col-md-7">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Name</td>
								<td><?=$sql[1]?></td>
							</tr>
							<tr>
								<td>Description</td>
								<td><?=$desc?></td>
							</tr>
							<tr>
								<td>Price</td>
								<td><?=$addition_row['item_price']?></td>
							</tr>
							<tr>
								<td>Size</td>
								<td><?=$size?></td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td>
									<div class="input-group">
										<div class="input-group-prepend">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"minus")' class="fa fa-minus" style='cursor: pointer;'></span></span>
										  </div>
										  <input type="number" id="itemqntty" value="<?=$addition_row['item_qntty']?>" readonly class="form-control">
										  <div class="input-group-append">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"add")' class="fa fa-plus" style='cursor: pointer;'></span></span>
										  </div>
									</div>
								</td>
							</tr>
							<tr>
								<td>Total</td>
								<td><?=$addition_row['item_price']*$addition_row['item_qntty']?></td>
							</tr>
							<tr>
								<td colspan="2"><button id='deletetocart<?=$addition_row["cart_id"]?>' onclick='deleteItemToCart(<?=$addition_row['cart_id']?>)' class="btn btn-sm btn-block btn-danger"><span class="fa fa-trash"></span> Delete</button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	
		<?php } ?>
	</div>
	<?php }else{ ?>
		<div class="col-md-12">
			<h5 style=""> No Additional Items Added. <span style="text-decoration: underline;cursor: pointer;" onclick='window.location="index.php?view=add-additional-items"'>Do you want to add?</span> </h5>
		</div>
	<?php } ?>
	
		<div class="col-md-12"></div>
		<div class="col-md-12">
			<div class="alert alert-success">
				<h6>Total Price: &#8369; <?=number_format(getTotalPrice_package($userID), 2)?></h6>
				<input type="hidden" id="totalPricePackage" value="<?=getTotalPrice_package($userID)?>" name="">	
			</div>
			<button class="btn btn-sm btn-block btn-success pull-right" id='finishedBook' onclick='window.location="index.php?view=finish-package-booking"'><span class="fa fa-check-circle"></span> Finalize My Booking </button>
			
			<button class="btn btn-sm btn-block btn-success pull-right" onclick='window.location="index.php?view=add-additional-items"'><span class="fa fa-shopping-cart"></span> Continue Adding to my booking </button>
		</div>
	
</div>
<?php }else{ ?>
<div class="col-md-12 animated slideInLeft">
	<h5> No Items In Cart</h5>
</div>
<?php } ?>
<script type="text/javascript">
	$(document).ready( function(){
		
	})
	$("#finishedBook").click(function(){
			$("#finishBookContent").slideToggle();
		});
	function updateQntty(cartID,action){
		$.post("../admin/ajax/updateCartQntty.php", {
			cartID: cartID,
			action: action
		},function(data){
			if(data > 0){
				swal({
	              title: "All Good!",
	              text: "Cart Item quantity successfully changed.",
	              type: "success"
	          	}, function(){
	           		window.location.reload();
	          	});
				
			}else{
				swal("Something went wrong, Please try again later.");
			}
		});
	}
	function deleteItemToCart(id){
		$("#deletetocart"+id).prop("disabled", true);
		$("#deletetocart"+id).html("<span class='fa fa-spin fa-spinner'></span> Deleting");
		$.post("../admin/ajax/deleteItemtoCart.php", {
			id: id
		}, function(data){
			if(data > 0){
				swal({
	              title: "All Good!",
	              text: "Cart Item successfully deleted.",
	              type: "success"
	          	}, function(){
	           		window.location.reload();
	          	});
				
			}else{
				swal("Something went wrong, Please try again later.");
			}
		});
	}
	function finishBooking(){
		var refNum = $("#refNum").val();
		var eventDate = $("#eventDate").val();
		var eventtimeFrom = $("#eventtimeFrom").val();
		var eventtimeTo = $("#eventtimeTo").val();
		var payment_mode = $("#payment_mode").val();
		var totalPricePackage = $("#totalPricePackage").val();
		$("#book").prop("disabled", true);
		$("#book").html("<span class='fa fa-spin fa-spinner'></span> Loading");
		$.post("../admin/ajax/finishBooking.php", {
			refNum: refNum,
			eventDate: eventDate,
			eventtimeFrom: eventtimeFrom,
			eventtimeTo:eventtimeTo,
			payment_mode: payment_mode,
			totalPricePackage: totalPricePackage
		}, function(data){
			if(data > 0){
				alert("Your Booking was successfully finished");
			}else{
				alert("Error While saving data");
			}
			window.location = 'index.php?view=transactions';
		})
	}
</script>