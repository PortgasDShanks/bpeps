<style type="text/css">


</style>
<h5 class="card-title animated flipInX">Transactions</h5>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<div class="col-md-12 animated slideInLeft" style="padding: 1%">
	<ul class="nav nav-tabs">
	  <li class="nav-item" style="width: 50%">
	    <a class="nav-link active" data-toggle="tab" href="#home">Packaged Transaction</a>
	  </li>
	  <li class="nav-item" style="width: 50%">
	    <a class="nav-link" data-toggle="tab" href="#menu1">Customize Transaction</a>
	  </li>
	</ul>
	<div class="tab-content">
	  <div class="tab-pane container active" id="home">
  		<div class='col-sm-12 animated slideInLeft' style='margin-top: 10px;overflow: auto;'>
			 <table id='package' class="table" style='margin-top:10px;width: 100%;'>
				<thead>
					<tr>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>ACTION</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>REFERENCE #</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>EVENT DATE</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>EVENT TIME</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>MODE OF PAYMENT</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>DATE ADDED</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>VENUE STATUS</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>STATUS</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	  </div>
	  <div class="tab-pane container fade" id="menu1">
	  	<div class='col-sm-12 animated slideInLeft' style='margin-top: 10px;overflow: auto;'>
			<table id='customize_bookings' class="table" style='margin-top:10px;width: 100%;'>
				<thead>
					<tr>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>REFERENCE #</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>EVENT DATE</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>EVENT TIME</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>MODE OF PAYMENT</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>DATE ADDED</th>
						<th style='background-color: rgb(34 45 50);color: #ffffff;'>STATUS</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	  </div>
	</div>
</div>
<!--  -->
<script type="text/javascript">
	$(document).ready( function(){
		packageBooking();
		costumizeBooking();
	});
	function viewDetails_package(id){
		window.location = 'index.php?view=view-package-details&id='+id;
	}
	function viewCostumDetails(id){
		window.location = 'index.php?view=view-costumize-details&id='+id;
	}
	function rateUs(id){
		$.post("../admin/ajax/rateChecker.php", {
			id: id
		}, function(data){
			if(data > 0){
				swal("You already rate this transaction!");
			}else{
				window.location = 'index.php?view=rate-us&id='+id;
			}
		})
		
	}
	function packageBooking(){
		$("#package").DataTable().destroy();
	        $('#package').dataTable({
	        "processing":true,
	        "ajax":{
	            "url":"../admin/ajax/datatables/package_bookings.php",
	            "dataSrc":"data"
	        },
	        "columns":[
	            {
	                "data":"count"
	            },
	            {
	                "data":"action"
	            },
	            {
	                "data":"ref_num"
	            },
	            {
	                "data":"eventDate"
	            },
	            {
	                "data":"eventTime"
	            },
	            {
	                "data":"modeOfPayment"
	            },
	            {
	                "data":"dateAdded"
	            },
	            {
	            	"data":"venue_stat"
	            },
	            {
	                "data":"status"
	            }
	        ]   
        });
	}
	function costumizeBooking(){
		$("#customize_bookings").DataTable().destroy();
	        $('#customize_bookings').dataTable({
	        "processing":true,
	        "ajax":{
	            "url":"../admin/ajax/datatables/costumize_booking.php",
	            "dataSrc":"data"
	        },
	        "columns":[
	            {
	                "data":"count"
	            },
	            {
	                "data":"action"
	            },
	            {
	                "data":"ref_num"
	            },
	            {
	                "data":"eventDate"
	            },
	            {
	                "data":"eventTime"
	            },
	            {
	                "data":"modeOfPayment"
	            },
	            {
	                "data":"dateAdded"
	            },
	            {
	                "data":"status"
	            }
	        ]   
        });
	}
</script>