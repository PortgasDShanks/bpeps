<div class="services jarallax" id="services">
    <h3 class="heading-agileinfo" data-aos="zoom-in">Top Selected Package <span class="thr">Events Is A Professionally Managed Event</span></h3>
  <div class="container">
      <div class="w3ls_banner_bottom_grids">
        <?php 
        $top = mysql_query("SELECT c.item_id, COUNT(*) AS total FROM tbl_transactions as t, tbl_booking_cart as c WHERE t.trans_id = c.transaction_id AND c.is_package = 1 AND c.item_type = '' GROUP BY c.item_id ORDER BY total DESC LIMIT 5");
        while($top_row = mysql_fetch_array($top)){
          $pkg_id = $top_row['item_id'];
          $getpkgDetails = mysql_fetch_array(mysql_query("SELECT * FROM tbl_package_header as p, tbl_themes as t WHERE p.theme_id = t.theme_id AND p.package_header_id = '$pkg_id'"));
        ?>
        <div class="col-md-4 agileits_services_grid aos-init aos-animate" data-aos="fade-left">
          <div class="w3_agile_services_grid1">
            <img src="assets/images/<?=$getpkgDetails['package_img']?>" alt=" " class="img-responsive">
            <div class="w3_blur"></div>
          </div>
          <div class="pr-ta">
            <h3><?=$getpkgDetails['theme_name']?></h3>
            <p><?=$getpkgDetails['package_desc']?></p>
            <div class="tabl-erat">
              <div class="col-md-12 ratt">
                <h6>&#8369; <?=number_format($getpkgDetails['package_price'], 2)?></h6>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <?php } ?>
        <div class="clearfix"> </div>
      </div>
    </div>
</div>