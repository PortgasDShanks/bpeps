<?php 
$user_id = $_SESSION['cust_user_id'];

// $getDetails = mysql_fetch_array(mysql_query("SELECT * FROM tbl_booking_cart  WHERE user_id = '$user_id' AND status = 0 AND is_package = 0"));

// $is_package = $getDetails['is_package'];
// $item_id = $getDetails['item_id'];
// $item_type = $getDetails['item_type'];

// 	if($item_type == ''){
// 		$sql = "SELECT * FROM tbl_package_header as p, tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.theme_id = t.theme_id AND p.package_header_id = '$item_id' AND p.package_header_id = h.package_header_id AND p.package_venue = v.venue_id";
// 	}
	


// $package_details = mysql_fetch_array(mysql_query($sql));
?>

<div class="col-md-12 animated flipInX" style="margin-top: 10px;">
   <h4>My Booking Cart</h4>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<?php if(countCartContent_customize($userID) > 0){ ?>
<div class="col-md-12 animated slideInLeft" >
	<div class="row" style="padding: 10px">
	<?php 
	$additional = mysql_query("SELECT * FROM tbl_booking_cart WHERE user_id = '$user_id' AND is_package = 0 AND status = 0");
		while($addition_row = mysql_fetch_array($additional)){
			$item_id = $addition_row['item_id'];
			$query = ($addition_row['item_type'] == 'I')?"SELECT design_img,design_name,design_desc FROM tbl_equipment_designs WHERE design_id = '$item_id'":(($addition_row['item_type'] == 'IC')?"SELECT ic_image,ic_name FROM tbl_invitational_cards WHERE ic_id = '$item_id'":(($addition_row['item_type'] == 'G')?"SELECT giveaway_img,giveaway_name FROM tbl_giveaways WHERE giveaway_id = '$item_id'":"SELECT cake_img,cake_name,size FROM tbl_cake as c, tbl_cake_sizes as s WHERE c.cake_id = s.cake_id AND s.size_id = '$item_id'"));

			$sql = mysql_fetch_array(mysql_query($query));

			$desc = ($addition_row['item_type'] == 'I')?$sql[2]:"";
			$size = ($addition_row['item_type'] == 'C')?$sql[2]:"";
	?>
		<div class="col-md-6" style="border: 1px solid #a2a2a26e;padding: 10px;">
			<div class="row">
				<div class="col-md-5">
					<img src="../assets/images/<?=$sql[0]?>" style='width: 100%;border-radius: 5%;    height: 100px;object-fit: contain;'>
				</div>
				<div class="col-md-7">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Name</td>
								<td><?=$sql[1]?></td>
							</tr>
							<tr>
								<td>Description</td>
								<td><?=$desc?></td>
							</tr>
							<tr>
								<td>Price</td>
								<td><?=$addition_row['item_price']?></td>
							</tr>
							<tr>
								<td>Size</td>
								<td><?=$size?></td>
							</tr>
							<tr>
								<td>Quantity</td>
								<td>
									<div class="input-group">
										<div class="input-group-prepend">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"minus")' class="fa fa-minus" style='cursor: pointer;'></span></span>
										  </div>
										  <input type="number" id="itemqntty" value="<?=$addition_row['item_qntty']?>" readonly class="form-control">
										  <div class="input-group-append">
										    <span class="input-group-text"><span onclick='updateQntty(<?=$addition_row['cart_id']?>,"add")' class="fa fa-plus" style='cursor: pointer;'></span></span>
										  </div>
									</div>
								</td>
							</tr>
							<tr>
								<td>Total</td>
								<td><?=$addition_row['item_price']*$addition_row['item_qntty']?></td>
							</tr>
							<tr>
								<td colspan="2"><button id='deletetocart<?=$addition_row["cart_id"]?>' onclick='deleteItemToCart(<?=$addition_row['cart_id']?>)' class="btn btn-sm btn-block btn-danger"><span class="fa fa-trash"></span> Delete</button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	
		<?php } ?>
	</div>
	
		<div class="col-md-12">
			<div class="row">
				<!-- <div class="col-md-6">
					<div class="input-group">
						<div class="input-group-prepend">
						    <span class="input-group-text"> Themes: </span>
						</div>
						  <select class="form-control" id="themeID">
						  	<option value="">&mdash; Please Choose &mdash; </option>
						  	<?php 
						  	$theme = mysql_query("SELECT * FROM tbl_themes");
						  	while($row_t = mysql_fetch_array($theme)){
						  	?>
						  	<option value="<?=$row_t['theme_id']?>"><?=$row_t['theme_name']?></option>
						  	<?php } ?>
						  </select>
						  <div class="input-group-append">
						    <span class="input-group-text"><span onclick='viewImage()' class="fa fa-eye" style='cursor: pointer;'></span></span>
						  </div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group">
						<div class="input-group-prepend">
						    <span class="input-group-text"> Venue: </span>
						</div>
						  <select class="form-control" id="venueID" onchange="getVenuePrice()">
						  	<option value="">&mdash; Please Choose &mdash; </option>
						  	<?php 
						  	$v = mysql_query("SELECT * FROM tbl_venue");
						  	while($row_v = mysql_fetch_array($v)){
						  	?>
						  	<option value="<?=$row_v['venue_id']?>"><?=$row_v['venue_name']?></option>
						  	<?php } ?>
						  </select>
					</div>
				</div> -->
			</div>
		</div>
		<div class="col-md-12" style="margin-top: 10px">
			<div class="alert alert-success">
				<h6>Total Price: &#8369; <?=number_format(getTotalPrice_costumize($userID), 2)?></h6>
				<input type="hidden" id="totalPriceCostum" value="<?=getTotalPrice_costumize($userID)?>" name="">
				<!-- <h6>Venue Price: &#8369; <span id="vPrice"></span></h6>
				<input type="hidden" id="totalvPrice" name="">	
				<h6>Service Fee: &#8369; <?=number_format(6000, 2)?></h6>
				<input type="hidden" id="serviceFee" value="6000" name="">
				<hr>
				<h6>Total Price: &#8369; <span id="totalAmount"></span></h6>
				<input type="hidden" id="totalAmout_save" name=""> -->
			</div>
			<button class="btn btn-sm btn-block btn-success pull-right" onclick='window.location="index.php?view=finish-costumize-booking"'><span class="fa fa-check-circle"></span> Finalize My Booking </button>
			<button class="btn btn-sm btn-block btn-success pull-right" onclick='window.location="index.php?view=costumize"'><span class="fa fa-shopping-cart"></span> Continue Adding to my booking </button>
		</div>
	
</div>
<?php }else{ ?>
<div class="col-md-12 animated slideInLeft">
	<h5> No Items In Cart</h5>
</div>
<?php } ?>



<div class="modal fade" id="finish_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="fa fa-check-circle"></span> Finish Booking</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      	<div class="input-group">
			<div class="input-group-prepend">
			     <span class="input-group-text"> Reference # </span>
			</div>
			<input type="text" readonly value="BPEPS-<?=date("YmdHis", strtotime(getCurrentDate()))?>" id="refNum" class="form-control">
		  
		</div>
		<br>
        <div class="input-group">
			<div class="input-group-prepend">
			     <span class="input-group-text"> Event Date </span>
			</div>
			<input type="date" id="eventDate" class="form-control">
		  
		</div>
		<br>
		<div class="input-group">
			<div class="input-group-prepend">
			     <span class="input-group-text"> Event Time </span>
			</div>
			<input type="time" id="eventtimeFrom" class="form-control">
		  	<div class="input-group-prepend">
			     <span class="input-group-text"> to </span>
			</div>
			<input type="time" id="eventtimeTo" class="form-control">
		</div>
		<br>
		<div class="input-group">
			<div class="input-group-prepend">
			     <span class="input-group-text"> Payment Mode </span>
			</div>
			<select class="form-control" id="payment_mode">
				<option value="">&mdash; Please Choose &mdash; </option>
				<option value="Gcash"> Gcash </option>
				<option value="Remittance"> Remittance </option>
			</select>
		</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      	<button type="button" id='book' onclick='finishBooking()' class="btn btn-primary" data-dismiss="modal"><span class="fa fa-check-circle"></span>  Continue </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Close </button>
      </div>

    </div>
  </div>
</div>	
<div class="modal fade" id="view_img">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="fa fa-eye"></span> View </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      	<div class="col-md-12">
      		<img id="imgView" style="width: 100%">
      	</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Close </button>
      </div>

    </div>
  </div>
</div>	
<script type="text/javascript">
	$(document).ready( function(){
		var totalPriceCostum = $("#totalPriceCostum").val();
		var serviceFee = $("#serviceFee").val();
		var totalvPrice = $("#totalvPrice").val();
		getTotalPrice(totalPriceCostum, serviceFee, totalvPrice);
		
	});
	function deleteItemToCart(id){
		$("#deletetocart"+id).prop("disabled", true);
		$("#deletetocart"+id).html("<span class='fa fa-spin fa-spinner'></span> Deleting");
		$.post("../admin/ajax/deleteItemtoCart.php", {
			id: id
		}, function(data){
			if(data > 0){
				swal({
	              title: "All Good!",
	              text: "Cart Item successfully deleted.",
	              type: "success"
	          	}, function(){
	           		window.location.reload();
	          	});
				
			}else{
				swal("Something went wrong, Please try again later.");
			}
		});
	}
	function getTotalPrice(totalPriceCostum, serviceFee, totalvPrice){
		$.post("../admin/ajax/getFormattedTotal.php", {
			totalPriceCostum: totalPriceCostum,
			serviceFee: serviceFee,
			totalvPrice: totalvPrice
		}, function(data){
			var total_amnt = data.split('-');
			$("#totalAmount").text(total_amnt[0]);
			$("#totalAmout_save").val(total_amnt[1]);
		})
	}
	function getVenuePrice(){
		var venueID = $("#venueID").val();
		var totalPriceCostum = $("#totalPriceCostum").val();
		var serviceFee = $("#serviceFee").val();
		$.post("../admin/ajax/getVenuePrice.php", {
			venueID: venueID
		}, function(data){
			var amount = data.split('-');
			$("#vPrice").text(amount[0]);
			$("#totalvPrice").val(amount[1]);
			getTotalPrice(totalPriceCostum, serviceFee, amount[1]);
		});
	}
	function viewImage(){
		var themeID = $("#themeID").val();
		if(themeID == ''){
			alert("Please Select Theme");
		}else{
			$.post("../admin/ajax/getThemeImg.php", {
				themeID: themeID
			}, function(data){
				$("#view_img").modal();
				$("#imgView").attr("src","../assets/images/"+data);
			})
		}
		
	}
	function updateQntty(cartID,action){
		$.post("../admin/ajax/updateCartQntty.php", {
			cartID: cartID,
			action: action
		},function(data){
			if(data > 0){
				swal({
	              title: "All Good!",
	              text: "Cart Item quantity successfully changed.",
	              type: "success"
	          	}, function(){
	           		window.location.reload();
	          	});
				
			}else{
				swal("Something went wrong, Please try again later.");
			}
		});
	}
	function finishBooking(){
		var refNum = $("#refNum").val();
		var eventDate = $("#eventDate").val();
		var eventtimeFrom = $("#eventtimeFrom").val();
		var eventtimeTo = $("#eventtimeTo").val();
		var payment_mode = $("#payment_mode").val();
		var totalPricePackage = $("#totalAmout_save").val();
		var theme = $("#themeID").val();
		var venue = $("#venueID").val();
		$("#book").prop("disabled", true);
		$("#book").html("<span class='fa fa-spin fa-spinner'></span> Loading");
		$.post("../admin/ajax/finishBooking_costum.php", {
			refNum: refNum,
			eventDate: eventDate,
			eventtimeFrom: eventtimeFrom,
			eventtimeTo:eventtimeTo,
			payment_mode: payment_mode,
			totalPricePackage: totalPricePackage,
			theme: theme,
			venue: venue
		}, function(data){
			if(data > 0){
				alert("Your Booking was successfully finished");
			}else{
				alert("Error While saving data");
			}
			window.location = 'index.php?view=transactions';
		})
	}
</script>