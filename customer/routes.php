<?php 
switch($view){
    case 'messages':
        require 'messages.php';
    break;
    case 'profile':
        require 'profile.php';
    break;
    case 'costumize':
        require 'book_costumize.php';
    break;
    case 'packages':
        require 'book_packages.php';
    break;
    case 'transactions':
        require 'booking_transactions.php';
    break;
    case 'add-to-cart-package':
        require 'add_to_cart_package.php';
    break;
    case 'view-package-cart':
        require 'view_package_cart.php';
    break;
    case 'add-additional-items':
        require 'add_additional_items.php';
    break;
    case 'equipment-to-cart':
        require 'equipment_to_cart.php';
    break;
    case 'invcard-to-cart':
        require 'invcard_to_cart.php';
    break;
    case 'giveaway-to-cart':
        require 'giveaway_to_cart.php';
    break;
    case 'cake-to-cart':
        require 'cake_to_cart.php';
    break;
    case 'equipment-to-cart-customize':
        require 'equipment_to_cart_customize.php';
    break;
    case 'invcard-to-cart-customize':
        require 'invcard_to_cart_customize.php';
    break;
    case 'giveaway-to-cart-customize':
        require 'giveaway_to_cart_customize.php';
    break;
    case 'cake-to-cart-customize':
        require 'cake_to_cart_customize.php';
    break;
    case 'view-costumize-cart':
        require 'view_costumize_cart.php';
    break;
    case 'view-package-details':
        require 'view_package_details.php';
    break;
    case 'view-costumize-details':
        require 'view_costumize_details.php';
    break;
    case 'finish-package-booking':
        require 'finish_package_booking.php';
    break;
    case 'finish-costumize-booking':
        require 'finish_customize_booking.php';
    break;
    case 'calendar':
        require 'calendar.php';
    break;
    case 'rate-us':
        require 'rate_us.php';
    break;
    default:
    break;

}