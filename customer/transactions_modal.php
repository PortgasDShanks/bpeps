<div class="modal about-modal fade" id="transact_cust" tabindex="-1" role="dialog" data-backdrop='static'>
		<div class="modal-dialog-md" role="document">
			<div class="modal-content">
				<div class="modal-header"> 
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						<h5 class="modal-title">Event Booking</h5>
				</div> 
				<div class="modal-body">
					<div class='row' id='cont_div'>
						<div class='col-sm-12'>
                            <label> Ref. #: <span id='refNum'> REF-<?php echo date("YmdHis", strtotime(getCurrentDate()))?></span></label>
                        </div>
                        <div class='col-sm-6' style='margin-top: 10px'>
                            <div class='input-group'>
                                <input type="radio" onchange="bookingType()" name="bookingType" value='P'> Package
                                <input type="radio" onchange="bookingType()" name="bookingType" value='C'> Customize
                            </div>
                        </div>
                        <div id='content_wrapper'>

                        </div>
					</div>			
				</div>
                <div class='modal-footer'>
                
                </div>
			</div>
		</div>
	</div>
	<!-- //modal -->