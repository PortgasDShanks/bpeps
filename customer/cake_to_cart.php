<?php 
$id = $_GET['id'];

$query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_cake WHERE  cake_id = '$id'"));
?>
<input type="hidden" value="<?=$id?>" id='dsgnID' name="">
<div class="col-md-12 animated slideInLeft" style="margin-top: 10px;">
   <span class="fa fa-shopping-cart" style="float: right;cursor: pointer;" onclick='viewBookingCart()'><span class="badge badge-secondary" style="position: absolute;bottom: 8px;right: 2px;border-radius: 50%;"><?=countCartContent_package($userID)?></span></span>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<div class="col-md-6 animated slideInLeft" style="padding: 5px">
	<div class="col-md-12">
		<img src="../assets/images/<?=$query['cake_img']?>" style='width: 100%;border-radius: 5%'>
	</div>
	
</div>
<div class="col-md-6 animated slideInLeft">
	<label><h6>Item Details</h6></label>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td>Name</td>
				<td><?=$query['cake_name']?></td>
			</tr>
			<tr>
				<td>Size</td>
				<td>
					<select class="form-control" id="size" onchange='getCakePrice()'>
						<option>&mdash; Please Choose &mdash; </option>
						<?php 
						$size = mysql_query("SELECT * FROM tbl_cake_sizes WHERE cake_id = '$id'");
						while($r = mysql_fetch_array($size)){
						?>
						<option value="<?=$r['size_id']?>"><?=$r['size']?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Price</td>
				<td><span id="cakePrice"></span></td>
			</tr>
			<tr>
				<td>Quantity</td>
				<td><input type="number" id="equipQntty" onkeyup="sumTotalAmount()" class="form-control" name=""><input type="hidden" id="cake_price" name=""></td>
			</tr>
			<tr>
				<td>Total Price</td>
				<td><span id="totalAmount"></span></td>
			</tr>
		</tbody>
	</table>
	<button class="btn btn-sm btn-block btn-success" onclick='addtocart()'><span class="fa fa-shopping-cart"></span> Add to my booking</button>
	<button class="btn btn-sm btn-block btn-success" onclick='window.location="index.php?view=add-additional-items"'><span class="fa fa-shopping-cart"></span> Continue Adding to my booking </button>
</div>
<script type="text/javascript">
	function addtocart(){
		var dsgnID = $("#dsgnID").val();
		var equipQntty= $("#equipQntty").val();
		var equipPrice = $("#cake_price").val();
		var type = 'cake';
		var is_package = 1;
		$("#designBtn").prop("disabled", true);
		$("#designBtn").html("<span class='fa fa-spin fa-spinner'></span> Loading");
		$.post("../admin/ajax/addEquipToCart.php", {
			design_id: dsgnID,
			type: type,
			equipQntty: equipQntty,
			equipPrice: equipPrice,
			is_package: is_package
		}, function(data){
			if(data > 0){
				swal({
	              title: "All Good!",
	              text: "Cake successfully added to your booking",
	              type: "success"
	          	}, function(){
	           		window.location = 'index.php?view=add-additional-items';
	          	});
			}else{
				swal("Something went wrong, Please try again later.");
			}
		})
	}
	function sumTotalAmount(){
		var equipPrice = $("#cake_price").val();
		var equipQntty = $("#equipQntty").val();

		var total = equipPrice * equipQntty;
		$("#totalAmount").text(total);
	}
	function viewBookingCart(){
		window.location = 'index.php?view=view-package-cart';
	}
	function getCakePrice(){
		var size = $("#size").val();
		$.post("../admin/ajax/getCakePrice.php", {
			size: size
		}, function(data){
			$("#cakePrice").text(data);
			$("#cake_price").val(data);
		})
	}
</script>