<?php 
$id = $_GET['id'];

$query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_package_header as p, tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.theme_id = t.theme_id AND p.package_header_id = '$id' AND p.package_header_id = h.package_header_id AND p.package_venue = v.venue_id"));
?>
<input type="hidden" value="<?=$id?>" id='pkgID' name="">
<div class="row animated slideInLeft">
	<div class="col-md-12" style="margin-top: 10px;">
   <span class="fa fa-shopping-cart" style="float: right;cursor: pointer;" onclick='viewBookingCart()'><span class="badge badge-secondary" style="position: absolute;bottom: 8px;right: 2px;border-radius: 50%;"><?=countCartContent_package($userID)?></span></span>
</div>
<div class="col-md-12" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<div class="col-md-6" style="padding: 5px">
	<div class="col-md-12">
		<img src="../assets/images/<?=$query['package_img']?>" style='width: 100%;border-radius: 5%'>
	</div>
	<div class="col-md-12">
		<button class="btn btn-sm btn-block btn-success" onclick='addtocart()'><span class="fa fa-shopping-cart"></span> Add to my booking</button>
	</div>
</div>
<div class="col-md-6">
	<label><h5>Package Details</h5></label>
	<br>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td>Theme Name</td>
				<td><?=$query['theme_name']?></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><?=$query['package_desc']?></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><?=$query['package_price']?></td>
			</tr>
		</tbody>
	</table>
	<label><h5>Inclusions</h5></label>
	<br>
	<label><h6>Equipments</h6></label>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td></td>
				<td>Quantity</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_equipment_designs as d WHERE h.package_item = d.design_id AND package_header_id = '$id' AND cat = 'I'");
				while($equip_row = mysql_fetch_array($equip)){
			?>
			<tr>
				<td><?=$equip_row['design_name']?></td>
				<td><?=$equip_row['item_qntty']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<label><h6>Invitational Cards</h6></label>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td></td>
				<td>Quantity</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_invitational_cards as d WHERE h.package_item = d.ic_id AND package_header_id = '$id' AND cat = 'IC'");
				while($equip_row = mysql_fetch_array($equip)){
			?>
			<tr>
				<td><?=$equip_row['ic_name']?></td>
				<td><?=$equip_row['item_qntty']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<label><h6>Giveaways</h6></label>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td></td>
				<td>Quantity</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_giveaways as d WHERE h.package_item = d.giveaway_id AND package_header_id = '$id' AND cat = 'G'");
				while($equip_row = mysql_fetch_array($equip)){
			?>
			<tr>
				<td><?=$equip_row['giveaway_name']?></td>
				<td><?=$equip_row['item_qntty']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<label><h6>Cakes</h6></label>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td></td>
				<td>Quantity</td>
				<td>Size</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_cake as d, tbl_cake_sizes as s WHERE h.package_item = s.size_id AND package_header_id = '$id' AND cat = 'C' AND d.cake_id = s.cake_id");
				while($equip_row = mysql_fetch_array($equip)){
			?>
			<tr>
				<td><?=$equip_row['cake_name']?></td>
				<td><?=$equip_row['item_qntty']?></td>
				<td><?=$equip_row['size']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<label><h6>freebies</h6></label>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td></td>
				<td>Quantity</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_freebies as d WHERE h.package_item = d.fb_id AND package_header_id = '$id' AND cat = 'F'");
				while($equip_row = mysql_fetch_array($equip)){
			?>
			<tr>
				<td><?=$equip_row['fb_name']?></td>
				<td><?=$equip_row['item_qntty']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<label><h6>Entertainments</h6></label>
	<table class="table table-bordered">
		<thead>
			<tr>
				<td></td>
				<td>Quantity</td>
			</tr>
		</thead>
		<tbody>
			<?php 
				$equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_entertainment as d WHERE h.package_item = d.ent_id AND package_header_id = '$id' AND cat = 'E'");
				while($equip_row = mysql_fetch_array($equip)){
			?>
			<tr>
				<td><?=$equip_row['ent_type']?></td>
				<td><?=$equip_row['item_qntty']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
</div>
<script type="text/javascript">
	function addtocart(){
		var pkgID = $("#pkgID").val();
		$.post("../admin/ajax/package_to_cart.php", {
			pkgID: pkgID
		}, function(data){
			if(data > 0){
				swal({
		              title: "All Good!",
		              text: "Package successfully added to your booking",
		              type: "success"
		          }, function(){
		           	window.location = 'index.php?view=view-package-cart';
		          });
			}else{
				swal("Something went wrong, Please try again later.");
			}
			
		})
	}
	function viewBookingCart(){
		window.location = 'index.php?view=view-package-cart';
	}
</script>