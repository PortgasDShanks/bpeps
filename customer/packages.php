<?php 

?>
<div class="event-time " id="packages">
    <div class="container">
        <h3 class="heading-agileinfo aos-init aos-animate" data-aos="zoom-in">Packages <span>Events Is A Professionally Managed Event</span></h3>
        <div class="testi-info">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <?php 
                $header_type = mysql_query("SELECT * FROM tbl_party_types");
                $count1= 1;
                while($h_row = mysql_fetch_array($header_type)){
                    $active2 = ($count1 == 1)?"active":"";
                ?>
                <li role="presentation" class="<?=$active2?>">
                    <a href="#<?=$h_row['party_type_id']?>" aria-controls="<?=$h_row['party_type_id']?>" role="tab" data-toggle="tab" aria-expanded="false"><?=$h_row['type_name']?></a>
                </li>
                <?php $count1++; } ?>
                
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <?php 
                $header_type = mysql_query("SELECT * FROM tbl_party_types");
                $count = 1;
                while($h_row = mysql_fetch_array($header_type)){
                    $typeID = $h_row['party_type_id'];
                    $active = ($count == 1)?"active":"";
                ?>
                <div role="tabpanel" class="tab-pane <?=$active?>" id="<?=$h_row['party_type_id']?>">
                    <div class="eventmain-info">
                        <div class="event-subinfo">
                            <?php 
                            $getPkg = mysql_query("SELECT * FROM tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.package_venue = v.venue_id AND h.theme_id = t.theme_id AND h.type_id = '$typeID'");
                            while($p_row = mysql_fetch_array($getPkg)){

                            ?>
                            <div class="col-md-6  w3-latest-grid">
                                <div class="col-md-6 col-xs-6 event-right eventtxt-right aos-init aos-animate" data-aos="fade-down">
                                   <img src="assets/images/<?=$p_row['package_img']?>" class="img-responsive" alt="" style='height: 200px;object-fit: cover;'>
                                </div>
                                <div class="col-md-6 col-xs-6 event-left aos-init aos-animate" data-aos="fade-right">
                                    <h5><?=$p_row['theme_name']?></h5>
                                    <p><?=$p_row['package_desc']?></p>
                                    <h6>
                                        <span class="icon-event" aria-hidden="true">venue:</span> <?=$p_row['venue_name']?>
                                    </h6>
                                    <?php
                                    if(isset($_SESSION['cust_user_id'])){ 
                                    ?>
                                    <a href="#" onclick='window.location="customer/index.php?view=add-to-cart-package&id=<?=$p_row['package_header_id']?>"'>Book Now
                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <?php } ?>
                            <div class="clearfix"> </div>
                        </div>
                        
                    </div>
                </div>
                <?php $count++; } ?>
            </div>
        </div>
    </div>
</div>