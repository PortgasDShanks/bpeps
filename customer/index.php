<?php 
include '../admin/main/configuration.php';
if(!isset($_SESSION['cust_user_id'])){
  $display = "display: none";
  $signin = "display: block";
  $viewMore = "data-toggle='modal' data-target='#signIN'";
}else{
  $display = "display: block";
  $signin = "display: none";
  $userID = $_SESSION['cust_user_id'];
  $viewMore = "onclick='viewMoreDetails()'";
}
// $user_id = $_SESSION['user_id'];
// $cat = $_SESSION['user_access'];

// $admin = ($cat == 0)?"Administrator":"User";
// checkSession();

$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';
?>
<!DOCTYPE HTML>
<html>
<head>
<title>BPEPS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<link href="../assets/images/logo.png" rel="icon">
<link href="../assets/images/logo.png" rel="apple-touch-icon">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- CSS -->
<link href="../assets/customer_assets/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="../assets/admin_assets/css/font-awesome.css" rel="stylesheet">
<link href="../assets/customer_assets/css/animate.css" rel="stylesheet">
<link href="../assets/admin_assets/css/dataTables.bootstrap4.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../assets/admin_assets/css/sweetalert.css">
<link href='../assets/admin_assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='../assets/admin_assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
  
<script src="../assets/customer_assets/js/jquery-2.2.3.min.js"></script>
<script src="../assets/customer_assets/js/popper.min.js"></script>
<script src="../assets/customer_assets/js/bootstrap.min.js"></script>
<script src="../assets/admin_assets/js/jquery.dataTables.js"></script>
<script src="../assets/admin_assets/js/dataTables.bootstrap4.js"></script>
<script src="../assets/admin_assets/js/sweetalert.min.js"></script>
<script src='../assets/admin_assets/fullcalendar/lib/moment.min.js'></script>
<script src="../assets/admin_assets/fullcalendar/fullcalendar.min.js"></script>
<style>
body{
    /*background: url(../assets/images/partyBanner5.jpg) no-repeat 0px 0px;
    background-size: cover;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    -ms-background-size: cover;*/

}
.footer {
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: red;
  color: white;
  text-align: center;
}
.dropdown-menu a:hover{
  background-color: #71757952;
}
</style>
</head> 
<body class="cbp-spmenu-push">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark justify-content-between">
  <a class="navbar-brand" href="#">
    <img src="../assets/images/logo.png" style="height: 30px;width: 30px;object-fit: cover">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">\
      <li class="nav-item">
        <a class="nav-link" href="index.php?view=transactions">
          <span class="fa fa-th-list"></span> Transactions 
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <span class="fa fa-th-list"></span> Packages
        </a>
        <div class="dropdown-menu" style="left: 200px;width: 250px;background-color: #343a40;" aria-labelledby="navbarDropdown">
          <?php
          $party = mysql_query("SELECT * FROM tbl_party_types");
          while($p_row = mysql_fetch_array($party)){ 
          ?>
          <a class="dropdown-item" href="#" onclick='window.location="index.php?view=packages&id=<?=$p_row['party_type_id']?>"' style="color: #fff"><?=$p_row['type_name']?></a>
          <?php } ?>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?view=costumize"><span class="fa fa-th-list"></span> Costumize</a>
      </li>
      
    </ul>
    <span class="navbar-text">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         <span class="fa fa-user"></span> Hi , <?php echo $_SESSION['firstname']; ?>
        </a>
        <div class="dropdown-menu" style="left: 80%;width: 250px;background-color: #343a40;" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?view=calendar" style="color: #fff">
            <span class="fa fa-calendar"></span> Calendar
          </a>
          <a class="dropdown-item" href="index.php?view=messages" style="color: #fff">
            <span class="fa fa-wechat"></span> Messages
          </a>
          <a class="dropdown-item" href="index.php?view=profile" style="color: #fff">
           <span class="fa fa-user"></span> Profile
          </a>
          <a class="dropdown-item" href="#" onclick="window.location='../index.php'" style="color: #fff">
            <span class="fa fa-arrow-left"></span> Go Back
          </a>
        </div>
      </li>
    </ul>
    </span>
  </div>
</nav>
<div class="container">
    <div class='row'>
      <?php include 'routes.php'; ?>
    </div>
</div>
</body>
</html>
