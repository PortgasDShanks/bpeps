<style type="text/css">

</style>
<link href="../assets/customer_assets/css/style_carousel.css" rel='stylesheet' type='text/css' />
<div class="col-md-12 animated flipInX">
	<h5 class="card-title">Costumize Booking</h5>
</div>
<div class="col-md-12 animated slideInLeft" style="margin-top: 10px;">
   <span class="fa fa-shopping-cart" onclick='viewBookingCart()' style="float: right;;cursor: pointer;"><span class="badge badge-secondary" style="position: absolute;bottom: 8px;right: 2px;border-radius: 50%;"><?=countCartContent_customize($userID)?></span></span>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>

<div class="accordion animated slideInLeft" id="accordionExample" style="padding: 10px">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h2 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#equipmentCollapse" aria-expanded="true" aria-controls="equipmentCollapse">
          Equipments
        </button>
      </h2>
    </div>

    <div id="equipmentCollapse" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
        <?php 
      $equipments = mysql_query("SELECT * FROM tbl_equipments");
      while($equip = mysql_fetch_array($equipments)){
        $id = $equip['equipment_id'];
      ?>
      <div class="col-md-12" style="border: 1px solid #efeeef;padding: 1%;">
        <h6 style="text-align: center;"><?=$equip['equip_name']?></h6>
        <div class="row mx-auto my-auto">
        <div id="myCarousel<?=$id?>" class="carousel slide w-100" data-ride="carousel">
              <div class="carousel-inner w-100" role="listbox">
                  <?php 
                  $equip_design = mysql_query("SELECT * FROM tbl_equipment_designs WHERE equipment_id = '$id'");
                  $count = 1;
                  while($design_row = mysql_fetch_array($equip_design)){
                    $active = ($count == 1)?"active":"";
                    $designID = $design_row['design_id'];
                    $has_price = mysql_fetch_array(mysql_query("SELECT * FROM tbl_pricing WHERE item_id = '$designID' AND item_cat = 'E'"));
                    if(!empty($has_price['item_id'])){
                  ?>
                  <div class="carousel-item <?=$active?>">
                      <div class="col-lg-4 col-md-8">
                          <img style="height: 200px;width: 100%;object-fit: cover;border-radius: 5%" class="img-fluid" src="../assets/images/<?=$design_row['design_img']?>">
                          <h6><?=$design_row['design_name']?></h6>
                          <button id="designBtn<?=$design_row['design_id']?>" class="btn btn-sm btn-success btn-block" onclick='addEquipmentstoCart(<?=$design_row["design_id"]?>)'><span class="fa fa-shopping-cart"></span> Add to my booking</button>
                      </div>
                  </div>
                <?php } $count++; } ?>
              </div>
              <a class="carousel-control-prev bg-dark w-auto" href="#myCarousel<?=$id?>" role="button" data-slide="prev" style="width: 2%">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next bg-dark w-auto" href="#myCarousel<?=$id?>" role="button" data-slide="next" style="width: 2%">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
              </a>
          </div>
      </div>
      </div>
      <hr>
      <?php } ?>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#invitationalCollapse" aria-expanded="false" aria-controls="invitationalCollapse">
          Invitational Cards
        </button>
      </h2>
    </div>
    <div id="invitationalCollapse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-12" style="border: 1px solid #efeeef;padding: 1%;">
      <div class="row mx-auto my-auto">
      <div id="myCarousel_ic" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner w-100" role="listbox">
                <?php 
                $ic_design = mysql_query("SELECT * FROM tbl_invitational_cards");
                $count2 = 1;
                while($ic_row = mysql_fetch_array($ic_design)){
                  $active2 = ($count2 == 1)?"active":"";
                  $icID = $ic_row['ic_id'];
                  $has_price = mysql_fetch_array(mysql_query("SELECT * FROM tbl_pricing WHERE item_id = '$icID' AND item_cat = 'I'"));
                  if(!empty($has_price['item_id'])){
                ?>
                <div class="carousel-item <?=$active2?>">
                    <div class="col-lg-4 col-md-8">
                        <img style="height: 200px;width: 100%;object-fit: cover;border-radius: 5%" class="img-fluid" src="../assets/images/<?=$ic_row['ic_image']?>">
                        <h6><?=$ic_row['ic_name']?></h6>
                        <button onclick='addInvCardtoCart(<?=$ic_row["ic_id"]?>)' class="btn btn-sm btn-success btn-block"><span class="fa fa-shopping-cart"></span> Add to my booking</button>
                    </div>
                </div>
              <?php } $count2++; } ?>
            </div>
            <a class="carousel-control-prev bg-dark w-auto" href="#myCarousel_ic" role="button" data-slide="prev" style="width: 2%">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next bg-dark w-auto" href="#myCarousel_ic" role="button" data-slide="next" style="width: 2%">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#giveawayCollapse" aria-expanded="false" aria-controls="giveawayCollapse">
          Giveaways
        </button>
      </h2>
    </div>
    <div id="giveawayCollapse" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-12" style="border: 1px solid #efeeef;padding: 1%;">
      <div class="row mx-auto my-auto">
      <div id="ga_carousel" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner w-100" role="listbox">
                <?php 
                $ga_list = mysql_query("SELECT * FROM tbl_giveaways");
                $ga_count = 1;
                while($giveaway_row = mysql_fetch_array($ga_list)){
                  $giveaway_active = ($ga_count == 1)?"active":"";
                  $gaID = $giveaway_row['giveaway_id'];
                  $has_price = mysql_fetch_array(mysql_query("SELECT * FROM tbl_pricing WHERE item_id = '$gaID' AND item_cat = 'G'"));
                  if(!empty($has_price['item_id'])){
                ?>
                <div class="carousel-item <?=$giveaway_active?>">
                    <div class="col-lg-4 col-md-8">
                        <img style="height: 200px;width: 100%;object-fit: cover;border-radius: 5%" class="img-fluid" src="../assets/images/<?=$giveaway_row['giveaway_img']?>">
                        <h6><?=$giveaway_row['giveaway_name']?></h6>
                        <button onclick='addgiveawaytoCart(<?=$giveaway_row["giveaway_id"]?>)' class="btn btn-sm btn-success btn-block"><span class="fa fa-shopping-cart"></span> Add to my booking</button>
                    </div>
                </div>
              <?php } $ga_count++; } ?>
            </div>
            <a class="carousel-control-prev bg-dark w-auto" href="#ga_carousel" role="button" data-slide="prev" style="width: 2%">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next bg-dark w-auto" href="#ga_carousel" role="button" data-slide="next" style="width: 2%">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
      </div>
    </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h2 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#cakeCollapse" aria-expanded="false" aria-controls="cakeCollapse">
          Cakes
        </button>
      </h2>
    </div>
    <div id="cakeCollapse" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div class="col-md-12" style="border: 1px solid #efeeef;padding: 1%;">
      <div class="row mx-auto my-auto">
      <div id="ga_carousel" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner w-100" role="listbox">
                <?php 
                $cake_list = mysql_query("SELECT * FROM tbl_cake");
                $cake_count = 1;
                while($cake_row = mysql_fetch_array($cake_list)){
                  $cake_active = ($cake_count == 1)?"active":"";
                ?>
                <div class="carousel-item <?=$cake_active?>">
                    <div class="col-lg-4 col-md-8">
                        <img style="height: 200px;width: 100%;object-fit: cover;border-radius: 5%" class="img-fluid" src="../assets/images/<?=$cake_row['cake_img']?>">
                        <h6><?=$cake_row['cake_name']?></h6>
                        <button onclick='addcaketoCart(<?=$cake_row["cake_id"]?>)' class="btn btn-sm btn-success btn-block"><span class="fa fa-shopping-cart"></span> Add to my booking</button>
                    </div>
                </div>
              <?php $cake_count++; } ?>
            </div>
            <a class="carousel-control-prev bg-dark w-auto" href="#ga_carousel" role="button" data-slide="prev" style="width: 2%">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next bg-dark w-auto" href="#ga_carousel" role="button" data-slide="next" style="width: 2%">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
      </div>
    </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="../assets/customer_assets/js/carousel.js"></script>
<script type="text/javascript">
  function addEquipmentstoCart(design_id){
    window.location = 'index.php?view=equipment-to-cart-customize&id='+design_id; 
  }
  function addInvCardtoCart(ic_id){
    window.location = 'index.php?view=invcard-to-cart-customize&id='+ic_id;
  }
  function addgiveawaytoCart(ga_id){
    window.location = 'index.php?view=giveaway-to-cart-customize&id='+ga_id;
  }
  function addcaketoCart(cake_id){
    window.location = 'index.php?view=cake-to-cart-customize&id='+cake_id;
  }
   function viewBookingCart(){
    window.location = 'index.php?view=view-costumize-cart';
  }
</script>