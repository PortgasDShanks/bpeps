<style type="text/css">
 #calendar{
  height: auto;
 }
</style>
<link href="../assets/customer_assets/css/style_carousel.css" rel='stylesheet' type='text/css' />
<div class="col-md-12 animated flipInX">
	<h5 class="card-title">Event Calendar</h5>
</div>
<div class="col-md-12 animated slideInLeft" style="margin-top: 10px;">
   <span class="fa fa-shopping-cart" onclick='viewBookingCart()' style="float: right;;cursor: pointer;"><span class="badge badge-secondary" style="position: absolute;bottom: 8px;right: 2px;border-radius: 50%;"><?=countCartContent_customize($userID)?></span></span>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<div class="col-md-12" style="padding-top: 10px">
  <div class="row">
    <div class="col-md-3"><div class="alert alert-success"> Available </div></div>
    <div class="col-md-3"><div class="alert alert-danger"> Fully Booked </div></div>
  </div>
  
  <div id="calendar"></div>
</div>

<script type="text/javascript">
  $(document).ready( function(){
    DisplayDateAvail();
  })
  function getDateStatus(date_new,cell){
    $.post("../admin/ajax/getDateStatus.php", {
      date_new: date_new
    }, function(data){
      if(data >= 3){
        cell.css("background-color", "#f8d7da");
      }else{
        cell.css("background-color", "#d4edda");
      }
    })
  }
  function DisplayDateAvail(){
    $('#calendar').fullCalendar({
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
      dayRender: function (date, cell) {
        //alert(date)
       var newdate = new Date(date);
       yr      = newdate.getFullYear(),
       month   = newdate.getMonth() < 10 ? '0' + newdate.getMonth() : newdate.getMonth(),
       day     = newdate.getDate()  < 10 ? '0' + newdate.getDate()  : newdate.getDate(),
       newDate = yr + '-' + month + '-' + day;
       getDateStatus(newDate, cell);
      },
      eventConstraint: {
          start: moment().format('YYYY-MM-DD'),
          end: '2100-01-01'
      }
    });
  }
</script>