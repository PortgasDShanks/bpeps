<?php
$trans_id = $_GET['id'];
?>
<style type="text/css">
h1 span{
  font-weight: 300;
  color: #Fd4;
}

div.stars{
  width: 270px;
  display: inline-block;
}

input.star{
  display: none;
}

label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}

input.star:checked ~ label.star:before {
  content:'\f005';
  color: #FD4;
  transition: all .25s;
}


input.star-5:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}

input.star-1:checked ~ label.star:before {
  color: #F62;
}

label.star:hover{
  transform: rotate(-15deg) scale(1.3);
}

label.star:before{
  content:'\f006';
  font-family: FontAwesome;
}

.rev-box{
  overflow: hidden;
  height: 0;
  width: 100%;
  transition: all .25s;
}

textarea.review1{
  background: #222;
  border: none;
  width: 100%;
  max-width: 100%;
  height: 100px;
  padding: 10px;
  box-sizing: border-box;
  color: #EEE;
}

label.review1{
  display: block;
  transition:opacity .25s;
}

textarea.review{
  background: #222;
  border: none;
  width: 100%;
  max-width: 100%;
  height: 100px;
  padding: 10px;
  box-sizing: border-box;
  color: #EEE;
}

label.review{
  display: block;
  transition:opacity .25s;
}



input.star:checked ~ .rev-box{
  height: 125px;
  overflow: visible;
}
</style>
<div class="col-md-12 animated flipInX">
  <h5 class="card-title">Rate Us</h5>
</div>
<div class="col-md-12 animated slideInLeft" style="border: 1px solid #9a979a29;margin-top: 1%;"></div>
<div class="col-md-12 animated slideInLeft">
    <input id="rateValue1" type="hidden" readonly="">
    <input id="transid" type="hidden" value="<?=$trans_id?>">
      <div class="stars">
        <input class="star star-5" id="star-5-2" type="radio" name="star" onclick="rateValue1(5)"/>
        <label class="star star-5" for="star-5-2"></label>
        <input class="star star-4" id="star-4-2" type="radio" name="star" onclick="rateValue1(4)"/>
        <label class="star star-4" for="star-4-2"></label>
        <input class="star star-3" id="star-3-2" type="radio" name="star" onclick="rateValue1(3)"/>
        <label class="star star-3" for="star-3-2"></label>
        <input class="star star-2" id="star-2-2" type="radio" name="star" onclick="rateValue1(2)"/>
        <label class="star star-2" for="star-2-2"></label>
        <input class="star star-1" id="star-1-2" type="radio" name="star" onclick="rateValue1(1)"/>
        <label class="star star-1" for="star-1-2"></label>
        <button class="btn btn-sm btn-success" onclick='sendRatings()'><span class="fa fa-star"></span> Rate Now</button>
      </div>
    
</div>
<script type="text/javascript">
  function sendRatings(){
    var rateValue1 = $("#rateValue1").val();
    var transid = $("#transid").val();
    $.post("../admin/ajax/sendRatings.php", {
      rateValue1: rateValue1,
      transid: transid
    }, function(data){
      if(data > 0){
        swal({
              title: "All Good!",
              text: "Ratings successfully sent!",
              type: "success"
          }, function(){
           window.location = 'index.php?view=transactions';
          });
      }else{
        swal("Can't Rate this time, Please try again later");
      }
    });
  }
   function rateValue1(id){
    $("#rateValue1").val(id)
  }
</script>