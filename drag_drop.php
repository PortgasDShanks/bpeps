<?php 
include('admin/main/configuration.php');
if(!isset($_SESSION['cust_user_id'])){
	$display = "display: none";
	$signin = "display: block";
	$viewMore = "data-toggle='modal' data-target='#signIN'";
}else{
	$display = "display: block";
	$signin = "display: none";
	$userID = $_SESSION['cust_user_id'];
	$viewMore = "onclick='viewMoreDetails()'";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>BPEPS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="assets/customer_assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="assets/customer_assets/css/style.css" type="text/css" media="all" />
<link href="assets/customer_assets/css/jQuery.lightninBox.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="assets/customer_assets/css/flexslider.css" type="text/css" media="screen" property="" />

<link href="assets/customer_assets/css/aos.css" rel="stylesheet" type="text/css" media="all" />
<link href="assets/customer_assets/css/font-awesome.css" rel="stylesheet"> 
<link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Oswald:400,500,600,700" rel="stylesheet">
<link href="assets/admin_assets/css/dataTables.bootstrap4.css" rel="stylesheet">


<script src="assets/customer_assets/js/jquery-2.2.3.min.js"></script>
<script src="assets/customer_assets/js/bootstrap.js"></script>

</head>
<style>
#div1, #div2 {
  float: left;
  width: 100px;
  height: 35px;
  margin: 10px;
  padding: 10px;
  border: 1px solid black;
}
</style>
<body>

<h2>Drag and Drop</h2>
<p>Drag the image back and forth between the two div elements.</p>

<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)">
  <img src="img_w3slogo.gif" draggable="true" ondragstart="drag(event)" id="drag1" width="88" height="31">
</div>

<div id="div2" ondrop="drop(event)" ondragover="allowDrop(event)"></div>



<script>
function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
}
</script>
</body>	
</html>