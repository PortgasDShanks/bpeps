-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2020 at 04:30 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpeps_2020_erd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cake`
--

CREATE TABLE `tbl_cake` (
  `cake_id` int(11) NOT NULL,
  `cake_img` varchar(50) NOT NULL DEFAULT '',
  `cake_name` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cake_sizes`
--

CREATE TABLE `tbl_cake_sizes` (
  `size_id` int(11) NOT NULL,
  `size` varchar(10) NOT NULL DEFAULT '',
  `cake_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_design_colors`
--

CREATE TABLE `tbl_design_colors` (
  `color_id` int(11) NOT NULL,
  `design_id` int(11) NOT NULL,
  `color` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_entertainment`
--

CREATE TABLE `tbl_entertainment` (
  `ent_id` int(11) NOT NULL,
  `ent_type` varchar(50) NOT NULL DEFAULT '',
  `ent_fee` decimal(12,3) NOT NULL,
  `ent_hrs` int(11) NOT NULL,
  `ent_add_fee` decimal(12,3) NOT NULL,
  `ent_add_fee_hrs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equipments`
--

CREATE TABLE `tbl_equipments` (
  `equipment_id` int(11) NOT NULL,
  `equip_name` varchar(100) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equipment_category`
--

CREATE TABLE `tbl_equipment_category` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equipment_designs`
--

CREATE TABLE `tbl_equipment_designs` (
  `design_id` int(11) NOT NULL,
  `design_img` varchar(50) NOT NULL DEFAULT '',
  `design_name` varchar(50) NOT NULL DEFAULT '',
  `design_desc` text NOT NULL,
  `equipment_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_freebies`
--

CREATE TABLE `tbl_freebies` (
  `fb_id` int(11) NOT NULL,
  `fb_name` varchar(100) NOT NULL DEFAULT '',
  `fb_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_giveaways`
--

CREATE TABLE `tbl_giveaways` (
  `giveaway_id` int(11) NOT NULL,
  `giveaway_name` varchar(50) NOT NULL DEFAULT '',
  `giveaway_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_invitational_cards`
--

CREATE TABLE `tbl_invitational_cards` (
  `ic_id` int(11) NOT NULL,
  `ic_name` varchar(50) NOT NULL DEFAULT '',
  `ic_image` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_messages`
--

CREATE TABLE `tbl_messages` (
  `message_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_content` text NOT NULL,
  `message_datetime` datetime NOT NULL,
  `message_status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_details`
--

CREATE TABLE `tbl_package_details` (
  `package_detail_id` int(11) NOT NULL,
  `package_header_id` int(11) NOT NULL,
  `package_item` int(11) NOT NULL,
  `item_qntty` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `cat` varchar(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_package_header`
--

CREATE TABLE `tbl_package_header` (
  `package_header_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `package_desc` text NOT NULL,
  `package_venue` int(11) NOT NULL,
  `package_price` decimal(12,3) NOT NULL,
  `package_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pricing`
--

CREATE TABLE `tbl_pricing` (
  `price_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price_amount` decimal(12,3) NOT NULL,
  `item_cat` varchar(2) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_themes`
--

CREATE TABLE `tbl_themes` (
  `theme_id` int(11) NOT NULL,
  `theme_name` varchar(100) NOT NULL DEFAULT '',
  `theme_img` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactions`
--

CREATE TABLE `tbl_transactions` (
  `trans_id` int(11) NOT NULL,
  `ref_number` varchar(20) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `trans_date` date NOT NULL,
  `trans_time_from` time NOT NULL,
  `trans_time_to` time NOT NULL,
  `venue` int(11) NOT NULL,
  `theme` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `mode_of_payment` varchar(20) NOT NULL DEFAULT '',
  `is_package` int(1) NOT NULL,
  `package_id` int(11) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '',
  `total_amount` decimal(12,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transaction_detail`
--

CREATE TABLE `tbl_transaction_detail` (
  `trans_detail_id` int(11) NOT NULL,
  `trans_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `price` decimal(12,3) NOT NULL,
  `quantity` decimal(12,3) NOT NULL,
  `total` decimal(12,3) NOT NULL,
  `arrival_time_from` time NOT NULL,
  `arrival_from_to` time NOT NULL,
  `item_cat` varchar(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `contact_no` varchar(11) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `category` varchar(2) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_venue`
--

CREATE TABLE `tbl_venue` (
  `venue_id` int(11) NOT NULL,
  `venue_name` varchar(50) NOT NULL DEFAULT '',
  `venue_address` text NOT NULL,
  `latitude` varchar(30) NOT NULL DEFAULT '',
  `longitude` varchar(30) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_cake`
--
ALTER TABLE `tbl_cake`
  ADD PRIMARY KEY (`cake_id`);

--
-- Indexes for table `tbl_cake_sizes`
--
ALTER TABLE `tbl_cake_sizes`
  ADD PRIMARY KEY (`size_id`),
  ADD UNIQUE KEY `cake_id` (`cake_id`);

--
-- Indexes for table `tbl_design_colors`
--
ALTER TABLE `tbl_design_colors`
  ADD PRIMARY KEY (`color_id`),
  ADD UNIQUE KEY `design_id` (`design_id`);

--
-- Indexes for table `tbl_entertainment`
--
ALTER TABLE `tbl_entertainment`
  ADD PRIMARY KEY (`ent_id`);

--
-- Indexes for table `tbl_equipments`
--
ALTER TABLE `tbl_equipments`
  ADD PRIMARY KEY (`equipment_id`),
  ADD UNIQUE KEY `equip_name` (`equip_name`),
  ADD UNIQUE KEY `added_by` (`added_by`),
  ADD UNIQUE KEY `cat_id` (`cat_id`);

--
-- Indexes for table `tbl_equipment_category`
--
ALTER TABLE `tbl_equipment_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_equipment_designs`
--
ALTER TABLE `tbl_equipment_designs`
  ADD PRIMARY KEY (`design_id`),
  ADD UNIQUE KEY `equipment_id` (`equipment_id`);

--
-- Indexes for table `tbl_freebies`
--
ALTER TABLE `tbl_freebies`
  ADD PRIMARY KEY (`fb_id`);

--
-- Indexes for table `tbl_giveaways`
--
ALTER TABLE `tbl_giveaways`
  ADD PRIMARY KEY (`giveaway_id`);

--
-- Indexes for table `tbl_invitational_cards`
--
ALTER TABLE `tbl_invitational_cards`
  ADD PRIMARY KEY (`ic_id`);

--
-- Indexes for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD UNIQUE KEY `sender_id` (`sender_id`),
  ADD UNIQUE KEY `receiver_id` (`receiver_id`);

--
-- Indexes for table `tbl_package_details`
--
ALTER TABLE `tbl_package_details`
  ADD PRIMARY KEY (`package_detail_id`),
  ADD UNIQUE KEY `package_header_id` (`package_header_id`),
  ADD UNIQUE KEY `package_item` (`package_item`),
  ADD UNIQUE KEY `color` (`color`);

--
-- Indexes for table `tbl_package_header`
--
ALTER TABLE `tbl_package_header`
  ADD PRIMARY KEY (`package_header_id`),
  ADD UNIQUE KEY `theme_id` (`theme_id`),
  ADD UNIQUE KEY `package_venue` (`package_venue`);

--
-- Indexes for table `tbl_pricing`
--
ALTER TABLE `tbl_pricing`
  ADD PRIMARY KEY (`price_id`),
  ADD UNIQUE KEY `item_id` (`item_id`);

--
-- Indexes for table `tbl_themes`
--
ALTER TABLE `tbl_themes`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  ADD PRIMARY KEY (`trans_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `venue` (`venue`),
  ADD UNIQUE KEY `theme` (`theme`),
  ADD UNIQUE KEY `package_id` (`package_id`);

--
-- Indexes for table `tbl_transaction_detail`
--
ALTER TABLE `tbl_transaction_detail`
  ADD PRIMARY KEY (`trans_detail_id`),
  ADD UNIQUE KEY `trans_id` (`trans_id`),
  ADD UNIQUE KEY `item_id` (`item_id`),
  ADD KEY `item_id_2` (`item_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_venue`
--
ALTER TABLE `tbl_venue`
  ADD PRIMARY KEY (`venue_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_cake`
--
ALTER TABLE `tbl_cake`
  MODIFY `cake_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_cake_sizes`
--
ALTER TABLE `tbl_cake_sizes`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_design_colors`
--
ALTER TABLE `tbl_design_colors`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_entertainment`
--
ALTER TABLE `tbl_entertainment`
  MODIFY `ent_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_equipments`
--
ALTER TABLE `tbl_equipments`
  MODIFY `equipment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_equipment_category`
--
ALTER TABLE `tbl_equipment_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_equipment_designs`
--
ALTER TABLE `tbl_equipment_designs`
  MODIFY `design_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_freebies`
--
ALTER TABLE `tbl_freebies`
  MODIFY `fb_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_giveaways`
--
ALTER TABLE `tbl_giveaways`
  MODIFY `giveaway_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_invitational_cards`
--
ALTER TABLE `tbl_invitational_cards`
  MODIFY `ic_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_package_details`
--
ALTER TABLE `tbl_package_details`
  MODIFY `package_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_package_header`
--
ALTER TABLE `tbl_package_header`
  MODIFY `package_header_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pricing`
--
ALTER TABLE `tbl_pricing`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_themes`
--
ALTER TABLE `tbl_themes`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_transaction_detail`
--
ALTER TABLE `tbl_transaction_detail`
  MODIFY `trans_detail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_venue`
--
ALTER TABLE `tbl_venue`
  MODIFY `venue_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_cake`
--
ALTER TABLE `tbl_cake`
  ADD CONSTRAINT `tbl_cake_ibfk_1` FOREIGN KEY (`cake_id`) REFERENCES `tbl_package_details` (`package_item`);

--
-- Constraints for table `tbl_cake_sizes`
--
ALTER TABLE `tbl_cake_sizes`
  ADD CONSTRAINT `tbl_cake_sizes_ibfk_1` FOREIGN KEY (`size_id`) REFERENCES `tbl_package_details` (`package_item`);

--
-- Constraints for table `tbl_entertainment`
--
ALTER TABLE `tbl_entertainment`
  ADD CONSTRAINT `tbl_entertainment_ibfk_1` FOREIGN KEY (`ent_id`) REFERENCES `tbl_package_details` (`package_item`);

--
-- Constraints for table `tbl_equipments`
--
ALTER TABLE `tbl_equipments`
  ADD CONSTRAINT `tbl_equipments_ibfk_1` FOREIGN KEY (`added_by`) REFERENCES `tbl_users` (`user_id`),
  ADD CONSTRAINT `tbl_equipments_ibfk_2` FOREIGN KEY (`cat_id`) REFERENCES `tbl_equipment_category` (`cat_id`);

--
-- Constraints for table `tbl_equipment_designs`
--
ALTER TABLE `tbl_equipment_designs`
  ADD CONSTRAINT `tbl_equipment_designs_ibfk_1` FOREIGN KEY (`equipment_id`) REFERENCES `tbl_equipments` (`equipment_id`),
  ADD CONSTRAINT `tbl_equipment_designs_ibfk_2` FOREIGN KEY (`design_id`) REFERENCES `tbl_package_details` (`package_item`);

--
-- Constraints for table `tbl_freebies`
--
ALTER TABLE `tbl_freebies`
  ADD CONSTRAINT `tbl_freebies_ibfk_1` FOREIGN KEY (`fb_id`) REFERENCES `tbl_package_details` (`package_item`);

--
-- Constraints for table `tbl_giveaways`
--
ALTER TABLE `tbl_giveaways`
  ADD CONSTRAINT `tbl_giveaways_ibfk_1` FOREIGN KEY (`giveaway_id`) REFERENCES `tbl_package_details` (`package_item`);

--
-- Constraints for table `tbl_invitational_cards`
--
ALTER TABLE `tbl_invitational_cards`
  ADD CONSTRAINT `tbl_invitational_cards_ibfk_1` FOREIGN KEY (`ic_id`) REFERENCES `tbl_package_details` (`package_item`);

--
-- Constraints for table `tbl_messages`
--
ALTER TABLE `tbl_messages`
  ADD CONSTRAINT `tbl_messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `tbl_users` (`user_id`),
  ADD CONSTRAINT `tbl_messages_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `tbl_users` (`user_id`);

--
-- Constraints for table `tbl_package_details`
--
ALTER TABLE `tbl_package_details`
  ADD CONSTRAINT `tbl_package_details_ibfk_1` FOREIGN KEY (`package_header_id`) REFERENCES `tbl_package_header` (`package_header_id`),
  ADD CONSTRAINT `tbl_package_details_ibfk_2` FOREIGN KEY (`color`) REFERENCES `tbl_design_colors` (`color_id`);

--
-- Constraints for table `tbl_pricing`
--
ALTER TABLE `tbl_pricing`
  ADD CONSTRAINT `tbl_pricing_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_giveaways` (`giveaway_id`);

--
-- Constraints for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  ADD CONSTRAINT `tbl_transactions_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`),
  ADD CONSTRAINT `tbl_transactions_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `tbl_package_header` (`package_header_id`),
  ADD CONSTRAINT `tbl_transactions_ibfk_3` FOREIGN KEY (`venue`) REFERENCES `tbl_venue` (`venue_id`),
  ADD CONSTRAINT `tbl_transactions_ibfk_4` FOREIGN KEY (`theme`) REFERENCES `tbl_themes` (`theme_id`);

--
-- Constraints for table `tbl_transaction_detail`
--
ALTER TABLE `tbl_transaction_detail`
  ADD CONSTRAINT `tbl_transaction_detail_ibfk_1` FOREIGN KEY (`trans_id`) REFERENCES `tbl_transactions` (`trans_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
