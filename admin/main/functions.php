<?php

function getCurrentDate(){
	ini_set('date.timezone','UTC');
	date_default_timezone_set('UTC');
	$today = date('H:i:s');
	$date = date('Y-m-d H:i:s', strtotime($today)+28800);
	
	return $date;
}

function clean($str) {
    $str = @trim($str);
    if(get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}

function select_query($type , $table , $params){

	$select_query = mysql_query("SELECT $type FROM $table WHERE $params")or die(mysql_error());
	$fetch = mysql_fetch_assoc($select_query);
	return $fetch;

}

function checkSession(){
	if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	}
}

function insert_query($table_name, $form_data , $last_id){
    $fields = array_keys($form_data);

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    $return_insert = mysql_query($sql)or die(mysql_error());
    $lastID = mysql_insert_id();

    if($last_id == 'Y'){
        if($return_insert){
            $val = $lastID;
        }else{
            $val = 0;
        }
    }else{
        if($return_insert){
            $val = 1;
        }else{
            $val = 0;
        }
    }

    return $val;
}

function delete_query($table_name, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "DELETE FROM ".$table_name.$whereSQL;
    
    $return_delete = mysql_query($sql);
    
    if($return_delete){
    	$val = 1;
    }else{
    	$val = 0;
    }

    return $val;
}

function update_query($table_name, $form_data, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "UPDATE ".$table_name." SET ";
    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);
    $sql .= $whereSQL;

    $return_query = mysql_query($sql) or die(mysql_error());
    if($return_query){
    	$val = 1;
    }else{
    	$val = 0;
    }
    
    return $val;
}
function itexmo($number,$message,$apicode,$password){
    $url = 'https://www.itexmo.com/php_api/api.php';
    $itexmo = array('1' => $number, '2' => $message, '3' => $apicode,'passwd' => $password);
    $param = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($itexmo),
        ),);
    $context  = stream_context_create($param);
    return file_get_contents($url, false, $context);
}
function split_words($string, $nb_caracs, $separator){
    $string = strip_tags(html_entity_decode($string));
    if( strlen($string) <= $nb_caracs ){
        $final_string = $string;
    } else {
        $final_string = "";
        $words = explode(" ", $string);
        foreach( $words as $value ){
            if( strlen($final_string . " " . $value) < $nb_caracs ){
                if( !empty($final_string) ) $final_string .= " ";
                $final_string .= $value;
            } else {
                break;
            }
        }
        $final_string .= "<p data-toggle='tooltip' data-placement='top' title='".$string."'>" . $separator . "<p/>";
    }
    return $final_string;
}
function getVenuePckg($venueID){
    $query = mysql_fetch_array(mysql_query("SELECT venue_name FROM tbl_venue WHERE venue_id = '$venueID'"));

    return $query[0];
}
function GETINCLUSIONLIST($pckgID){

}
function getPackages(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_package_header");
    while($row = mysql_fetch_array($query)){
        $pckgName = $row['package_name'];
        $venue = $row['package_venue'];
        $desc = $row['package_desc'];
        $price = $row['package_price'];
        $img = $row['package_img'];
        echo "<div>";
        echo "<li class='col-md-6'>";
            echo "<div class='thumbnail'>";
                echo "<img src='../assets/images/$img' alt='".$pckgName."' style='object-fit: contain;'>";
                echo "<div class='caption'>";
                echo "<h4>Theme Name: <i>$pckgName</i> </h4>";
                echo "<p>Venue: <i>".getVenuePckg($venue)."</i> </p>";
                echo "<p> Theme Description: <i>$desc</i></p>";
                echo "<p> Price: &#8369; $price</p>";
                echo "<p align='center'> INCLUSIONS </p>";
                echo "<p align='center'><a class='btn btn-primary' id='del' onclick='updatePackage(".$row['package_header_id'].")'><span class='fa fa-pencil'></span> Update</a><a class='btn btn-danger' id='del' onclick='deletePackage(".$row['package_header_id'].")'><span class='fa fa-trash'></span> Delete</a></p>";
                echo "</div>";
            echo "</div>";
        echo "</li>";
        echo "</div>";
    }
}
function getCategories(){
    $data = "<option> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_equipment_category");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['cat_id']."'> ".$row['cat_name']." </option>";
    }

    return $data;
}
function getCategoryName($catID){
    $query = mysql_fetch_array(mysql_query("SELECT cat_name FROM tbl_equipment_category WHERE cat_id = '$catID'"));

    return $query[0];
}
function getDesigns($equipment_id){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_equipment_designs WHERE equipment_id = '$equipment_id'");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['design_id']."'> ".$row['design_name']." </option>";
    }

    return $data;
}
function getVenue(){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_venue");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['venue_id']."'> ".$row['venue_name']." </option>";
    }

    return $data;
}
function getItemDesigns($itemID){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_equipment_designs WHERE equipment_id = '$itemID'");
    while($row = mysql_fetch_array($query)){
        $img = $row['design_img'];
        $name = $row['design_name'];
        $desc = $row['design_desc'];
        $data .= "<li class='col-md-4'>
                    <div class='thumbnail'>
                        <img src='../assets/images/".$img."' alt='".$name."'>
                        <div class='caption'>
                        <h3>$name</h3>
                        <p>$desc</p>
                        <p align='center'><a class='btn btn-danger btn-block' id='del".$row['design_id']."' onclick='deleteDesign(".$row['design_id'].")'><span class='fa fa-trash'></span> Delete</a></p>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getICDesigns(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_invitational_cards");
    while($row = mysql_fetch_array($query)){
        $img = $row['ic_image'];
        $data .= "<li class='col-md-4'>
                    <div class='thumbnail'>
                        <img src='../assets/images/".$img."' alt='".$img."' style='height: 200px;width: 200px;object-fit: contain;'>
                        <div class='caption'>
                        <p align='center'><a class='btn btn-danger btn-block' id='del".$row['ic_id']."' onclick='deleteDesign(".$row['ic_id'].")'><span class='fa fa-trash'></span> Delete</a></p>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getGADesigns(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_giveaways");
    while($row = mysql_fetch_array($query)){
        $img = $row['giveaway_img'];
        $data .= "<li class='col-md-4'>
                    <div class='thumbnail'>
                        <img src='../assets/images/".$img."' alt='".$img."' style='height: 200px;width: 200px;object-fit: contain;'>
                        <div class='caption'>
                        <p align='center'><a class='btn btn-danger btn-block' id='del".$row['giveaway_id']."' onclick='deleteDesign(".$row['giveaway_id'].")'><span class='fa fa-trash'></span> Delete</a></p>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getEquipmentName($equipID){
    $query = mysql_fetch_array(mysql_query("SELECT equip_name FROM tbl_equipments WHERE equipment_id = '$equipID'"));

    return $query[0];
}
function getPriceEquip($designID){
    $query = mysql_fetch_array(mysql_query("SELECT price_amount FROM tbl_pricing WHERE item_id = '$designID' AND item_cat = 'E'"));

    return $query[0];
}
function getVenueEquip($designID){
    $query = mysql_fetch_array(mysql_query("SELECT price_amount FROM tbl_pricing WHERE item_id = '$designID' AND item_cat = 'V'"));

    return $query[0];
}
function getIC($designID){
    $query = mysql_fetch_array(mysql_query("SELECT price_amount FROM tbl_pricing WHERE item_id = '$designID' AND item_cat = 'I'"));

    return $query[0];
}
function getGA($designID){
    $query = mysql_fetch_array(mysql_query("SELECT price_amount FROM tbl_pricing WHERE item_id = '$designID' AND item_cat = 'G'"));

    return $query[0];
}
function getCAKE($designID){
    $query = mysql_fetch_array(mysql_query("SELECT price_amount FROM tbl_pricing WHERE item_id = '$designID' AND item_cat = 'C'"));

    return $query[0];
}
function getPosition($project_id , $member_id){

    $get_memberPosition = mysql_fetch_array(mysql_query("SELECT * FROM pms_project_members as pm , pms_users as u WHERE pm.project_member_id = u.user_id AND pm.project_id = '$project_id' AND project_member_id = '$member_id'"));
        if($get_memberPosition['project_access'] == 'P'){
          $position = 'Team Leader/Project Manager';
        }else if($get_memberPosition['project_access'] == 'M'){
          $position = 'Project Member';
        }else{
          $position = '';
        }
        return $position; 
}

function countMembers($project_id){
    $get_allMembers = mysql_query("SELECT * FROM pms_project_members as pm , pms_users as u WHERE pm.project_member_id = u.user_id AND project_id = '$project_id' AND pm.project_access != 'P'");
    $count_members = mysql_num_rows($get_allMembers);

    return $count_members;
}

function getProjectLeader($project_id){
    $get_leader = mysql_fetch_array(mysql_query("SELECT * FROM pms_project_members as pm , pms_users as u WHERE pm.project_member_id = u.user_id AND project_id = '$project_id' AND pm.project_access = 'P'"));

    return $get_leader; 
}
function getUser($user_id){
    $getUser = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname) as fullname FROM tbl_users WHERE user_id = '$user_id'"));

    return $getUser[0];
}
function getCategoryType($catID){
    $cat = mysql_fetch_array(mysql_query("SELECT category_name FROM tbl_categories WHERE category_id = '$catID'"));

    return $cat[0];
}

function getAllOffer(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_package_header");
    $count = 0;
    while($row = mysql_fetch_array($query)){
        $active = ($count == 0)?"active":"";
        $data .= "<li data-target='#carouselExampleCaptions' data-slide-to='$count' class='$active'></li>";

        $count++;
    }

    return $data;
}

function getAllOfferEquip(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_equipment_designs");
    $count = 0;
    while($row = mysql_fetch_array($query)){
        $active = ($count == 0)?"active":"";
        $data .= "<li data-target='#carouselExampleCaptions2' data-slide-to='$count' class='$active'></li>";

        $count++;
    }

    return $data;
}

function getAllOfferData(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_package_header");
    $count = 1;
    while($row = mysql_fetch_array($query)){
        $pckg_img = $row['package_img'];
        $active = ($count == 1)?"active":"";
        $theme = $row['package_name'];
        $desc = $row['package_desc'];
        $data .= '<div class="carousel-item '.$active.'" style="">
                        <img src="assets/images/'.$pckg_img.'" class="d-block w-100" alt="..." style="object-fit: contain;">
                       
                        <div class="carousel-caption d-md-block" style="width: 590px;background-color: #52525266;height: 200px;">
                        <h5 style="color: #fff;font-size: 20px;text-align: justify;padding-left: 10px;">Theme Name: '.$theme.'</h5>
                        <p style="color: #fff;font-size: 18px;text-align: justify;padding-left: 10px;">Theme Description: '.$desc.'</p>
                        <p style="color: #fff;font-size: 18px;text-align: justify;padding-left: 10px;">Venue: '.$desc.'</p>
                        <p style="color: #fff;font-size: 18px;text-align: justify;padding-left: 10px;">Price: '.$desc.'</p>
                        <p style="color: #fff;font-size: 15px;"><a href="#" onclick="viewMoreItems()" id="viewMore" style="color: black;"><span class="fa fa-eye"></span> More Details... </a></p>
                        </div>
                       
                    </div>';

        $count++;
    }

    return $data;
}

function getAllOfferDataEquip(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_equipment_designs");
    $count = 1;
    while($row = mysql_fetch_array($query)){
        $pckg_img = $row['design_img'];
        $active = ($count == 1)?"active":"";
        $theme = $row['design_name'];
        $desc = $row['design_desc'];
        $data .= '<div class="carousel-item '.$active.'" style="">
                        <img src="assets/images/'.$pckg_img.'" class="d-block w-100" alt="..." style="object-fit: contain;">
                       
                        <div class="carousel-caption d-md-block" style="width: 590px;background-color: #52525266;height: 200px;">
                        <h5 style="color: #fff;font-size: 20px;text-align: justify;padding-left: 10px;">Theme Name: '.$theme.'</h5>
                        <p style="color: #fff;font-size: 18px;text-align: justify;padding-left: 10px;">Theme Description: '.$desc.'</p>
                        </div>
                       
                    </div>';

        $count++;
    }

    return $data;
}
function getCakeSizes($cakeID){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_cake_sizes WHERE cake_id = '$cakeID'");
    $count = mysql_num_rows($query);
    if($count > 0){
        while($row = mysql_fetch_array($query)){
            $data .= $row['size'].",";
        }
    }else{
        $data = "No Sizes Available";
    }

    return $data;
}
function getAllThemes(){
    $data = "<option value=''>&mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_themes");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['theme_id']."'>".$row['theme_name']."</option>";
    }

    return $data;
}
function getTypes(){
    $data = "<option value=''>&mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_party_types");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['party_type_id']."'>".$row['type_name']."</option>";
    }

    return $data;
}
function getAllPackages(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.theme_id = t.theme_id AND h.package_venue = v.venue_id");
    while($row = mysql_fetch_array($query)){
        $img = $row['package_img'];
        $data .= "<li class='liList'>
                    <div class='thumbnail' id='thmbnl".$row['package_header_id']."'>
                        <label for='chk1".$row['package_header_id']."'><img id='img".$row['package_header_id']."' src='assets/images/".$img."' alt='".$img."' style=' height: 200px;width: 375px;object-fit: cover;'></label>
                        <input type='checkbox' hidden name='chk1' id='chk1".$row['package_header_id']."' value='".$row['package_header_id']."' class='chk1' autocomplete='off' onchange='getSelectedPckg(".$row['package_header_id'].")'>
                        <div class='caption'>
                        <h3 style='text-align: center;'>".$row['theme_name']."</h3>
                        <h5>Description: ".$row['package_desc']."</h5>
                        <h5>Price:  &#8369; ".number_format($row['package_price'], 2)."</h5>
                        <h5>Venue:  ".$row['venue_name']."</h5>
                        <p align='center'><a class='btn btn-success btn-block' id='del' onclick='choosePackage(".$row['package_header_id'].")'><span class='fa fa-eye'></span> View Inclusions </a></p>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getAllEquipmentDesigns($eid){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_equipment_designs as d, tbl_pricing as p  WHERE d.design_id = p.item_id AND equipment_id = '$eid' AND item_cat = 'E'");
    while($row = mysql_fetch_array($query)){
        $img = $row['design_img'];
        $data .= "<li class='liList col-sm-3'>
                    <div class='thumbnail' id='thmbnl".$row['design_id']."'>
                        <img src='assets/images/".$img."' alt='".$img."' style=' height: 200px;width: 375px;object-fit: cover;'>
                        <div class='caption'>
                        <h3 style='text-align: center;'>".$row['design_name']."</h3>
                        <div class='input-group'>
                            <span class='input-group-addon'>Price</span>
                            <input type='number' id='desPrice".$row['design_id']."' readonly class='form-control' value='".$row['price_amount']."'>
                        </div>
                        <div class='input-group'>
                            <span class='input-group-addon'>Quantity</span>
                            <input type='number' id='des".$row['design_id']."' onkeyup='getTotalAmnt(".$row['design_id'].")' class='form-control'>
                        </div>
                        <div class='input-group'>
                            <span class='input-group-addon'>Amount</span>
                            <input type='number' id='DAmnt".$row['design_id']."' readonly class='form-control'>
                        </div>
                            <input type='hidden' id='equipSaveVal".$row['design_id']."' name='equipSaveVal' readonly class='form-control' value='0-0-0-0'>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getAllGiveaways(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_giveaways as g, tbl_pricing as p  WHERE g.giveaway_id = p.item_id AND item_cat = 'G'");
    while($row = mysql_fetch_array($query)){
        $img = $row['giveaway_img'];
        $data .= "<li class='liList col-sm-3'>
                    <div class='thumbnail' id='thmbnl".$row['giveaway_id']."'>
                        <img src='assets/images/".$img."' alt='".$img."' style=' height: 200px;width: 375px;object-fit: cover;'>
                        <div class='caption'>
                        <h3 style='text-align: center;'>".$row['giveaway_name']."</h3>
                            <div class='input-group'>
                                <span class='input-group-addon'>Price</span>
                                <input type='number' id='GPrice".$row['giveaway_id']."' readonly class='form-control' value='".$row['price_amount']."'>
                            </div>
                            <div class='input-group'>
                                <span class='input-group-addon'>Quantity</span>
                                <input type='number' id='GA".$row['giveaway_id']."' onkeyup='GAgetTotalAmnt(".$row['giveaway_id'].")' class='form-control'>
                            </div>
                            <div class='input-group'>
                                <span class='input-group-addon'>Amount</span>
                                <input type='number' id='gaAmnt".$row['giveaway_id']."' readonly class='form-control'>
                            </div>
                            <div class='input-group'>
                                <input type='hidden' id='GASaveVal".$row['giveaway_id']."' name='GASaveVal' readonly class='form-control' value='0-0-0-0'>
                            </div>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getAllIC(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_invitational_cards as g, tbl_pricing as p  WHERE g.ic_id = p.item_id AND item_cat = 'I'");
    while($row = mysql_fetch_array($query)){
        $img = $row['ic_image'];
        $data .= "<li class='liList col-sm-3'>
                    <div class='thumbnail' id='thmbnl".$row['ic_id']."'>
                        <img src='assets/images/".$img."' alt='".$img."' style=' height: 200px;width: 375px;object-fit: cover;'>
                        <div class='caption'>
                        <h3 style='text-align: center;'>".$row['ic_name']."</h3>
                            <div class='input-group'>
                                <span class='input-group-addon'>Price</span>
                                <input type='number' id='ICPrice".$row['ic_id']."' readonly class='form-control' value='".$row['price_amount']."'>
                            </div>
                            <div class='input-group'>
                                <span class='input-group-addon'>Quantity</span>
                                <input type='number' id='IC".$row['ic_id']."' onkeyup='ICgetTotalAmnt(".$row['ic_id'].")' class='form-control'>
                            </div>
                            <div class='input-group'>
                                <span class='input-group-addon'>Amount</span>
                                <input type='number' id='ICAmnt".$row['ic_id']."' readonly class='form-control'>
                            </div>
                            <div class='input-group'>
                                <input type='hidden' id='ICSaveVal".$row['ic_id']."' name='ICSaveVal' readonly class='form-control' value='0-0-0-0'>
                            </div>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getCakeSizes_2($id){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_cake_sizes WHERE cake_id = '$id'");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['size_id']."'>".$row['size']."</option>";
    }

    return $data;
}
function getAllCakes(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_cake");
    while($row = mysql_fetch_array($query)){
        $img = $row['cake_img'];
        $data .= "<li class='liList col-sm-3'>
                    <div class='thumbnail' id='thmbnl".$row['cake_id']."'>
                        <img src='assets/images/".$img."' alt='".$img."' style=' height: 200px;width: 375px;object-fit: cover;'>
                        <div class='caption'>
                        <h3 style='text-align: center;'>".$row['cake_name']."</h3>
                            <div class='input-group'>
                                <span class='input-group-addon'>Sizes</span>
                                <select id='cakeSize".$row['cake_id']."' onchange='getSizePrice(".$row['cake_id'].")' class='form-control'>
                                    ".getCakeSizes_2($row['cake_id'])."
                                </select>
                            </div>
                            <div class='input-group'>
                                <span class='input-group-addon'>Price</span>
                                <input type='number' id='CakePrice".$row['cake_id']."' readonly class='form-control'>
                            </div>
                            <div class='input-group'>
                                <span class='input-group-addon'>Quantity</span>
                                <input type='number' id='Cake".$row['cake_id']."' onkeyup='CakegetTotalAmnt(".$row['cake_id'].")' class='form-control'>
                            </div>
                            <div class='input-group'>
                                <span class='input-group-addon'>Amount</span>
                                <input type='number' id='CakeAmnt".$row['cake_id']."' readonly class='form-control'>
                            </div>
                            <div class='input-group'>
                                <input type='hidden' id='CakeSaveVal".$row['cake_id']."' name='CakeSaveVal' readonly class='form-control' value='0-0-0-0'>
                            </div>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getAllThemesforCostumize(){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_themes");
    while($row = mysql_fetch_array($query)){
        $img = $row['theme_img'];
        $data .= "<li class='liList'>
                    <div class='thumbnail' id='themethmbnl".$row['theme_id']."'>
                        <label for='themechk1".$row['theme_id']."'><img id='themeimg".$row['theme_id']."' src='assets/images/".$img."' alt='".$img."' style=' height: 200px;width: 375px;object-fit: cover;'></label>
                        <input type='checkbox' hidden name='themechk1' id='themechk1".$row['theme_id']."' value='".$row['theme_id']."' class='themechk1' autocomplete='off' onchange='getSelectedthme(".$row['theme_id'].")'>
                        <div class='caption'>
                        <h3 style='text-align: center;'>".$row['theme_name']."</h3>
                        </div>
                    </div>
                </li>";
    }

    return $data;
}
function getVenueforCostumize(){
    $data = "<option value='0'> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_venue as v , tbl_pricing as p WHERE v.venue_id = p.item_id AND item_cat = 'V'");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['venue_id']."'> ".$row['venue_name']." ( &#8369 ".$row['price_amount'].") </option>";
    }

    return $data;
}
function getEntertainments(){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_entertainment");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['ent_id']."'> ".$row['ent_type']." ( &#8369 ".$row['ent_fee']."/".$row['ent_hrs']."Hrs) </option>";
    }

    return $data;
}
function getAllEquipments_package(){
    $equipments = mysql_query("SELECT * FROM tbl_equipments");
    while($e_row = mysql_fetch_array($equipments)){
        $equip_name = $e_row['equip_name'];
        $equipment_id = $e_row['equipment_id'];
        echo "<div class='col-md-12' style='max-width: 900px;overflow-y: auto;margin-top:20px;'>";
                echo "<h5 style='font-size: 20px;text-align: center;background: #cccccc;padding: 5px;'>$equip_name</h5>";
                echo "<ul class='thumbnails' style='display: flex;list-style: none;'>";
            $designs = mysql_query("SELECT * FROM tbl_equipment_designs WHERE equipment_id = '$equipment_id'");
                while($d_row = mysql_fetch_array($designs)){
                    $design_id = $d_row['design_id'];
                    $design_img = $d_row['design_img'];
                    $design_name = strtoupper($d_row['design_name']);
                    $type = "I";
                    echo "<li style='max-width: 200px;'>";
                        echo "<div class='thumbnail'>";
                            echo "<img src='../assets/images/$design_img' style='height: 120px;object-fit:fill;width: 190px;' alt='ALT NAME'>";
                            echo "<div class=''>";
                              echo "<h3 style='background: #cccccc;text-align: center;padding: 5px;font-size:15px'>$design_name</h3>";
                              echo "<p align='center'><a href='#' onclick='addItemToPackage(\"".$design_id."\",\"".$type."\")' class='btn btn-primary btn-block'><span class='fa fa-plus-circle'></span> Add to Package</a></p>";
                            echo "</div>";
                          echo "</div>";
                    echo "<li>";
                }

                echo "</ul>"; 
        echo "</div>";
    }
}
function getAddtional_package(){
    $array = array("Giveaways" => "ga","Invitational Cards" => "ic", "Cake" => "C", "Freebies" => "F");
    foreach ($array as $key => $value) {
        echo "<div class='col-md-12' style='max-width: 900px;overflow-y: auto;margin-top:20px;'>";
                echo "<h5 style='font-size: 20px;text-align: center;background: #cccccc;padding: 5px;'>$key</h5>";
                echo "<ul class='thumbnails' style='display: flex;list-style: none;'>";
                $sql = ($value == 'ga')?"SELECT * FROM tbl_giveaways":(($value == 'ic')?"SELECT * FROM tbl_invitational_cards":(($value == 'C')?"SELECT * FROM tbl_cake":"SELECT * FROM tbl_freebies"));
                $query = mysql_query($sql);
                while($row = mysql_fetch_array($query)){
                    $design_img = ($value == 'ga')?$row['giveaway_img']:(($value == 'ic')?$row['ic_image']:(($value == 'C')?$row['cake_img']:$row['fb_img']));

                    $design_name = ($value == 'ga')?$row['giveaway_name']:(($value == 'ic')?$row['ic_name']:(($value == 'C')?$row['cake_name']:$row['fb_name']));

                    $design_id = ($value == 'ga')?$row['giveaway_id']:(($value == 'ic')?$row['ic_id']:(($value == 'C')?$row['cake_id']:$row['fb_id']));

                    $type = ($value == 'ga')?"G":(($value == 'ic')?"IC":(($value == 'C')?"C":"F"));

                    echo "<li style='max-width: 200px;'>";
                        echo "<div class='thumbnail'>";
                            echo "<img src='../assets/images/$design_img' style='height: 120px;object-fit:fill;width: 190px;' alt='ALT NAME'>";
                            echo "<div class=''>";
                              echo "<h3 style='background: #cccccc;text-align: center;padding: 5px;font-size:15px'>$design_name</h3>";
                              echo "<p align='center'><a href='#' onclick='addItemToPackage(\"".$design_id."\",\"".$type."\")' class='btn btn-primary btn-block'><span class='fa fa-plus-circle'></span> Add to Package</a></p>";
                            echo "</div>";
                          echo "</div>";
                    echo "<li>";

                }
                echo "</ul>"; 
        echo "</div>";
    }
}
function getEntertainments_package(){
    $data = "<div class='col-md-6'>
                <div class='form-group'>
                <div class='input-group'>
                 <div class='input-group-addon'>Entertainments: </div>
                <select class='form-control' onchange='ent_pkg()' id='entertainmentid'>";
    $data .= "<option value=''>&mdash; Please Choose &mdash; </option>";
        $query = mysql_query("SELECT * FROM tbl_entertainment");
        while($row = mysql_fetch_array($query)){
            $data .= "<option value='".$row['ent_id']."-E'>".$row['ent_type']."</option>";
        }

    $data .= "</select></div></div></div>";

    return $data;
}
function getSelectedTheme($id){
    $data = "<option value=''>&mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_themes");
    while($row = mysql_fetch_array($query)){
        $selected = ($id == $row['theme_id'])?"selected":'';
        $data .= "<option $selected value='".$row['theme_id']."'>".$row['theme_name']."</option>";
    }

    return $data;
}
function getSelectedType($id){
    $data = "<option value=''>&mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_party_types");
    while($row = mysql_fetch_array($query)){
        $selected = ($id == $row['party_type_id'])?"selected":'';
        $data .= "<option $selected value='".$row['party_type_id']."'>".$row['type_name']."</option>";
    }

    return $data;
}
function getItemName($id, $type){
    $sql = ($type == 'I')?"SELECT design_name FROM tbl_equipment_designs WHERE design_id = '$id'":(($type == 'IC')?"SELECT ic_name FROM tbl_invitational_cards WHERE ic_id = '$id'":(($type == 'C')?"SELECT cake_name FROM tbl_cake as c, tbl_cake_sizes as s WHERE c.cake_id = s.cake_id AND size_id = '$id'":(($type == 'G')?"SELECT giveaway_name FROM tbl_giveaways WHERE giveaway_id = '$id'":(($type == 'F')?"SELECT fb_name FROM tbl_freebies WHERE fb_id = '$id'":"SELECT ent_type FROM tbl_entertainment WHERE ent_id = '$id'"))));

    $query = mysql_fetch_array(mysql_query($sql));

    return $query[0];
}
function countCartContent_package($user_id){
    $query = mysql_query("SELECT * FROM tbl_booking_cart WHERE user_id = '$user_id' AND is_package = '1' AND status = 0");
    $count = mysql_num_rows($query);

    return $count;
}
function getTotalPrice_package($user_id){
    $query = mysql_query("SELECT * FROM tbl_booking_cart WHERE user_id = '$user_id' AND is_package = '1' AND status = 0");
    $total = 0;
    while($row = mysql_fetch_array($query)){
        if($row['item_type'] == ''){
            $price = $row['item_price'];
        }else{
            $price = $row['item_price'] * $row['item_qntty'];
        }

        $total += $price;
    }

    return $total;
}
function getTotalPrice_costumize($user_id){
    $query = mysql_query("SELECT * FROM tbl_booking_cart WHERE user_id = '$user_id' AND is_package = 0 AND status = 0");
    $total = 0;
    while($row = mysql_fetch_array($query)){
        if($row['item_type'] == ''){
            $price = $row['item_price'];
        }else{
            $price = $row['item_price'] * $row['item_qntty'];
        }

        $total += $price;
    }

    return $total;
}
function countCartContent_customize($user_id){
    $query = mysql_query("SELECT * FROM tbl_booking_cart WHERE user_id = '$user_id' AND is_package = 0 AND status = 0");
    $count = mysql_num_rows($query);

    return $count;
}
function getTotalPrice_additional($trans_id){
    $query = mysql_query("SELECT * FROM tbl_booking_cart WHERE transaction_id = '$trans_id' AND item_type != '' AND status = 1");
    $total = 0;
    while($row = mysql_fetch_array($query)){
        if($row['item_type'] == ''){
            $price = $row['item_price'];
        }else{
            $price = $row['item_price'] * $row['item_qntty'];
        }

        $total += $price;
    }

    return $total;
}
function getTotalPrice_costumize_view($trans_id){
    $query = mysql_query("SELECT * FROM tbl_booking_cart WHERE transaction_id = '$trans_id' AND item_type != '' AND status = 1");
    $total = 0;
    while($row = mysql_fetch_array($query)){
        if($row['item_type'] == ''){
            $price = $row['item_price'];
        }else{
            $price = $row['item_price'] * $row['item_qntty'];
        }

        $total += $price;
    }

    return $total;
}
function getVenuePrice($venue_id){
    $query = mysql_query("SELECT * FROM tbl_pricing WHERE item_id = '$venue_id' AND item_cat = 'V'");
   
    $row = mysql_fetch_array($query);
        

    return $row['price_amount'];
}
function countBoookingNotif(){
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_notifications WHERE notif_status = 0 AND notif_type = 'B'"));

    return $count[0];
}
function countMsgNotif(){
    $count = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_notifications WHERE notif_status = 0 AND notif_type = 'M'"));

    return $count[0];
}
?>