<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
    <aside class="sidebar-left">
        <nav class="navbar navbar-inverse">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="index.html"><span class="fa fa-area-chart"></span> BPEPS<span class="dashboard_text">Blu Party</span></a></h1>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
                <li class="header" style='text-align: center;font-size: 13px;'>MAIN NAVIGATION</li>
                <li class="treeview">
                    <a href="index.php?view=dashboard">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-th-list"></i> <span>Master Data</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <a href="index.php?view=party-types">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Party Types</li>
                        </a>
                        <a href="index.php?view=themes">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Themes</li>
                        </a>
                        <a href="index.php?view=item-category">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Equipment Category</li>
                        </a>
                        <a href="index.php?view=equipments">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Equipment Items</li>
                        </a>
                        <a href="index.php?view=invitational-cards">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Invitational Card</li>
                        </a>
                        <a href="index.php?view=giveaways">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Giveaways</li>
                        </a>
                        <a href="index.php?view=cake-list">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Cake</li>
                        </a>
                        <a href="index.php?view=entertainments">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Entertainment</li>
                        </a>
                        <a href="index.php?view=freebies">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Freebies</li>
                        </a>
                        <a href="index.php?view=venue">
                            <li style="padding: 10px 5px 5px 10px;"><i class="fa fa-circle-o"></i> Venue</li>
                        </a>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="index.php?view=pricing">
                        <i class="fa fa-money"></i> <span>Pricing</span>
                    </a>
                </li>
               
                
                <li class="treeview">
                    <a href="index.php?view=package-list">
                        <i class="fa fa-th-list"></i> <span>Package</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="index.php?view=transactions">
                        <i class="fa fa-exchange"></i> <span>Transactions</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="index.php?view=reports">
                        <i class="fa fa-print"></i> <span>Reports</span>
                    </a>
                </li>
            </ul>
            </div>
        </nav>
    </aside>
</div>