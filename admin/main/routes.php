<?php 
switch($view){
    case 'dashboard':
        require 'views/dashboard.php';
    break;
    case 'equipments':
        require 'views/equipments.php';
    break;
    case 'pricing':
        require 'views/pricing.php';
    break;
    case 'package-list':
        require 'views/package_list.php';
    break;
    case 'item-category':
        require 'views/item_category.php';
    break;
    case 'item':
        require 'views/item_designs.php';
    break;
    case 'venue':
        require 'views/venues.php';
    break;
    case 'add-package-list':
        require 'views/add_package_list.php';
    break;
    case 'invitational-cards':
        require 'views/inv_cards.php';
    break;
    case 'giveaways':
        require 'views/giveaways.php';
    break;
    case 'transactions':
        require 'views/transactions.php';
    break;
    case 'reports':
        require 'views/reports.php';
    break;
    case 'all-users':
        require 'views/users.php';
    break;
    case 'cake-list':
        require 'views/cake.php';
    break;
    case 'entertainments':
        require 'views/entertainments.php';
    break;
    case 'themes':
        require 'views/themes.php';
    break;
    case 'freebies':
        require 'views/freebies.php';
    break;
    case 'messages':
        require 'views/messages.php';
    break;
    case 'view-details':
        require 'views/view_details.php';
    break;
    case 'party-types':
        require 'views/party_types.php';
    break;
    default:
    break;

}