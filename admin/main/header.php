<style>
.profile_details ul li ul.dropdown-menu.drp-mnu {
    padding: 1em;
    min-width: 160px;
    top: 130%;
    left: -28%;
}
</style>
<div class="sticky-header header-section ">
    <div class="header-left">
        <button id="showLeftPush"><i class="fa fa-bars"></i></button>
        <div class="profile_details_left">
            <ul class="nofitications-dropdown">
                <li class="dropdown head-dpdn">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge"><?=countMsgNotif();?></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="notification_header">
                                <h3>You have <?=countMsgNotif();?> new messages</h3>
                            </div>
                        </li>
                        <ul style="list-style: none;">
                            <?php 
                            $book = mysql_query("SELECT * FROM tbl_notifications WHERE notif_status = 0 AND notif_type = 'M'");
                            while($b_row = mysql_fetch_array($book)){
                                $userid = $b_row['user_id'];
                                $user = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname,' ',lastname) FROM tbl_users WHERE user_id = '$userid'"));
                            ?>
                            <li><a href="#" onclick='gotoMsg(<?=$b_row['notif_id']?>,<?=$b_row['user_id']?>)'><?=$user[0]?> has new message.</a></li>
                            <?php } ?>  
                        </ul>
                        <li>
                            <hr>
                            <div class="notification_footer" style='background-color: #FAFAFA;text-align: center;'>
                                <a href="#" onclick="window.location='index.php?view=messages&id=0'"> See All Messages</a>
                            </div>
                            
                        </li>
                    </ul>
                </li>
                <li class="dropdown head-dpdn">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue"><?=countBoookingNotif()?></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="notification_header">
                                <h3>You have <?=countBoookingNotif()?> new notification</h3>
                            </div>
                        </li>
                        <ul style="list-style: none;">
                            <?php 
                            $book = mysql_query("SELECT * FROM tbl_notifications WHERE notif_status = 0 AND notif_type = 'B'");
                            while($b_row = mysql_fetch_array($book)){
                                $userid = $b_row['user_id'];
                                $user = mysql_fetch_array(mysql_query("SELECT CONCAT(firstname,' ',lastname) FROM tbl_users WHERE user_id = '$userid'"));
                            ?>
                            <li><a href="#" onclick='gotoTrans(<?=$b_row['notif_id']?>,<?=$b_row['trans_id']?>)'><?=$user[0]?> finished his/her booking</a></li>
                            <?php } ?>  
                        </ul>
                    </ul>
                </li>	
            </ul>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <div class="header-right">
        <div class="profile_details">		
            <ul>
                <li class="dropdown profile_details_drop">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <div class="profile_img">	
                            <span class="prfil-img"><img src="images/2.jpg" alt=""> </span> 
                            <div class="user-name">
                                <p><?php echo strtoupper(getUser($user_id)); ?></p>
                                <span><?php echo $admin ?></span>
                            </div>
                            <i class="fa fa-angle-down lnr"></i>
                            <i class="fa fa-angle-up lnr"></i>
                            <div class="clearfix"></div>	
                        </div>	
                    </a>
                    <ul class="dropdown-menu drp-mnu">
                        <!-- <li> <a href="#" onclick="window.location='index.php?view=all-users'"><i class="fa fa-users"></i> Users</a> </li> -->
                        <li> <a href="#" onclick='logout()'><i class="fa fa-sign-out"></i> Logout</a> </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"> </div>				
    </div>
    <div class="clearfix"> </div>	
</div>