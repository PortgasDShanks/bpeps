<?php 
include('../main/configuration.php');

$headerID = $_POST['headerID'];
$arrayData = $_POST['arrayData'];
$arrayDataGA = $_POST['arrayDataGA'];
$arrayDataIC = $_POST['arrayDataIC'];
$arrayDataCake = $_POST['arrayDataCake'];
$arrayDataFB = $_POST['arrayDataFB'];
$arrayDataEnt = $_POST['arrayDataEnt'];
// for ITEMS
if(count($arrayData) > 0){
    foreach($arrayData as $data_){
        $explode = explode("-", $data_);
        $id = $explode[0];
        $qntty = $explode[1];

        $data_d = array("package_header_id" => $headerID, "package_item" => $id, "item_qntty" => $qntty, "cat" => "I");
        $query = insert_query("tbl_package_details",$data_d,"N");
    }
}
// FOR GIVEAWAYS
if(count($arrayDataGA)){
    foreach($arrayDataGA as $gaID){

        $data_d = array("package_header_id" => $headerID, "package_item" => $gaID, "item_qntty" => 30, "cat" => "G");
        $query = insert_query("tbl_package_details",$data_d,"N");

    }
}
// FOR INVITATIONAL
if(count($arrayDataIC)){
    foreach($arrayDataIC as $icID){

        $data_d = array("package_header_id" => $headerID, "package_item" => $icID, "item_qntty" => 30, "cat" => "IC");
        $query = insert_query("tbl_package_details",$data_d,"N");

    }
}
// FOR CAKES
if(count($arrayDataCake)){
    foreach($arrayDataCake as $cakeID){

        $data_d = array("package_header_id" => $headerID, "package_item" => $cakeID, "item_qntty" => 1, "cat" => "C");
        $query = insert_query("tbl_package_details",$data_d,"N");

    }
}
// FOR FREEBIES
if(count($arrayDataFB)){
    foreach($arrayDataFB as $fbID){

        $data_d = array("package_header_id" => $headerID, "package_item" => $fbID, "item_qntty" => 1, "cat" => "F");
        $query = insert_query("tbl_package_details",$data_d,"N");

    }
}
// FOR ENT
if(count($arrayDataGA)){
    foreach($arrayDataGA as $entID){

        $data_d = array("package_header_id" => $headerID, "package_item" => $entID, "item_qntty" => 1, "cat" => "E");
        $query = insert_query("tbl_package_details",$data_d,"N");

    }
}
echo $query;