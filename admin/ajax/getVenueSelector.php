<?php
include('../main/configuration.php');
$type = $_POST['venueSelect'];
if($type == 'p'){
?>
<div class="col-md-6" style="padding-top: 10px">
	<div class="input-group">
		<div class="input-group-prepend">
		    <span class="input-group-text"> Venue: </span>
		</div>
		  <select class="form-control" id="venueID" onchange="getVenuePrice()">
		  	<option value="">&mdash; Please Choose &mdash; </option>
		  	<?php 
		  	$v = mysql_query("SELECT * FROM tbl_venue");
		  	while($row_v = mysql_fetch_array($v)){
		  	?>
		  	<option value="<?=$row_v['venue_id']?>"><?=$row_v['venue_name']?></option>
		  	<?php } ?>
		  </select>
	</div>
</div>
<?php }else{ ?>
<div class="col-md-12" style="padding-top: 10px">
	<h5> Pin your venue on map (Click on the google map and the marker will move)</h5>
	<div id="map"></div>
	
</div>
<?php } ?>
<script type="text/javascript">
var position = [10.6840, 122.9563];

function initialize() { 
    var latlng = new google.maps.LatLng(position[0], position[1]);
    var myOptions = {
        zoom: 16,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), myOptions);

    marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: "Latitude:"+position[0]+" | Longitude:"+position[1]
    });

    google.maps.event.addListener(map, 'click', function(event) {
        var result = [event.latLng.lat(), event.latLng.lng()];
        transition(result);
    });
}



var numDeltas = 100;
var delay = 10; //milliseconds
var i = 0;
var deltaLat;
var deltaLng;

function transition(result){
    i = 0;
    deltaLat = (result[0] - position[0])/numDeltas;
    deltaLng = (result[1] - position[1])/numDeltas;
    moveMarker();
    $("#lat").val(position[0]);
    $("#long").val(position[1]);
}

function moveMarker(){
    position[0] += deltaLat;
    position[1] += deltaLng;
    var latlng = new google.maps.LatLng(position[0], position[1]);
    marker.setTitle("Latitude:"+position[0]+" | Longitude:"+position[1]);
    marker.setPosition(latlng);
    if(i!=numDeltas){
        i++;
        setTimeout(moveMarker, delay);
    }
}
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback=initialize"> </script>