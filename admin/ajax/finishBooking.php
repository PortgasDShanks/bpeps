<?php
include('../main/configuration.php');

$user_id = $_SESSION['cust_user_id'];
$refNum = $_POST['refNum'];
$eventDate = $_POST['eventDate'];
$eventtimeFrom = $_POST['eventtimeFrom'];
$eventtimeTo = $_POST['eventtimeTo'];
$payment_mode = $_POST['payment_mode'];
$totalPricePackage = $_POST['totalPricePackage'];
$venue = $_POST['venue'];
$lat = $_POST['lat'];
$long = $_POST['long'];
$curdate = getCurrentDate();

$array = array("ref_number" => $refNum, "user_id" => $user_id, "trans_date" => $eventDate, "trans_time_from" => $eventtimeFrom, "trans_time_to" => $eventtimeTo,"venue" => $venue,"venue_lat" => $lat, "venue_long" => $long, "date_added" => getCurrentDate(), "mode_of_payment" => $payment_mode, "is_package" => 1, "status" => "P", "total_amount" => $totalPricePackage);

$query = insert_query("tbl_transactions",$array,"Y");
if($query > 0){
	$array2 = array("status" => 1, "transaction_id" => $query);

	$query2 = update_query("tbl_booking_cart",$array2," WHERE user_id = '$user_id' AND status = 0");

	$notif_data = array("user_id" => $user_id, "notif_datetime" => $curdate, "notif_status" => 0, "trans_id" => $query, "notif_type" => "B");

	$query3 = insert_query("tbl_notifications", $notif_data, "N");

	echo $query2;
}

