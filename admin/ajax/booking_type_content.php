<link href="assets/admin_assets/css/fSelect.css" rel="stylesheet">
<script src="../../assets/admin_assets/js/fSelect.js"></script>
<?php 
include '../main/configuration.php';
$type = $_POST['bType'];
if($type == "P"){

?>  
    <div class='col-sm-12'>
        <div class='col-sm-12' style='margin-top: 20px;padding: 20px;border: 1px solid #cccccc;border-radius: 10px;box-shadow: 1px 4px 4px 0px #cccccc'>
            <div class='col-sm-4'>
                <div class='input-group'>
                    <span class='input-group-addon'> Date: </span>
                    <input type="date" id='eventDate' class='form-control'>
                </div>
            </div>
            <div class='col-sm-6'>
                <div class='input-group'>
                    <span class='input-group-addon'> Duration: </span>
                    <input type="time" id='eventTF' class='form-control'>
                    <span class='input-group-addon'> to </span>
                    <input type="time" id='eventTT' class='form-control'>
                </div>
            </div>
            <div class='col-sm-2'>
                <div class='input-group'>
                    <button class='btn btn-sm btn-primary' id='bookHEad' onclick="saveBookingHeader()"><span class='fa fa-check-circle'></span> Continue </button>
                </div>
            </div>
        </div>
    </div>
    
    <input type="hidden" name="" id="headerID">
    <div id='additional_wrapper' style='display: none'>
        <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;padding: 15px;border: 1px solid #cccccc;border-radius: 10px;box-shadow: 1px 4px 4px 0px #cccccc'>
            <label>Package List: </label>
            <ul class='ulList' style='list-style: none;display: flex;'>
            <?php echo getAllPackages(); ?>
            </ul>
        </div>
        <div class='col-sm-12'>
            <div class='col-sm-12' style='margin-top: 20px;'>
                <label>Would you like to add another equipment/s? &nbsp; &nbsp;
                    <input type="radio" onchange='getAnswer()' name="askCust" id="askCust" value="Y"> Yes
                    <input type="radio" onchange='getAnswer()' name="askCust" id="askCust" value="N"> No
                </label>
            </div>
            <div class='col-sm-12' id='additionalDiv' style='display: none;padding: 15px;border: 1px solid #cccccc;border-radius: 10px;box-shadow: 1px 4px 4px 0px #cccccc'>
                <div class='col-sm-12' style='margin-top: 20px;'>
                    <label style='text-indent: 20px;'>EQUIPMENTS</label>
                </div>
                    <?php 
                        $query1 = mysql_query("SELECT * FROM tbl_equipments");
                        while($q_1 = mysql_fetch_array($query1)){
                        $eID = $q_1['equipment_id'];
                    ?>     
                        <div class='col-sm-12' style='margin-top: 10px;'>
                            <label style='text-indent: 30px;'><?php echo $q_1['equip_name']?></label> 
                        </div>

                        <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                            <ul class='ulList' style='list-style: none;display: flex;'>
                            <?php echo getAllEquipmentDesigns($eID); ?>
                            </ul>
                        </div>
                    <?php } ?>
                    
                
                <div class='col-sm-12' style='margin-top: 20px;'>
                    <label style='text-indent: 20px;'>INVITATIONAL CARDS</label>
                </div>
                <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                    <ul class='ulList' style='list-style: none;display: flex;'>
                    <?php echo getAllIC(); ?>
                    </ul>
                </div> 
                

                <div class='col-sm-12' style='margin-top: 20px;'>
                    <label style='text-indent: 20px;'>GIVEAWAYS</label>
                </div>
                <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                    <ul class='ulList' style='list-style: none;display: flex;'>
                    <?php echo getAllGiveaways(); ?>
                    </ul>
                </div> 


                <div class='col-sm-12' style='margin-top: 20px;'>
                    <label style='text-indent: 20px;'>CAKES</label>
                </div>
                <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                    <ul class='ulList' style='list-style: none;display: flex;'>
                    <?php echo getAllCakes(); ?>
                    </ul>
                </div>
                <!-- <div class='col-sm-12' style='margin-top: 20px;'>
                    <label style='text-indent: 20px;'>ENTERTAINMENTS</label>
                </div>
                <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                    <ul class='ulList' style='list-style: none;display: flex;'>
                    <?php //echo getAllGiveaways(); ?>
                    </ul>
                </div>  -->
            </div>
        </div>
        <div class='col-sm-12' style='margin-top: 10px'>
            <button class='btn btn-sm btn-primary pull-right' disabled id='finaLStep' onclick="finalStep()"><span class='fa fa-check-circle'></span> Continue </button>
        </div>
    </div>
    
    
<?php } else { ?>
    <div class='col-sm-12'>
        <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;padding: 15px;border: 1px solid #cccccc;border-radius: 10px;box-shadow: 1px 4px 4px 0px #cccccc'>
            <label>Theme List: (<span style='color: red'> Leave Blank if not necessary , This is optional.</span>) </label>
            <ul class='ulListThemes' style='list-style: none;display: flex;'>
                <?php echo getAllThemesforCostumize(); ?>
            </ul>
        </div>
    </div>
    <div class='col-sm-12'>
        <div class='col-sm-12' style='margin-top: 20px;padding: 20px;border: 1px solid #cccccc;border-radius: 10px;box-shadow: 1px 4px 4px 0px #cccccc'>
            <div class='col-sm-6'>
                <div class='input-group'>
                    <span class='input-group-addon'> Venue (Leave Blank if not necessary): </span>
                    <select name="venueID" id="venueID" class='form-control'>
                        <?php echo getVenueforCostumize(); ?>
                    </select>
                </div>
            </div>
            <div class='col-sm-6'>
                <div class='input-group'>
                    <span class='input-group-addon'> Date: </span>
                    <input type="date" id='eventDate' class='form-control'>
                </div>
            </div>
            <div class='col-sm-6' style='margin-top: 10px'>
                <div class='input-group'>
                    <span class='input-group-addon'> Duration: </span>
                    <input type="time" id='eventTF' class='form-control'>
                    <span class='input-group-addon'> to </span>
                    <input type="time" id='eventTT' class='form-control'>
                </div>
            </div>
            <div class='col-sm-6' style='margin-top: 10px'>
                <div class='input-group'>
                    <button class='btn btn-sm btn-primary' id='bookHEad' onclick="saveBookingHeaderCostumize()"><span class='fa fa-check-circle'></span> Continue </button>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="" id="headerIDCostumize">
    <div class='col-sm-12'>
        <div class='col-sm-12' id='costumizeDiv' style='display:none;padding: 15px;border: 1px solid #cccccc;border-radius: 10px;box-shadow: 1px 4px 4px 0px #cccccc'>
            <div class='col-sm-12' style='margin-top: 20px;'>
                <label style='text-indent: 20px;'>EQUIPMENTS</label>
            </div>
                <?php 
                    $query1 = mysql_query("SELECT * FROM tbl_equipments");
                    while($q_1 = mysql_fetch_array($query1)){
                    $eID = $q_1['equipment_id'];
                ?>     
                    <div class='col-sm-12' style='margin-top: 10px;'>
                        <label style='text-indent: 30px;'><?php echo $q_1['equip_name']?></label> 
                    </div>

                    <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                        <ul class='ulList' style='list-style: none;display: flex;'>
                        <?php echo getAllEquipmentDesigns($eID); ?>
                        </ul>
                    </div>
                <?php } ?>
                
            
            <div class='col-sm-12' style='margin-top: 20px;'>
                <label style='text-indent: 20px;'>INVITATIONAL CARDS</label>
            </div>
            <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                <ul class='ulList' style='list-style: none;display: flex;'>
                <?php echo getAllIC(); ?>
                </ul>
            </div> 
            

            <div class='col-sm-12' style='margin-top: 20px;'>
                <label style='text-indent: 20px;'>GIVEAWAYS</label>
            </div>
            <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                <ul class='ulList' style='list-style: none;display: flex;'>
                <?php echo getAllGiveaways(); ?>
                </ul>
            </div> 


            <div class='col-sm-12' style='margin-top: 20px;'>
                <label style='text-indent: 20px;'>CAKES</label>
            </div>
            <div class='col-sm-12' style='overflow-x: scroll;overflow-y: hidden;margin-top:30px;'>
                <ul class='ulList' style='list-style: none;display: flex;'>
                <?php echo getAllCakes(); ?>
                </ul>
            </div>
            <div class='col-sm-4' style='margin-top: 20px;'>
                <div class='input-group'>
                    <span class='input-group-addon'> ENTERTAINMENTS: </span>
                    <select name="entID" id="entID" class='form-control'>
                        <?php echo getEntertainments(); ?>
                    </select>
                </div>
            </div> 
            <div class='col-sm-4' style='margin-top: 20px;'>
                <div class='input-group'>
                    <span class='input-group-addon'> Arrival Time: </span>
                    <input type="time" onkeyup='getArrival()' id='arrivalFrom' class='form-control'>
                    <span class='input-group-addon'> to </span>
                    <input type="time" onkeyup='getArrival()' id='arrivato' class='form-control'>
                </div>
            </div>
            <div class='col-sm-4' style='margin-top: 20px;'>
                <div class='input-group'>
                    <span class='input-group-addon'> Additional Amount: </span>
                    <input type="text" readonly id='addAmnt' class='form-control'>
                    <input type="hidden" id='addAmntValSave' value='0,0,0,0,0' class='form-control'>
                </div>
            </div>
        </div>
    </div>
    <div class='col-sm-12' style='margin-top: 10px'>
        <button class='btn btn-sm btn-primary pull-right' id='finaLStepCost' onclick="finalStepCostumize()"><span class='fa fa-check-circle'></span> Continue </button>
    </div>
<?php } ?>
<script>
$(document).ready( function(data){
    $(".fSelect").fSelect({
        placeholder: " -- Please Choose -- "
    });
});
</script>