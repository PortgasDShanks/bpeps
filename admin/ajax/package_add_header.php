<?php 
include('../main/configuration.php');

$designName = clean($_POST['designName']);
$partyType = $_POST['partyType'];
$pckg_price = $_POST['pckg_price'];
$pckg_desc = clean($_POST['pckg_desc']);

if(is_array($_FILES)) {
    if(is_uploaded_file($_FILES['avatar']['tmp_name'])) {
        $sourcePath = $_FILES['avatar']['tmp_name'];
        $targetPath = "../../assets/images/".$_FILES['avatar']['name'];
        if(move_uploaded_file($sourcePath,$targetPath)) {
            $img = $_FILES['avatar']['name'];
            $array_data = array(
                                "theme_id" => $designName,
                                "type_id" => $partyType,
                                "package_desc" => $pckg_desc,
                                "package_price" => $pckg_price,
                                "package_img" => $img

            );
            $result = insert_query("tbl_package_header",$array_data,"Y");

           
        }
    }
}
echo $result;