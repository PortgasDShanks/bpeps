<?php 
include '../../main/configuration.php';

$query = mysql_query("SELECT * FROM tbl_transactions WHERE is_package = 0 ORDER BY date_added DESC");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){
    $list = array();
    $list['trans_id'] = $row['trans_id'];
    $list['count'] = $count++;
    $list['ref_num'] = $row['ref_number'];
    $list['eventDate'] = date("F d, Y", strtotime($row['trans_date']));
    $list['eventTime'] = date("h:i A", strtotime($row['trans_time_from']))." to ".date("h:i A", strtotime($row['trans_time_to']));
    $list['modeOfPayment'] = $row['mode_of_payment'];
    $list['dateAdded'] = date("F d, Y", strtotime($row['date_added']));
    $rate = ($row['status'] == 'F')?"dispay:block":"display:none";
    $list['action'] = "<div class='btn-group'>
                            <button data-toggle='tooltip' title='View Transaction Details' class='btn btn-success btn-sm' id='updatePriceGA".$row['trans_id']."' onclick='viewCostumDetails(".$row['trans_id'].")'><span class='fa fa-pencil'></span></button>
                        </div>
                        <div class='btn-group'>
                            <button style='$rate' data-toggle='tooltip' title='Rate Us' class='btn btn-success btn-sm' onclick='rateUs(".$row['trans_id'].")'><span class='fa fa-star'></span> </button>
                        </div>";
     $list['status'] = ($row['status'] == 'P')?"<span style='color: orange'> Pending </span>":(($row['status'] == 'F')?"<span style='color: green'> Finished </span>":(($row['status'] == 'A')?"<span style='color: blue'> Approved </span>":"<span style='color: orange'> Cancelled </span>"));

    array_push($response['data'],$list);
}
	echo json_encode($response);