<?php 
include '../../main/configuration.php';

$query = mysql_query("SELECT * FROM tbl_transactions as t, tbl_users as u WHERE t.user_id = u.user_id ORDER BY trans_date DESC");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){
    $list = array();
    $list['trans_id'] = $row['trans_id'];
    $list['count'] = $count++;
    $list['booking_type'] = ($row['is_package'] == '0')?"COSTUMIZED":"PACKAGED";
    $list['customer'] = $row['firstname'].' '.$row['lastname'];
    $list['booking_date'] = date("F d, Y", strtotime($row['trans_date']));
    $list['booking_time'] = date("h:i A", strtotime($row['trans_time_from'])).' &mdash; '.date("h:i A", strtotime($row['trans_time_to']));
    $list['mode_of_payment'] = $row['mode_of_payment'];
    $list['total_amount'] = number_format($row['total_amount'], 2);
    $list['status'] = ($row['status'] == 'P')?"<span style='color: orange'>Pending</span>":(($row['status'] == 'A')?"<span style='color: blue'>APPROVED</span>":(($row['status'] == 'C')?"<span style='color: red'>Cancelled</span>":"<span style='color: green'>Finished</span>"));
     $list['venue_stat'] = ($row['venue_status'] == 0)?'<span style="color: orange">Pending</span>':(($row['venue_status'] == 1)?'<span style="color: green">Approved</span>':'<span style="color: red">N/A</span>');
    if($row['status'] == 'P'){
        $list['action'] = "<div class='btn-group'>
                            <button data-toggle='tooltip' title='Approve Booking' data-placement='top' class='btn btn-success btn-sm' id='approveBooking".$row['trans_id']."' onclick='approveBooking(".$row['trans_id'].")'><span class='fa fa-check-circle'></span></button>
                            <button data-toggle='tooltip' title='Cancel Booking' data-placement='top' class='btn btn-danger btn-sm' id='cancelBooking".$row['trans_id']."' onclick='cancelBooking(".$row['trans_id'].")'><span class='fa fa-close'></span> </button>
                        </div>";
    }else if($row['status'] == 'A'){
        $list['action'] = "<div class='btn-group'>
                            <button data-toggle='tooltip' title='Finish Booking' data-placement='top' class='btn btn-success btn-sm' id='finishBooking".$row['trans_id']."' onclick='finishBooking(".$row['trans_id'].")'><span class='fa fa-check-circle'></span> Finish</button>
                        </div>";
    }else{
        $list['action'] = "";
    }

    $list['view'] = "<div class='btn-group'>
                            <button data-toggle='tooltip' title='View Details' data-placement='top' class='btn btn-success btn-sm' id='finishBooking".$row['trans_id']."' onclick='viewDetails(".$row['trans_id'].",".$row['is_package'].")'><span class='fa fa-eye'></span></button>
                        </div>";

    array_push($response['data'],$list);
}
	echo json_encode($response);