<?php 
include '../../main/configuration.php';
$cust_id = $_SESSION['cust_user_id'];
$query = mysql_query("SELECT * FROM tbl_transactions WHERE user_id = '$cust_id'");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){
    $list = array();
    $list['trans_id'] = $row['trans_id'];
    $list['count'] = $count++;
    $list['ref_number'] = $row['ref_number'];
    $list['trans_date'] = date("F d, Y", strtotime($row['trans_date']));
    $list['trans_time_from'] = date("h:i A", strtotime($row['trans_time_from']));
    $list['trans_time_to'] = date("h:i A", strtotime($row['trans_time_to']));
    $list['type'] = ($row['is_package'] == 1)?"Package":"Customize";
    $list['date_added'] = date("F d, Y", strtotime($row['date_added']));
    $list['status'] = ($row['status'] == 'P')?"<span style='color: orange'>PENDING</span>":(($row['status'] == 'A')?"<span style='color: blue'>APPROVED</span>":(($row['status'] == 'C')?"<span style='color: red'>CANCELLED</span>":"<span style='color: green'>FINISHED</span>"));

    array_push($response['data'],$list);
}
	echo json_encode($response);