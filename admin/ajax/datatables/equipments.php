<?php 
include '../../main/configuration.php';

$query = mysql_query("SELECT * FROM tbl_equipments");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){
    $list = array();
    $list['equipID'] = $row['equipment_id'];
    $list['count'] = $count++;
    $list['equip_name'] = $row['equip_name'];
    $list['category'] = getCategoryName($row['cat_id']);
    $list['date_added'] = date("F d, Y h:i A", strtotime($row['date_added']));
    $list['added_by'] = getUser($row['added_by']);
    $list['action'] = "<center>
								<li class='dropdown' style='list-style: none; font-size: 18px; color: #FFF;'>
									<a href='#' class='dropdown-toggle' data-toggle='dropdown' style='color: #607D8B;'><strong><span class='fa fa-gear'></span></strong></a>
										<ul class='dropdown-menu'>
											<ul style='background: #444; border: 1px solid #333; padding: 5px 10px;'>
												
												<li style='list-style:none;color:#FFF;'><a href='#' onclick='viewDesigns(".$row['equipment_id'].");' style='color: #fff;'><span class='fa fa-eye' style='font-size: 14px;'></span> View Designs </a></li>

												<li style='list-style:none;color:#FFF;'><a href='#' onclick='editRecord(\"".$row['equipment_id']."\",\"".$row['equip_name']."\");' style='color: #fff;'><span class='fa fa-pencil' style='font-size: 14px;'></span> Edit Record </a></li>

											</ul>
										</ul>
								</li>
							</center>";

    array_push($response['data'],$list);
}
	echo json_encode($response);