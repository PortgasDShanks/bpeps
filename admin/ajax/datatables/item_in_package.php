<?php 
include '../../main/configuration.php';
$id = $_POST['headerID'];
$query = mysql_query("SELECT * FROM tbl_package_details WHERE package_header_id = '$id'");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){
    $list = array();
    $size_id = ($row['cat'] == 'C')?$row['package_item']:0;
    $color_id = ($row['cat'] == 'I')?$row['package_item']:0;

    $type = ($row['cat'] == 'I')?"Equipments":(($row['cat'] == 'G')?"Giveaways":(($row['cat'] == 'IC')?"Invitational Cards":(($row['cat'] == 'C')?"Cake":(($row['cat'] == 'F')?"Freebies":"Entertainment"))));

    $cake_size = mysql_fetch_array(mysql_query("SELECT * FROM tbl_cake_sizes WHERE size_id = '$size_id'"));

    $design_color = mysql_fetch_array(mysql_query("SELECT * FROM tbl_design_colors WHERE design_id = '$color_id'"));
    $sel_color = (!empty($design_color['color']))?"<div style='background-color: ".$design_color['color'].";height: 20px;width: 50px;'></div>":"No Selected Color";


    $size = ($row['cat'] == 'C')?$cake_size['size']:"<span style='color:red'>N/A</span>";

    $color = ($row['cat'] == 'I')?$sel_color:"<span style='color:red'>N/A</span>";

    $list['detail_id'] = $row['package_detail_id'];
    $list['count'] = $count++;
    $list['type'] = $type;
    $list['item'] = getItemName($row['package_item'], $row['cat']);
    $list['size'] = $size;
    $list['color'] = $color;
    $list['quantity'] = $row['item_qntty'];
    $list['action'] = "<button class='btn btn-sm btn-danger' id='deleteItems' onclick='deleteFromPackage(".$row['package_detail_id'].")'><span class='fa fa-trash'></span> Delete</button>";

    array_push($response['data'],$list);
}
	echo json_encode($response);