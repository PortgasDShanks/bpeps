<?php 
include('../main/configuration.php');
$curDate = getCurrentDate();
$userID = $_SESSION['cust_user_id'];
$refNUm = $_POST['refNUm'];
$dateEvent = date("Y-m-d", strtotime($_POST['dateEvent']));
$bType = $_POST['bType'];
$eventTF = $_POST['eventTF'];
$eventTT = $_POST['eventTT'];

$is_pckg = ($bType == 'P')?1:0;
$data = array(
            "ref_number" => $refNUm, 
            "user_id" => $userID, 
            "trans_date" => $dateEvent, 
            "trans_time_from" => $eventTF, 
            "trans_time_to" => $eventTT,
            "date_added" => $curDate,
            "is_package" => $is_pckg,
            "status" => 0
            );

            $return = insert_query("tbl_transactions",$data,"Y");

            echo $return;