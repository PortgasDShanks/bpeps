<?php 
include('../main/configuration.php');
$user_id = $_SESSION['cust_user_id'];
$type = $_POST['type'];

switch ($type) {
	case 'equip':
		$design_id = $_POST['design_id'];
		$equipQntty = $_POST['equipQntty'];
		$equipPrice = $_POST['equipPrice'];
		$is_package = $_POST['is_package'];
		$array = array("user_id" => $user_id, "item_id" => $design_id, "is_package" => $is_package, "item_type" => "I", "item_qntty" => $equipQntty , "item_price" => $equipPrice);

		$return = insert_query("tbl_booking_cart", $array , "N");
		break;
	case 'invcard':
		$design_id = $_POST['design_id'];
		$equipQntty = $_POST['equipQntty'];
		$equipPrice = $_POST['equipPrice'];
		$is_package = $_POST['is_package'];
		$array = array("user_id" => $user_id, "item_id" => $design_id, "is_package" => $is_package, "item_type" => "IC", "item_qntty" => $equipQntty , "item_price" => $equipPrice);

		$return = insert_query("tbl_booking_cart", $array , "N");
		break;
	case 'giveaway':
		$design_id = $_POST['design_id'];
		$equipQntty = $_POST['equipQntty'];
		$equipPrice = $_POST['equipPrice'];
		$is_package = $_POST['is_package'];
		$array = array("user_id" => $user_id, "item_id" => $design_id, "is_package" => $is_package, "item_type" => "G", "item_qntty" => $equipQntty , "item_price" => $equipPrice);

		$return = insert_query("tbl_booking_cart", $array , "N");
		break;
	case 'cake':
		$design_id = $_POST['design_id'];
		$equipQntty = $_POST['equipQntty'];
		$equipPrice = $_POST['equipPrice'];
		$is_package = $_POST['is_package'];
		$array = array("user_id" => $user_id, "item_id" => $design_id, "is_package" => $is_package, "item_type" => "C", "item_qntty" => $equipQntty , "item_price" => $equipPrice);

		$return = insert_query("tbl_booking_cart", $array , "N");
		break;
	default:
		# code...
		break;
}

echo $return;