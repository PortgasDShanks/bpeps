<?php 
include('../main/configuration.php');

$venueName = clean($_POST['venueName']);
$venueadd = $_POST['venueadd'];

$restrict_period = str_replace(".","",$venueadd);
$formattedAddr = str_replace(' ','+',$restrict_period);

$geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddr.'&key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0');

$output= json_decode($geocode);

$addr_latitude = $output->results[0]->geometry->location->lat;
$addr_longitude = $output->results[0]->geometry->location->lng;

$data = array("venue_name" => $venueName, "venue_address" => $venueadd, "latitude" => $addr_latitude, "longitude" => $addr_longitude);

$query = insert_query("tbl_venue",$data,"N");

echo $query;