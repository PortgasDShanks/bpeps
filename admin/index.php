<?php 
include 'main/configuration.php';
$user_id = $_SESSION['user_id'];
$cat = $_SESSION['user_access'];

$admin = ($cat == 0)?"Administrator":"User";
checkSession();

$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';
?>
<!DOCTYPE HTML>
<html>
<head>
<title>BPEPS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- CSS -->
<link href="../assets/admin_assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="../assets/admin_assets/css/style.css" rel='stylesheet' type='text/css' />
<link href="../assets/admin_assets/css/font-awesome.css" rel="stylesheet">
<link href='../assets/admin_assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<link rel="stylesheet" href="../assets/admin_assets/css/export.css" type="text/css" media="all" />
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<link href="../assets/admin_assets/css/custom.css" rel="stylesheet">
<link href="../assets/admin_assets/css/dataTables.bootstrap4.css" rel="stylesheet">
<link href='../assets/admin_assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='../assets/admin_assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link href="../assets/admin_assets/css/fSelect.css" rel="stylesheet">
<!-- <link href="../assets/admin_assets/css/bootstrap-tagsinput.css" rel="stylesheet"> -->

 <!-- js-->
 <script src="../assets/admin_assets/js/jquery-1.11.1.min.js"></script>

  <script src='../assets/admin_assets/js/SidebarNav.min.js' type='text/javascript'></script>
  <script src='../assets/admin_assets/fullcalendar/lib/moment.min.js'></script>
  <script src="../assets/admin_assets/fullcalendar/fullcalendar.min.js"></script>
  <script src="../assets/admin_assets/js/fSelect.js"></script>
  <script src="../assets/admin_assets/js/highcharts1.js"></script>
<script src="../assets/admin_assets/js/highcharts-more.js"></script>
<script src="../assets/admin_assets/js/exporting.js"></script>
<script src="../assets/admin_assets/js/bootstrap-tagsinput.js"></script>
<!-- <script src="../assets/admin_assets/js/bootstrap-tagsinput-angular.js"></script> -->
  <script src="../assets/admin_assets/js/jquery.nicescroll.js"></script>
  
  
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}

</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
    <?php include 'main/sidebar.php'?>
  
    <?php include 'main/header.php'; ?>
      <!-- main content start-->
      <div id="page-wrapper">
        <div class="main-page">
          <div class="row-one widgettable">
              <?php 
                include 'main/routes.php';
              ?>
          </div>
        </div>
      </div>
    <?php include 'main/footer.php'; ?>
  </div>
 
  <script src="../assets/admin_assets/js/jquery.dataTables.js"></script>
  <script src="../assets/admin_assets/js/dataTables.bootstrap4.js"></script>
  <script src="../assets/admin_assets/js/notify.min.js"></script>
  <script src="../assets/admin_assets/js/scripts.js"></script>
  <script src="../assets/admin_assets/js/SimpleChart.js"></script>
  <script src="../assets/admin_assets/js/bootstrap.js"> </script>
  <script src="../assets/admin_assets/js/modernizr.custom.js"></script>
  <script src="../assets/admin_assets/js/Chart.js"></script>
  <script src="../assets/admin_assets/js/metisMenu.min.js"></script>
  <script src="../assets/admin_assets/js/custom.js"></script>
  <script src="../assets/admin_assets/js/amcharts.js"></script>
  <script src="../assets/admin_assets/js/serial.js"></script>
  <script src="../assets/admin_assets/js/export.min.js"></script>
  <script src="../assets/admin_assets/js/light.js"></script>
  <script src="../assets/admin_assets/js/index1.js"></script>
  <script src="../assets/admin_assets/js/Chart.bundle.js"></script>
  <script src="../assets/admin_assets/js/utils.js"></script>
  <script src="../assets/admin_assets/js/classie.js"></script>
 
  
		<script>
      $(document).ready( function(){
          $('.sidebar-menu').SidebarNav()
          $(".fSelect").fSelect({
            placeholder: " -- Please Choose -- ",
            
          });

           $('[data-toggle="tooltip"]').tooltip();
      });
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
        function gotoTrans(notif,id){
            $.post("ajax/notif_update.php", {
                id: id,
                notif: notif
            }, function(data){
                window.location = 'index.php?view=view-details&id='+id+'&cat='+data;
            })
        }
        function gotoMsg(notif,id){
          $.post("ajax/notif_update_msg.php", {
                id: id,
                notif: notif
            }, function(data){
                window.location = 'index.php?view=messages&id='+id;
            })
        }
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}

      function logout(){
        window.location.href = 'ajax/logout.php';
      }
      function alertMe(icon,title,message,type){
        $.notify({
          icon: icon,
          title: title,
          message: message
        },{
          type: type
        });
      }
      function failed_query(){
        $.notify({
          icon: "fa fa-exclamation-circle",
          message: "Unable to save data, Contact Technical Support"
        });
      }
		</script>
</body>
</html>