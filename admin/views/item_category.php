<div class="content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>EQUIPMENT CATEGORY</h3>
        </div>
        <div class="card-body">
        	<div class='row'>
            	<div class='col-md-12'>
        		<button class='btn btn-sm btn-default pull-right' id='delete_i' onclick='deleteEquip()'><span class='fa fa-trash'></span> Delete</button>
        		<button class='btn btn-sm btn-default pull-right' onclick='addcatModalShow()'><span class='fa fa-plus-circle'></span> Add</button>
        	</div>
        	
        	<div class='col-md-12' style='margin-top:10px;'>
                <table id='category' class="table" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>CATEGORY NAME</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
<?php require 'modals/item_cat_modal.php'; ?>
<script type="text/javascript">
	$(document).ready( function(){
		category();
	});
	function addcatModalShow(){
		$("#addcat").modal('show');
	}
	function addCat(){
		var catname = $("#catname").val();
		var action = 'add';
		$.post("ajax/item_category.php", {
			catname: catname,
			action: action
		}, function(data){
			$("#addcat").modal('hide');
			if(data > 0){
				alertMe("fa fa-check-circle","All Good!","Equipment Successfully Added","success");
			}else{
				failed_query();
			}
			category();
		});
	}
	function deleteEquip(){
		var count_checked = $('input[name="checkbox_cat"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_i").prop("disabled", true);
        $("#delete_i").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/item_category.php", {
        	checked_array: count_checked,
        	action: action
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Equipment Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_i").prop("disabled", false);
        	$("#delete_i").html("<span class='fa fa-trash'></span> Delete");
        	category();
        });
	}
	function category(){
        $("#category").DataTable().destroy();
        $('#category').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/category.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_cat' value='"+row.cat_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"cat_name"
            }
            
        ]   
        });
    }
</script>