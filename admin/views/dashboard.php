<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>DASHBOARD</h3>
        </div>
        <div class='card-body'>
            <div class='row'>
                <div class="col-lg-12" style='margin-top: 10px;'>
                    <div id="calendar"></div>
                </div>
            </div>
        </div> 
    </div>
</div>
</div>
<?php require 'modals/view_event.php';?>
<?php 

$query_getEvents = mysql_query("SELECT *  FROM tbl_transactions as t,tbl_users as u WHERE t.user_id = u.user_id");
  $count_events = mysql_num_rows($query_getEvents);
?>
<script>
    function gotoTransval(){
        var id = $("#transID").val();
        var type = $("#transtype").val();

        window.location = 'index.php?view=view-details&id='+id+'&cat='+type;
    }
$(document).ready( function(){
    $('#calendar').fullCalendar({
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
      events: [
        <?php 
          
        $ctrEvent=1;
        while($fetch_events = mysql_fetch_array($query_getEvents)){
          $event_title = $fetch_events['firstname']." ".$fetch_events['lastname'];
          $start_date = $fetch_events['trans_date'];
          $end_date = date("Y-m-d", strtotime('+1 day', strtotime($fetch_events['trans_date'])));
          $event_time = date("g:ia", strtotime($fetch_events['trans_time_from'])).'-'.date("g:ia", strtotime($fetch_events['trans_time_to']));
          $e_time = date("g:ia", strtotime($fetch_events['trans_time_from'])).' &mdash; '.date("g:ia", strtotime($fetch_events['trans_time_to']));
        ?>
        {
          title: '<?php echo strtoupper($fetch_events['firstname']." ".$fetch_events['lastname'])." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          end: '<?php echo $end_date?>',
          color: '#3498db',
          id: '<?php echo $fetch_events['trans_id']; ?>',
          event_date: '<?php echo date("F d, Y", strtotime($fetch_events['trans_date'])); ?>',
          event_time: '<?php echo $event_time ?>',
          event_title: '<?php echo strtoupper($fetch_events['firstname']." ".$fetch_events['lastname']);?>',
          trans_type: '<?php echo $fetch_events['is_package']?>'
        }<?php 
          if($ctrEvent < $count_events){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
        
      ],
      eventClick: function(callEvent , jsEvent, view){
        $("#viewEvent").modal();

        $("#customer").val(callEvent.event_title);
        $("#deliverydate").val(callEvent.event_date);
        $("#deliverytime").val(callEvent.event_time);
        $("#transID").val(callEvent.id);
        $("#transtype").val(callEvent.trans_type);
       
      },
        eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2100-01-01'
        }
    });
});
</script>