<style>
ul.dropdown-menu li {
    margin-left: 0;
    width: 100%;
    padding: 0;
    background: #444;
}
ul.dropdown-menu {
    padding: 1em;
    min-width: 200px;
    top: 101%;
}
</style>
<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>CAKES</h3>
        </div>
        <div class='row'>
            <div class='col-md-12'>
                <button class='btn btn-sm btn-default pull-right' id='delete_in' onclick='deletecake()'><span class='fa fa-trash'></span> Delete</button>
                <button class='btn btn-sm btn-default pull-right' onclick="addCakeModalShow()"><span class='fa fa-plus-circle'></span> Add</button>
            </div>
            <div class='col-md-12' id='thumbnail_div' style='margin-top: 10px'>
                <table id='cakeList' class="table" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>IMAGE</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>NAME</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>SIZES</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php require 'modals/add_cake_modal.php';?>
.<?php require 'modals/update_cake_modal.php';?>
<?php require 'modals/add_size_modal.php';?>
<script>
$(document).ready( function(){
    cakeList();
    $("#cakeAdd").on('submit',(function(e) {
        e.preventDefault();
            $("#btn_add_c").prop("disabled", true);
            $("#btn_add_c").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/add_cake.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                $("#addcake").modal('hide');
                if(data == 1){
                    alertMe("fa fa-check-circle","All Good!","Cake Design Successfully Added","success");
                }else{
                    alert("Error");
                }
                cakeList();
                $("#btn_add_c").prop("disabled", false);
                $("#btn_add_c").html("<span class='fa fa-check-circle'></span> Continue ");  
            },error: function(){
                alert("Error");
            }
            
        });
            
        }));
});
    $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
function addCakeModalShow(){
    $("#addcake").modal('show');
}
function addSizes(id){
    $("#cakeID").val(id);
    $("#cakeAddSize").modal();

}
function deletecake(){
    var count_checked = $('input[name="checkbox_equip"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_in").prop("disabled", true);
        $("#delete_in").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/update_delete_cakes.php", {
        	checked_array: count_checked,
        	action: action
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Cake Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_in").prop("disabled", false);
        	$("#delete_in").html("<span class='fa fa-trash'></span> Delete");
        	cakeList();
        });
}
function cakeSize(){
    var cakeSize = $("#cakeSize").val();
    var cakeID = $("#cakeID").val();
    $.post("ajax/addSizes.php", {
        cakeSize: cakeSize,
        cakeID: cakeID
    }, function(data){
        $("#cakeAddSize").modal('hide');
        if(data == 1){
            alertMe("fa fa-check-circle","All Good!","Cake Size Successfully Added","success");
        }else{
            alert("Error");
        }
        cakeList();
    });
}
function editRecord(id,name){
    $("#cakeNameU").val(name);
    $("#cakeIDU").val(id);
    $("#updateE").modal();
}
function updateMe(){
    var cakeNameU = $("#cakeNameU").val();
    var cakeIDU = $("#cakeIDU").val();
    var action = 'update';
    $("#btn_update").prop("disabled", true);
    $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Updating");
    $.post("ajax/update_delete_cakes.php", {
        cakeNameU: cakeNameU,
        cakeIDU: cakeIDU,
        action: action
    }, function(data){
        $("#updateE").modal('hide');
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Cake Successfully Updated","success");
        }else{
            failed_query();
        }
        $("#btn_update").prop("disabled", false);
        $("#btn_update").html("<span class='fa fa-check-circle'></span> Continue");
        cakeList();
    });
}
function cakeList(){
        $("#cakeList").DataTable().destroy();
        $('#cakeList').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/cakeList.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_equip' value='"+row.cake_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"cake_img"
            },
            {
                "data":"cake_name"
            },
            {
                "data":"sizes"
            }
            
        ]   
        });
    }
</script>