<?php 
$id = $_GET['id'];

?>
<style>
.fs-label-wrap{
  height: 35px;
  width: 150%;
}
.fs-dropdown{
    width: 65%;
}
 #div2 {
  height: 377px;
  overflow: auto;
  /*border: 1px solid #eeeeee;
  border-radius: 5px;
  box-shadow: 3px 4px 6px 1px #eeeeee;*/
}
</style>
<div class='row'>
<div class="col-md-12 content-top-2 card">
    <?php if($id == 0) { ?>
        <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>ADD PACKAGE LISTS</h3>
        </div>
        <div class='card-body'>
            <div class='row'>
            <form id="addPckgHeader" method="POST" action="" enctype="multipart/form-data">
                <div class='col-md-12' style='text-align: center'>
                    <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="../assets/images/img_upload.png" style="object-fit: cover;width: 400px;height: 200px;">

                    <div class="image-upload" style="margin-top: 5px;margin-left: 52px;">
                    <input type="file" name="avatar" style='visibility: hidden' required id="files" class="btn-inputfile share" />
                    <label for="files" class="btn default" style="font-size: 16px;margin-top:-40px;margin-right: 30px;"><i class="fa fa-file-image-o"></i> CHOOSE IMAGE </label>
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Package Name (Theme): </div>
                            <select name="designName" id="designName" class='form-control' required>
                                <?php echo getAllThemes(); ?>
                            </select>
                           
                        </div>
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Package Type: </div>
                            <select name="partyType" id="partyType" class='form-control' required>
                                <?php echo getTypes(); ?>
                            </select>
                           
                        </div>
                    </div>
                </div>
               <!--  <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Venue: </div>
                            <select name="venueID" class='form-control' id="venueID" required>
                                <?php echo getVenue(); ?>
                            </select>
                        </div>
                    </div>
                </div> -->
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Price: </div>
                            <input type="text" id="pckg_price" name="pckg_price" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Description: </div>
                            <textarea style='resize:none' required rows='1' id="pckg_desc" name="pckg_desc" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class="form-group">
                        <button class='btn btn-primary btn-sm pull-right' id='addHeader' onclick="addHeader()"><span class='fa fa-check-circle'></span> Continue</button>
                    </div>
                </div>
                </form>
                <div class='row' >
                <div class='col-md-12' style='border: 1px solid #cccccc;margin: 12px 0px 12px 0px;'></div>
                    <div class='inclusions' style='display: none;'>
                        <div class='col-md-12'>
                            <label for="">INCLUSIONS: </label>
                            <input type='hidden' id='headerID'>
                        </div> 
                        <div class='col-md-12'>
                           <span class="btn-group">
                               <button class='btn btn-sm btn-success' onclick="viewAddedToPackage()" style='float: right;'><span class='fa fa-eye'></span> View Package </button>
                           </span>
                        </div>  
                         <?=getAllEquipments_package()?>

                         <?=getAddtional_package()?>
                    </div>
                </div>
                </div>
            </div>
        </div>
    <?php } else { 
         $getDetails = mysql_fetch_array(mysql_query("SELECT * FROM tbl_package_header WHERE package_header_id = '$id'"));
        $img = $getDetails['package_img'];
        
    ?> 
        <input type='hidden' id='headerID' value='<?=$id?>'>
        <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>UPDATE PACKAGE LISTS</h3>
        </div>
        <div class='card-body'>
            <div class='row'>
            <form id="updatePkgHeader" method="POST" action="" enctype="multipart/form-data">
                <input type="hidden" value="<?php echo $id ?>" name="pkgHead">
                <div class="col-md-12" style='text-align: center'>
                    <?php if(!empty($getDetails['package_img'])){?>
                    <img id="img_wrap" class="previewImage01 image-wrap" src="../assets/images/<?php echo $getDetails['package_img'];?>" style="object-fit: cover;width: 400px;height: 200px;">
                    <?php } else { ?>
                    <img id="img_wrap" class="previewImage01 image-wrap" src="../assets/images/img_upload.png" style="object-fit: cover;width: 400px;height: 200px;">
                    <?php } ?>
                    <!-- <div class="image-upload" style="margin-top: 5px;margin-left: 30px;">
                    <input type="file" name="avatar" id="files" class="btn-inputfile share" />
                      <label for="files" class="btn default" style="font-size: 16px;"><i class="fa fa-file-image-o"></i> Change</label> -->
                  </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Package Name (Theme): </div>
                            <select name="UdesignName" id="UdesignName" class='form-control' required>
                                <?php echo getSelectedTheme($getDetails['theme_id']); ?>
                            </select>
                           
                        </div>
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Package Type: </div>
                            <select name="Upartytype" id="Upartytype" class='form-control' required>
                                <?php echo getSelectedType($getDetails['type_id']); ?>
                            </select>
                           
                        </div>
                    </div>
                </div>
                <!-- <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Venue: </div>
                            <select name="venueID" class='form-control' id="venueID">
                                <option value="">&mdash; Please Choose &mdash;</option>
                                <?php 
                                $getV = mysql_query("SELECT * FROM tbl_venue");
                                while($vRow = mysql_fetch_array($getV)){
                                    $selected = ($vRow['venue_id'] == $getDetails['package_venue'])?"selected":"";
                                    $id = $vRow['venue_id'];
                                    echo "<option ".$selected." value='".$id."'>".$vRow['venue_name']."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div> -->
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Price: </div>
                            <input type="text" id="pckg_price" value="<?php echo $getDetails['package_price']?>" name="pckg_price" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Description: </div>
                            <textarea style='resize:none' rows='1' id="pckg_desc" name="pckg_desc" class="form-control"><?php echo $getDetails['package_desc']?></textarea>
                        </div>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class="form-group">
                        <button class='btn btn-primary btn-sm pull-right' type='submit' id='updateHeader'><span class='fa fa-check-circle'></span> Save Changes</button>
                    </div>
                </div>
                </form>
                <div class='col-md-12' style='border: 1px solid #cccccc;margin: 12px 0px 12px 0px;'></div>
                <div class='inclusions' style=''>
                    <div class='col-md-12'>
                        <label for="">INCLUSIONS: </label>
                       
                    </div>
                    <div class='col-md-12'>
                       <span class="btn-group">
                           <button class='btn btn-sm btn-success' onclick="viewAddedToPackage()" style='float: right;'><span class='fa fa-eye'></span> View Package </button>
                       </span>
                    </div>  
                     <?=getAllEquipments_package()?>

                     <?=getAddtional_package()?>
                     <?=getEntertainments_package()?>  
                </div>
            </div>
        </div>
    </div>
    <?php } ?> 
</div>
</div>
<?php require 'views/modals/equipment_modal.php'?>
<?php require 'views/modals/show_items_in_package.php';?>
<script>
$(document).ready( function(){
    $('input.invCheck').on('change', function() {
        $('input.invCheck').not(this).prop('checked', false);  
    });

    $('input.gaCheck').on('change', function() {
        $('input.gaCheck').not(this).prop('checked', false);  
    });

    $("#addPckgHeader").on('submit',(function(e) {
        e.preventDefault();
            $("#addHeader").prop("disabled", true);
            $("#addHeader").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/package_add_header.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                if(data > 0){
                    alertMe("fa fa-check-circle","All Good!","Successfully Added","success");
                    $("#addHeader").prop("disabled", true);
                    $("#addHeader").html("<span class='fa fa-check-circle'></span> Continue ");
                    $("#headerID").val(data);
                    $(".inclusions").css("display","block");

                    $("#files").val("");
                    $("#designName").val("");
                    $("#venueID").val("");
                    $("#pckg_price").val("");
                    $('#pckg_desc').val("");
                }else{
                    alert("Error");
                }
                
            },error: function(){
                alert("Error");
            }
                     
        });
            
        }));

        $("#updatePkgHeader").on('submit',(function(e) {
        e.preventDefault();
            $("#updateHeader").prop("disabled", true);
            $("#updateHeader").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/package_update_header.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                if(data > 0){
                    alertMe("fa fa-check-circle","All Good!","Successfully Added","success");
                    $("#updateHeader").prop("disabled", false);
                    $("#updateHeader").html("<span class='fa fa-check-circle'></span> Save Changes ");
                }else{
                    alert("Error");
                }
                
            },error: function(){
                alert("EEEE");
            }
                     
        });
            
        }));
})
// function allowDrop(ev) {
//   ev.preventDefault();
// }

// function drag(ev,eid) {
//   ev.dataTransfer.setData("text", ev.target.id);

// }

// function drop(ev) {
//   ev.preventDefault();
//   var data = ev.dataTransfer.getData("text");
//   ev.target.appendChild(document.getElementById(data));
//   showModal_color(data);
// }
function addItemToPackage(id, type){
    showModal_color(id, type);
}
function showModal_color(id, type){
    $("#showDetails").modal();
    $.post("ajax/getModalDetails.php", {
        id: id,
        type: type
    }, function(data){
        $("#cont_div").html(data);
    });
}
function additemToPackage(){
    var qtty = $("#designQtty").val();
    var designid = $("#designid").val();
    var headerID = $("#headerID").val();
    var itemType = $("#itemType").val();
    $("#btn_add_i").prop("disabled", true);
    $("#btn_add_i").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    if(itemType == 'I'){
        $.post("ajax/color_checker.php", {
        designid: designid
        }, function(data){
            if(data > 0){
                var color =  $("input[name='designColor']:checked").val();
                insertEquipment_package(color, qtty, designid, headerID);
            }else{
                var conf = confirm("No Color Selected, Would you like to continue?");
                if(conf == true){
                    var color = 0;
                    insertEquipment_package(color, qtty, designid, headerID);
                }
            }
             
        });
    }else{
        var sizes = (itemType == 'C') ? $("#cakeSizeID").val() : designid;
        insertAdditional_package(sizes, qtty, headerID, itemType);
    }
    $("#btn_add_i").prop("disabled", false);
    $("#btn_add_i").html("<span class='fa fa-check-circle'></span> Save Changes");
     
   
}
function insertAdditional_package(size, qtty, headerID, itemType){
    $.post("ajax/addAdditional_package.php", {
        size: size,
        qtty: qtty,
        headerID: headerID,
        itemType: itemType
   }, function(data){
    $("#showDetails").modal('hide');
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Item Successfully Added","success");
        }else if(data < 0){
             alertMe("fa fa-Exclamation","Aw Snap!","Item Already Exist","warning");
        }else{
            failed_query();
        }
       
   });
}
function insertEquipment_package(color, qtty, designid, headerID){
    $.post("ajax/addEquipments_package.php", {
        color: color,
        qtty: qtty,
        designid: designid,
        headerID: headerID
   }, function(data){
    $("#showDetails").modal('hide');
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Item Successfully Added","success");
        }else if(data < 0){
             alertMe("fa fa-Exclamation","Aw Snap!","Item Already Exist","warning");
        }else{
            failed_query();
        }
       
   });
}
function ent_pkg(){
    var ent_data = $("#entertainmentid").val();
    var split_data = ent_data.split("-");
    var item_id = split_data[0];
    var type = split_data[1];
    showModal_color(item_id, type);
}
function viewAddedToPackage(){
    $("#itemInPackage").modal();
    packageDetails();
   
}
function deleteFromPackage(detailID){
    $("#deleteItems").prop("disabled", true);
    $("#deleteItems").html("<span class='fa fa-spinner fa-spin'></span> Deleting");
    $.post("ajax/delete_item_from_package.php", {
        detailID: detailID
    }, function(data){
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Item Successfully Deleted","success");
        }else{
            failed_query();
        }
        $("#deleteItems").prop("disabled", false);
        $("#deleteItems").html("<span class='fa fa-trash'></span> Delete");
        packageDetails();
    });
}
function packageDetails(){
    var headerID = $("#headerID").val();
    $("#item_in_package").DataTable().destroy();
        $('#item_in_package').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/item_in_package.php",
            "dataSrc":"data",
            "data":{
                headerID: headerID
            },
            "type":"POST",
        },
        "columns":[
            {
                "data":"count"
            },
            {
                "data":"type"
            },
            {
                "data":"item"
            },
            {
                "data":"size"
            },
            {
                "data":"color"
            },
            {
                "data":"quantity"
            },
            {
                "data":"action"
            }
            
        ]   
        });
}

$(".btn-inputfile").change(function () {
      $("#btn-edit").prop("disabled", false);
      var input = document.getElementById('files');
      previewFile(input);
  });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
  function ifIncludeIC(){
      if($("#checker").prop("checked")){
        $("#tbodyIC").fadeIn();
      }else{
        $("#tbodyIC").fadeOut();
        $(".invCheck").prop("checked", false);
      }
  }
  function ifIncludeGA(){
    if($("#checkerG").prop("checked")){
        $("#tbodyGA").fadeIn();
      }else{
        $("#tbodyGA").fadeOut();
        $(".gaCheck").prop("checked", false);
      }
  }
  function getEquipID(d_id){
    $.post("ajax/getEquipmentID.php", {
        d_id: d_id
    }, function(data){

    });
  }
  function getQntty(e_id){
    var d_id = $("#equipDesign"+e_id).val();
    var qtty = $("#qntty"+e_id).val();
   
        $("#valtoUse"+e_id).val(d_id+"-"+qtty);

  }
  function getDesignID(e_id){
  
    var d_id = $("#equipDesign"+e_id).val();
    var qtty = $("#qntty"+e_id).val();

    $("#valtoUse"+e_id).val(d_id+"-"+qtty);
  }
  
</script>