<style>
ul.dropdown-menu li {
    margin-left: 0;
    width: 100%;
    padding: 0;
    background: #444;
}
ul.dropdown-menu {
    padding: 1em;
    min-width: 200px;
    top: 101%;
}
</style>
<div class="content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>FREEBIES</h3>
        </div>
        <div class="card-body">
        	<div class='row'>
            	<div class='col-md-12'>
        		<button class='btn btn-sm btn-default pull-right' id='delete_e' onclick='deletefb()'><span class='fa fa-trash'></span> Delete</button>
        		<button class='btn btn-sm btn-default pull-right' onclick='addEquipModalShow()'><span class='fa fa-plus-circle'></span> Add</button>
        	</div>
        	
        	<div class='col-md-12' style='margin-top:10px;'>
                <table id='freebies' class="table" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>IMAGE</th>
                             <th style='background-color: rgb(34 45 50);color: #ffffff;'>NAME</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
<?php require 'modals/add_fb_modal.php'; ?>
<?php require 'modals/update_freebies_modal.php'; ?>
<script type="text/javascript">
	$(document).ready( function(){
		freebies();

        $("#fbAddForm").on('submit',(function(e) {
        e.preventDefault();
            $("#btn_add_f").prop("disabled", true);
            $("#btn_add_f").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/add_freebie.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                $("#fbAdd").modal('hide');
                if(data == 1){
                    alertMe("fa fa-check-circle","All Good!","Item Design Successfully Added","success");
                }else{
                    alert("Error");
                }
                freebies();
                $("#btn_add_f").prop("disabled", false);
                $("#btn_add_f").html("<span class='fa fa-check-circle'></span> Continue ");  

                
            },error: function(){
                alert("Error");
            }
                     
        });
            
        }));
	});
    $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
    function viewDesigns(equipID){
        window.location = 'index.php?view=item&equipmentid='+equipID;
    }
	function addEquipModalShow(){
		$("#fbAdd").modal('show');
	}
	function addEquip(){
		var equipment = $("#equipment").val();
		var itemCat = $("#itemCat").val();
		var action = 'add';
		$.post("ajax/equipments.php", {
			equipment: equipment,
			itemCat: itemCat,
			action: action
		}, function(data){
			$("#addequipment").modal('hide');
			if(data > 0){
				alertMe("fa fa-check-circle","All Good!","Equipment Successfully Added","success");
			}else{
				failed_query();
			}
			equipments();
		});
	}
	function deletefb(){
		var count_checked = $('input[name="checkbox_equip"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_e").prop("disabled", true);
        $("#delete_e").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/update_delete_freebies.php", {
        	checked_array: count_checked,
        	action: action
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Freebies Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_e").prop("disabled", false);
        	$("#delete_e").html("<span class='fa fa-trash'></span> Delete");
        	freebies();
        });
	}
    function editRecord(id,iname){
        var fbNameU = $("#fbNameU").val(iname);
        var fbIDU = $("#fbIDU").val(id);
        $("#updateE").modal();
    }
    function updateMe(){
        var fbNameU = $("#fbNameU").val();
        var fbIDU = $("#fbIDU").val();
        var action = 'update';
        $("#btn_update").prop("disabled", true);
        $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Updating");
        $.post("ajax/update_delete_freebies.php", {
            fbNameU: fbNameU,
            fbIDU: fbIDU,
            action: action
        }, function(data){
            $("#updateE").modal('hide');
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Freebies Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#btn_update").prop("disabled", false);
        	$("#btn_update").html("<span class='fa fa-check-circle'></span> Continue");
            freebies();
        });
    }
	function freebies(){
        $("#freebies").DataTable().destroy();
        $('#freebies').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/freebies.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_equip' value='"+row.fb_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"fb_img"
            },
            {
                "data":"fb_name"
            }
            
        ]   
        });
    }
</script>