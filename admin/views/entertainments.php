<style>
ul.dropdown-menu li {
    margin-left: 0;
    width: 100%;
    padding: 0;
    background: #444;
}
ul.dropdown-menu {
    padding: 1em;
    min-width: 200px;
    top: 101%;
}
</style>
<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>ENTERTAINMENT</h3>
        </div>
        <div class='row'>
            <div class='col-md-12'>
                <button class='btn btn-sm btn-default pull-right' id='delete_in' onclick='deleteent()'><span class='fa fa-trash'></span> Delete</button>
                <button class='btn btn-sm btn-default pull-right' onclick="addEntShow()"><span class='fa fa-plus-circle'></span> Add</button>
            </div>
            <div class='col-md-12' id='thumbnail_div' style='margin-top: 10px'>
                <table id='EntList' class="table" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                        <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>NAME</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>FEE</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>HOURS</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>ADDITIONAL</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<?php require 'modals/add_ent_modal.php';?>
<?php require 'modals/update_ent_modal.php';?>
<script>
$(document).ready( function(){
    EntList();
});
    $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
function addEntShow(){
    $("#addEnt").modal('show');
}
function addSizes(id){
    $("#cakeID").val(id);
    $("#cakeAddSize").modal();

}
function addEntertainment(){
    var TypeName = $("#TypeName").val();
    var Fee = $("#Fee").val();
    var Hours = $("#Hours").val();
    var addHours = $("#addHours").val();
    var addFee = $("#addFee").val();
    $.post("ajax/addEntertainment.php", {
        TypeName: TypeName,
        Fee: Fee,
        Hours: Hours,
        addHours: addHours,
        addFee: addFee
    }, function(data){
        $("#addEnt").modal('hide');
        if(data == 1){
            alertMe("fa fa-check-circle","All Good!","Entertainment Successfully Added","success");
        }else{
            alert("Error");
        }
        EntList();
    });
}
function deleteent(){
    var count_checked = $('input[name="checkbox_equip"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_in").prop("disabled", true);
        $("#delete_in").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/update_delete_ent.php", {
        	checked_array: count_checked,
        	action: action
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Entertainment Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_in").prop("disabled", false);
        	$("#delete_in").html("<span class='fa fa-trash'></span> Delete");
        	EntList();
        });
}
function editRecord(id){
    $("#update_ent").modal();
    $.post("ajax/getEntData.php", {
        id: id
    }, function(data){
        var ent = JSON.parse(data);
        $("#entIDU").val(id);
        $("#TypeNameU").val(ent.ent_type);
        $("#FeeU").val(ent.ent_fee);
        $("#HoursU").val(ent.ent_hrs);
        $("#addHoursU").val(ent.ent_add_fee_hrs);
        $("#addFeeU").val(ent.ent_add_fee);
    });
}
function updateMe(){
    var entIDU = $("#entIDU").val();
    var TypeNameU = $("#TypeNameU").val();
    var FeeU = $("#FeeU").val();
    var HoursU = $("#HoursU").val();
    var  addHoursU = $("#addHoursU").val();
    var addFeeU = $("#addFeeU").val();
    var action = 'update';
    $("#btn_up").prop("disabled", true);
    $("#btn_up").html("<span class='fa fa-spin fa-spinner'></span> Updating");
    $.post("ajax/update_delete_ent.php", {
        entIDU: entIDU,
        TypeNameU: TypeNameU,
        FeeU: FeeU,
        HoursU: HoursU,
        addHoursU: addHoursU,
        addFeeU: addFeeU,
        action: action
    }, function(data){
        $("#update_ent").modal('hide');
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Entertainment Successfully Updated","success");
        }else{
            failed_query();
        }
        $("#btn_up").prop("disabled", false);
        $("#btn_up").html("<span class='fa fa-check-circle'></span> Continue");
        EntList();
    });
}
function EntList(){
        $("#EntList").DataTable().destroy();
        $('#EntList').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/EntList.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_equip' value='"+row.ent_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"ent_type"
            },
            {
                "data":"ent_fee"
            },
            {
                "data":"ent_hrs"
            },
            {
                "data":"ent_add"
            }
            
        ]   
        });
    }
</script>