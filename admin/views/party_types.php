<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>PARTY TYPES</h3>
        </div>
        <div class='card-body'>
            <div class='row'>
                <div class='col-md-12'>
                    <button class='btn btn-sm btn-default pull-right' id='delete_e' onclick='deleteptype()'><span class='fa fa-trash'></span> Delete</button>
                    <button class='btn btn-sm btn-default pull-right' data-toggle='modal' data-target='#typeAdd'><span class='fa fa-plus-circle'></span> Add</button>
                </div>
                <div class='col-md-12' style='margin-top:10px;'>
                    <table id='partytypes' class="table" style='margin-top:10px;width: 100%;'>
                        <thead>
                            <tr>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>NAME</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>DESCRIPTION</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
    </div>
</div>
</div>
<?php require 'modals/add_ptype_modal.php'; ?>
<?php require 'modals/update_ptype_modal.php'; ?>
<script>
$(document).ready( function(){
    getPartyTypes();
}); 
function updateDetails(id,name,desc){
    $("#typeUpdate").modal();
    $("#utypeName").val(name);
    $("#utypeDesc").val(desc);
    $("#utypeID").val(id);
}
function updatepartytypes(){
    var name = $("#utypeName").val();
    var desc = $("#utypeDesc").val();
    var id = $("#utypeID").val();
    var action = 'update';
    $("#btn_u_t").prop("disabled", true);
    $("#btn_u_t").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/add_party_types.php", {
        name: name,
        desc: desc,
        action: action,
        id: id
    }, function(data){
        $("#typeUpdate").modal('hide');
        if(data == 1){
            alertMe("fa fa-check-circle","All Good!","Party Type Successfully changed","success");
        }else{
            failed_query();
        }
        getPartyTypes();
    })
}
function deleteptype(){
        var count_checked = $('input[name="checkbox_party_type"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_e").prop("disabled", true);
        $("#delete_e").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/add_party_types.php", {
            checked_array: count_checked,
            action: action
        }, function(data){
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Theme Successfully Deleted","success");
            }else{
                failed_query();
            }

            $("#delete_e").prop("disabled", false);
            $("#delete_e").html("<span class='fa fa-trash'></span> Delete");
            getPartyTypes();
        });
    }
function addpartytypes(){
    var name = $("#typeName").val();
    var desc = $("#typeDesc").val();
    var action = 'add';
    $("#btn_add_t").prop("disabled", true);
    $("#btn_add_t").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/add_party_types.php", {
        name: name,
        desc: desc,
        action: action
    }, function(data){
        $("#typeAdd").modal('hide');
        if(data == 1){
            alertMe("fa fa-check-circle","All Good!","Party Type Successfully Added","success");
        }else if(data == 2){
            alertMe("fa fa-check-circle","All Good!","Party Type Already Exist","warning");
        }else{
            failed_query();
        }
        getPartyTypes();
    })
}
function getPartyTypes(){
    $("#partytypes").DataTable().destroy();
        $('#partytypes ').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/party_types_list.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_party_type' value='"+row.party_type_id+"'>";        
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"type_name"
            },
            {
                "data":"type_desc"
            }
            
        ]   
        });
}
</script>