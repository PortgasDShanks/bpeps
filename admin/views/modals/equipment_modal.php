<div class="modal fade" id="showDetails" tabindex="-1" role="dialog" aria-labelledby="showDetailsLabel" data-backdrop='static'>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-plus-circle'></span> EQUIPMENTS </h4>
            </div>
            <div class="modal-body">
                <div class='row' id='cont_div'>
                
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_i" onclick="additemToPackage()" type="button"><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>