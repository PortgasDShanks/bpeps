<div class="modal fade" id="colorModal" tabindex="-1" role="dialog" aria-labelledby="colorModalLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-eye'></span> DESIGN COLORS </h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                   <div class='col-md-12' id='colorDiv'>
                    
                   </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>