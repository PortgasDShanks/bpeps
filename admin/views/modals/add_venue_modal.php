<div class="modal fade" id="addVenue" tabindex="-1" role="dialog" aria-labelledby="addVenueLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-plus-circle'></span> Venues </h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Name: </div>
                                <input type="text" id="venueName" name="venueName" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Address: </div>
                                <input type="text" id="venueadd" name="venueadd" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_v" type="button" onclick='addVenue()'><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>