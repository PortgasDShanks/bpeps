<div class="modal fade" id="cakeAddSize" tabindex="-1" role="dialog" aria-labelledby="cakeAddSizeLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-plus-circle'></span> CAKE SIZES </h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Size: </div>
                                <input type="text" id="cakeSize" name="cakeSize" class="form-control">
                                <input type="hidden" id="cakeID" name="cakeID" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_s" type="button" onclick="cakeSize()"><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>