<div class="modal fade" id="addequipment" tabindex="-1" role="dialog" aria-labelledby="addequipmentLabel" data-backdrop='static'>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-plus-circle'></span> Add New Equipment Name</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Category: </div>
                       <select id="itemCat" class="form-control">
                           <?php echo getCategories();?>
                       </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Name: </div>
                        <input type="text" id="equipment" name="equipment" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_e" type="button" onclick='addEquip()'><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>