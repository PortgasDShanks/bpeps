<form id="giveawayAdd" method="POST" action="" enctype="multipart/form-data">
<div class="modal fade" id="addga" tabindex="-1" role="dialog" aria-labelledby="addgaLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-plus-circle'></span> GIVEAWAYS</h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12' style='text-align: center'>
                        <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="../assets/images/img_upload.png" style="object-fit: contain;width:320px;height:200px;">

                        <div class="image-upload" style="margin-top: 5px;margin-left: 52px;">
                        <input type="file" name="avatar" style='visibility: hidden' id="files" class="btn-inputfile share" />
                        <label for="files" class="btn default" style="font-size: 16px;margin-top:-40px;margin-right: 30px;"><i class="fa fa-file-image-o"></i> CHOOSE IMAGE </label>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Name: </div>
                                <input type="text" id="gaName" name="gaName" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_ga" type="submit"><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>
</form>