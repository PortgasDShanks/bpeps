<div class="modal fade" id="typeUpdate" tabindex="-1" role="dialog" aria-labelledby="typeUpdateLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-pencil'></span> UPDATE PARTY TYPES</h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Name: </div>
                                <input type="text" id="utypeName" name="utypeName" class="form-control">
                                <input type="hidden" id="utypeID" name="utypeID" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Description: </div>
                                <textarea class="form-control" id="utypeDesc" style="resize: none;" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_u_t" type="button" onclick="updatepartytypes()"><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>