<div class="modal fade" id="addEnt" tabindex="-1" role="dialog" aria-labelledby="addEntLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-plus-circle'></span> Entertainments </h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Type: </div>
                                <input type="text" id="TypeName" name="TypeName" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Fee: </div>
                                <input type="text" id="Fee" name="Fee" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Hours: </div>
                                <input type="text" id="Hours" name="Hours" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <label for=""> Additional </label>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Hours: </div>
                                <input type="text" id="addHours" name="addHours" class="form-control">
                                
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Fee: </div>
                                <input type="text" id="addFee" name="addFee" class="form-control">
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_e" type="button" onclick='addEntertainment()'><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>