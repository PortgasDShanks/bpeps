<div class="modal fade" id="update_ent" tabindex="-1" role="dialog" aria-labelledby="addEntLabel" data-backdrop='static'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-pencil'></span> Entertainments </h4>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Type: </div>
                                <input type="text" id="TypeNameU" name="TypeNameU" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Fee: </div>
                                <input type="text" id="FeeU" name="FeeU" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Hours: </div>
                                <input type="text" id="HoursU" name="HoursU" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <label for=""> Additional </label>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Hours: </div>
                                <input type="text" id="addHoursU" name="addHoursU" class="form-control">
                                
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Fee: </div>
                                <input type="text" id="addFeeU" name="addFeeU" class="form-control">
                                <input type="hidden" id="entIDU" name="entIDU" class="form-control">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_up" type="button" onclick='updateMe()'><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>