<div class="modal fade" id="updateE" tabindex="-1" role="dialog" aria-labelledby="addcatLabel" data-backdrop='static'>
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-pencil'></span> Update Item Name</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Name: </div>
                        <input type="text" id="itemName" name="itemName" class="form-control">
                        <input type="hidden" id="itemID" name="itemID" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_update" type="button" onclick='updateMe()'><span class="fa fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>