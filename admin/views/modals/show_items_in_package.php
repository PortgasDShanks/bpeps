<div class="modal fade" id="itemInPackage" tabindex="-1" role="dialog" aria-labelledby="itemInPackageLabel" data-backdrop='static'>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fa fa-eye'></span> Item List </h4>
            </div>
            <div class="modal-body">
                  <div class='col-md-12' id='' style='margin-top: 10px'>
                    <table id='item_in_package' class="table" style='margin-top:10px;width: 100%;'>
                        <thead>
                            <tr>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>TYPE</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>ITEM</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>SIZE</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>COLOR</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>QUANTITY</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>