<style>
ul.dropdown-menu li {
    margin-left: 0;
    width: 100%;
    padding: 0;
    background: #444;
}
ul.dropdown-menu {
    padding: 1em;
    min-width: 200px;
    top: 101%;
}
</style>
<div class="content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>ITEM</h3>
        </div>
        <div class="card-body">
        	<div class='row'>
            	<div class='col-md-12'>
        		<button class='btn btn-sm btn-default pull-right' id='delete_e' onclick='deleteEquip()'><span class='fa fa-trash'></span> Delete</button>
        		<button class='btn btn-sm btn-default pull-right' onclick='addEquipModalShow()'><span class='fa fa-plus-circle'></span> Add</button>
        	</div>
        	
        	<div class='col-md-12' style='margin-top:10px;'>
                <table id='trackAndstrand' class="table" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>NAME</th>
                             <th style='background-color: rgb(34 45 50);color: #ffffff;'>CATEGORY</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>ADDED BY</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>DATE ADDED</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
<?php require 'modals/add_equipment_modal.php'; ?>
<?php require 'modals/update_items_modal.php'; ?>
<script type="text/javascript">
	$(document).ready( function(){
		equipments();
	});
    function viewDesigns(equipID){
        window.location = 'index.php?view=item&equipmentid='+equipID;
    }
	function addEquipModalShow(){
		$("#addequipment").modal('show');
	}
	function addEquip(){
		var equipment = $("#equipment").val();
		var itemCat = $("#itemCat").val();
		var action = 'add';
		$.post("ajax/equipments.php", {
			equipment: equipment,
			itemCat: itemCat,
			action: action
		}, function(data){
			$("#addequipment").modal('hide');
			if(data > 0){
				alertMe("fa fa-check-circle","All Good!","Equipment Successfully Added","success");
			}else{
				failed_query();
			}
			equipments();
		});
	}
	function deleteEquip(){
		var count_checked = $('input[name="checkbox_equip"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_e").prop("disabled", true);
        $("#delete_e").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/equipments.php", {
        	checked_array: count_checked,
        	action: action
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Equipment Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_e").prop("disabled", false);
        	$("#delete_e").html("<span class='fa fa-trash'></span> Delete");
        	equipments();
        });
	}
    function editRecord(id,equipN){
        $("#itemName").val(equipN);
        $("#itemID").val(id);
        $("#updateE").modal();
    }
    function updateMe(){
        var itemName = $("#itemName").val();
        var itemID = $("#itemID").val();
        var action = 'update';
        $("#btn_update").prop("disabled", true);
        $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Updating");
        $.post("ajax/equipments.php", {
            itemName: itemName,
            itemID: itemID,
            action: action
        }, function(data){
            $("#updateE").modal('hide');
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Equipment Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#btn_update").prop("disabled", false);
        	$("#btn_update").html("<span class='fa fa-trash'></span> Delete");
            equipments();
        });
    }
	function equipments(){
        $("#trackAndstrand").DataTable().destroy();
        $('#trackAndstrand').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/equipments.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_equip' value='"+row.equipID+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"equip_name"
            },
            {
                "data":"category"
            },
            {
                "data":"added_by"
            },
            {
                "data":"date_added"
            }
            
        ]   
        });
    }
</script>