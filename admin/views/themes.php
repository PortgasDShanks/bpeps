<style>

</style>
<div class="content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>THEMES</h3>
        </div>
        <div class="card-body">
        	<div class='row'>
            	<div class='col-md-12'>
        		<button class='btn btn-sm btn-default pull-right' id='delete_e' onclick='deleteTheme()'><span class='fa fa-trash'></span> Delete</button>
        		<button class='btn btn-sm btn-default pull-right' onclick='addEquipModalShow()'><span class='fa fa-plus-circle'></span> Add</button>
        	</div>
        	
        	<div class='col-md-12' style='margin-top:10px;'>
                <table id='themes' class="table" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>IMAGE</th>
                             <th style='background-color: rgb(34 45 50);color: #ffffff;'>NAME</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
<?php require 'modals/add_theme_modal.php'; ?>
<script type="text/javascript">
	$(document).ready( function(){
		themes();

        $("#themeAddForm").on('submit',(function(e) {
        e.preventDefault();
            $("#btn_add_t").prop("disabled", true);
            $("#btn_add_t").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/add_theme.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                $("#themeAdd").modal('hide');
                if(data == 1){
                    alertMe("fa fa-check-circle","All Good!","Item Design Successfully Added","success");
                }else{
                    alert("Error");
                }
                themes();
                $("#btn_add_t").prop("disabled", false);
                $("#btn_add_t").html("<span class='fa fa-check-circle'></span> Continue ");  

                
            },error: function(){
                alert("Error");
            }
                     
        });
            
        }));
	});
    $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
    function viewDesigns(equipID){
        window.location = 'index.php?view=item&equipmentid='+equipID;
    }
	function addEquipModalShow(){
		$("#themeAdd").modal('show');
	}
    function deleteTheme(){
        var count_checked = $('input[name="checkbox_theme"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_e").prop("disabled", true);
        $("#delete_e").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/delete_theme.php", {
        	checked_array: count_checked
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Theme Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_e").prop("disabled", false);
        	$("#delete_e").html("<span class='fa fa-trash'></span> Delete");
        	themes();
        });
    }
	function addEquip(){
		var equipment = $("#equipment").val();
		var itemCat = $("#itemCat").val();
		var action = 'add';
		$.post("ajax/equipments.php", {
			equipment: equipment,
			itemCat: itemCat,
			action: action
		}, function(data){
			$("#addequipment").modal('hide');
			if(data > 0){
				alertMe("fa fa-check-circle","All Good!","Equipment Successfully Added","success");
			}else{
				failed_query();
			}
			equipments();
		});
	}
	function themes(){
        $("#themes").DataTable().destroy();
        $('#themes').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/themes.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_theme' value='"+row.theme_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"theme_img"
            },
            {
                "data":"theme_name"
            }
            
        ]   
        });
    }
</script>