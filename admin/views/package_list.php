<style>
ul.dropdown-menu li {
    margin-left: 0;
    width: 100%;
    padding: 0;
    background: #444;
}
ul.dropdown-menu {
    padding: 1em;
    min-width: 200px;
    top: 101%;
}
</style>
<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>PACKAGE LISTS</h3>
        </div>
        <div class='row'>
            <div class='col-md-12'>
                 <button class='btn btn-sm btn-default pull-right' id='delete_in' onclick='deletePckg()'><span class='fa fa-trash'></span> Delete</button>
                <button class='btn btn-sm btn-default pull-right' onclick="window.location='index.php?view=add-package-list&id=0'"><span class='fa fa-plus-circle'></span> Add</button>
            </div>
            <div class='col-md-12' id='thumbnail_div' style='margin-top: 10px'>
                <table id='packages' class="table table-bordered" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>IMAGE</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>THEME</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>PRICE</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>DESC</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<script>
$(document).ready( function(){
    packages();
});
function addPackageDesignShow(){
    $("#addpackage").modal('show');
}
function updatePackage(id){
    window.location = "index.php?view=add-package-list&id="+id;
}
function deletePackage(id){
    $.post("ajax/deletePackge.php", {
        id: id
    }, function(data){
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Equipment Successfully Deleted","success");
        }else{
            failed_query();
        }
        $("#thumbnail_div").load(location.href + " #thumbnail_div");
    });
}
function deletePckg(){
		var count_checked = $('input[name="checkbox_equip"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        $("#delete_in").prop("disabled", true);
        $("#delete_in").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/delete_packages.php", {
        	checked_array: count_checked
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Package Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_in").prop("disabled", false);
        	$("#delete_in").html("<span class='fa fa-trash'></span> Delete");
        	packages();
        });
	}
function packages(){
        $("#packages").DataTable().destroy();
        $('#packages').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/packages.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_equip' value='"+row.package_header_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"package_img"
            },
            {
                "data":"theme_name"
            },
            {
                "data":"package_price"
            },
            {
                "data":"package_desc"
            }
            
        ]   
        });
    }
</script>