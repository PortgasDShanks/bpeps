<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>REPORTS</h3>
        </div>
        <div class='card-body'>
            <div class='row'>
            <div class='col-md-12'>
                <div id='chart'></div>
            </div>
            </div>
        </div> 
    </div>
</div>
</div>
<!-- <script src="../../assets/admin_assets/js/highcharts1.js"></script>
<script src="../../assets/admin_assets/js/highcharts-more.js"></script>
<script src="../../assets/admin_assets/js/exporting.js"></script> -->
<script>
$(document).ready( function(){
    bot_chart();
});
function bot_chart(){

  $.getJSON('ajax/chart.php', function(data){
    Highcharts.chart("chart", {
      chart: {
        type: 'column'
      },
      title: {
        text: "TOTAL SALES PER MONTH"
      },
      xAxis: {
        type: 'category',
        title: {
          text: "TOTAL SALES PER MONTH"
        }
      },
      yAxis: {
        title: {
          text: 'SALES'
        }
      },
      plotOptions: {
          column: {
            dataLabels: {
              enabled: true
            },
            enableMouseTracking: true
          },
          series: {
            connectNulls: true
          }
      },

      tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title}</span>: <b>{point.y:.0f}</b><br/>'
      },

     series: data['series']
    });

  });
}
</script>