<style>
    /* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>ITEM PRICING</h3>
        </div>
        <div class="card-body">
        	<div class='row' style='margin-top: 10px;'>
                <div class="tab">
                    <button class="tablinks active" onclick="openCity(event, 'equipment')">Equipments</button>
                    <button class="tablinks" onclick="openCity(event, 'venue')">Venues</button>
                    <button class="tablinks" onclick="openCity(event, 'inv_card')">Invitational Cards</button>
                    <button class="tablinks" onclick="openCity(event, 'giveaway')">Giveaways</button>
                    <button class="tablinks" onclick="openCity(event, 'cakes')">Cakes</button>
                </div>
                <div id="equipment" class="tabcontent" style="display: block;">
                    <div class='row'>
                        <div class="form-group" style='margin-top: 15px;'>
                            <div class="input-group">
                                <div class="input-group-addon">Category: </div>
                            <select id="itemCat" class="form-control" onchange='getItems()' style='width:50%'>
                                <?php echo getCategories();?>
                            </select>
                            </div>
                        </div>
                        
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='pricingTable' class="table" style='margin-top:10px;width: 100%;'>
                                <thead>
                                    <tr>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>ITEM NAME</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>AMOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="venue" class="tabcontent">
                    <div class='row'>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='vTable' class="table" style='margin-top:10px;width: 100%;'>
                                <thead>
                                    <tr>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>VENUE</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>AMOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="inv_card" class="tabcontent">
                    <div class='row'>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='icTable' class="table" style='margin-top:10px;width: 100%;'>
                                <thead>
                                    <tr>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>IMAGE</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>AMOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="giveaway" class="tabcontent">
                    <div class='row'>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='gaTable' class="table" style='margin-top:10px;width: 100%;'>
                                <thead>
                                    <tr>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>IMAGE</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>AMOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="cakes" class="tabcontent">
                    <div class='row'>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Cake: </div>
                                <select id="cakeID" class="form-control" onchange='getCakeSizes()'>
                                   <?php 
                                    $query = mysql_query("SELECT * FROM tbl_cake");
                                    while($row = mysql_fetch_array($query)){
                                        echo "<option value='".$row['cake_id']."'>".$row['cake_name']."</option>";
                                    }
                                   ?>
                                </select>
                            </div>
                        </div>
                    </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='cakeTable' class="table" style='margin-top:10px;width: 100%;'>
                                <thead>
                                    <tr>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>SIZE</th>
                                        <th style='background-color: rgb(34 45 50);color: #ffffff;'>AMOUNT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                </div>
               
                
            </div>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready( function(){
        getItems();
        venueList();
        invCardList();
        giveawayList();
        getCakeSizes();
    });
    function openCity(evt, cityName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    function getItems(){
        var itemCat = $("#itemCat").val();
        priceList(itemCat);
    }
    function setPriceCake(id){
        var price = $("#cakePrice"+id).val();

        $("#updatePriceCake"+id).prop("disabled", true);
        $("#updatePriceCake"+id).html("<span class='fa fa-spin fa-spinner'></span>");
        $.post("ajax/add_price_to_cake.php", {
            id: id,
            price: price
        }, function(data){
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Equipment Price Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#updatePriceCake"+id).prop("disabled", false);
            $("#updatePriceCake"+id).html("<span class='fa fa-pencil'></span>");
            var cakeID = $("#cakeID").val();
            cakeList(cakeID);

       
        });
    }
    function setPrice(id){
        var price = $("#designPrice"+id).val();

        $("#updatePrice"+id).prop("disabled", true);
        $("#updatePrice"+id).html("<span class='fa fa-spin fa-spinner'></span>");
        $.post("ajax/add_price_to_design.php", {
            id: id,
            price: price
        }, function(data){
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Equipment Price Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#updatePrice"+id).prop("disabled", false);
            $("#updatePrice"+id).html("<span class='fa fa-pencil'></span>");
            priceList(data);
        });
    }
    function setPriceVenue(id){
        var priceV = $("#venuePrice"+id).val();
        $("#updatePriceV"+id).prop("disabled", true);
        $("#updatePriceV"+id).html("<span class='fa fa-spin fa-spinner'></span>");
        $.post("ajax/add_price_to_venue.php", {
            id: id,
            priceV: priceV
        }, function(data){
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Venue Price Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#updatePriceV"+id).prop("disabled", false);
            $("#updatePriceV"+id).html("<span class='fa fa-pencil'></span>");
            venueList();
        });
    }
    function setPriceIC(id){
        var priceIC = $("#icPrice"+id).val();
        $("#updatePriceIC"+id).prop("disabled", true);
        $("#updatePriceIC"+id).html("<span class='fa fa-spin fa-spinner'></span>");
        $.post("ajax/add_price_to_ic.php", {
            id: id,
            priceIC: priceIC
        }, function(data){
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Invitational Card Price Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#updatePriceIC"+id).prop("disabled", false);
            $("#updatePriceIC"+id).html("<span class='fa fa-pencil'></span>");
            invCardList();
        });
    }
    function setPriceGA(id){
        var priceGA = $("#gaPrice"+id).val();
        $("#updatePriceGA"+id).prop("disabled", true);
        $("#updatePriceGA"+id).html("<span class='fa fa-spin fa-spinner'></span>");
        $.post("ajax/add_price_to_ga.php", {
            id: id,
            priceGA: priceGA
        }, function(data){
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Invitational Card Price Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#updatePriceIC"+id).prop("disabled", false);
            $("#updatePriceIC"+id).html("<span class='fa fa-pencil'></span>");
            giveawayList();
        });
    }
function priceList(itemCat){
    $("#pricingTable").DataTable().destroy();
    $('#pricingTable').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/pricing.php",
        "dataSrc":"data",
        "data":{
            itemCat: itemCat
        },
        "type":"POST"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"item_name"
        },
        {
            "data":"amount"
        }
        
    ]   
    });
}
function venueList(){
    $("#vTable").DataTable().destroy();
    $('#vTable').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/venue_pricing.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"item_name"
        },
        {
            "data":"amount"
        }
        
    ]   
    });
}
function invCardList(){
    $("#icTable").DataTable().destroy();
    $('#icTable').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/inv_cards.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"ic_card"
        },
        {
            "data":"amount"
        }
        
    ]   
    });
}
function getCakeSizes(){
    var cakeID = $("#cakeID").val();
    cakeList(cakeID);
}
function cakeList(cakeID){
    
    $("#cakeTable").DataTable().destroy();
    $('#cakeTable').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/cakeSizes.php",
        "dataSrc":"data",
        "data":{
            cakeID: cakeID
        },
        "type":"POST"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"size"
        },
        {
            "data":"amount"
        }
        
    ]   
    });
}
function giveawayList(){
    $("#gaTable").DataTable().destroy();
    $('#gaTable').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/giveaway.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"ic_card"
        },
        {
            "data":"amount"
        }
        
    ]   
    });
}
</script>