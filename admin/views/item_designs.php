<?php 
$equipID = $_GET['equipmentid'];
?>
<style>
ul.dropdown-menu li {
    margin-left: 0;
    width: 100%;
    padding: 0;
    background: #444;
}
ul.dropdown-menu {
    padding: 1em;
    min-width: 200px;
    top: 101%;
}
</style>
<div class="content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>ITEM DESIGNS of <i><?php echo getEquipmentName($equipID) ?></i></h3>
        </div>
        <div class="card-body">
        	<div class='row'>
            	<div class='col-md-12'>
                    <button class='btn btn-sm btn-default pull-right' id='delete_e' onclick='deleteDesign()'><span class='fa fa-trash'></span> Delete</button>
                    <button class='btn btn-sm btn-default pull-right' onclick='additemDesignShow()'><span class='fa fa-plus-circle'></span> Add</button>
                    <input type="hidden" id='equipID' value='<?php echo $equipID ?>'>
                </div>
                <div class='col-md-12' id='thumbnail_div' style='margin-top: 10px'>
                    <table id='itemDesigns' class="table" style='margin-top:10px;width: 100%;'>
                        <thead>
                            <tr>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>IMAGE</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>NAME</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>DESCRIPTION</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>COLOR</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
        	</div>
        </div>
    </div>
</div>
<?php require 'modals/add_item_design_modal.php'; ?>
<?php require 'modals/add_color_design_modal.php'; ?>
<?php require 'modals/show_color_modal.php'; ?>
<script type="text/javascript">
	$(document).ready( function(){
		itemDesigns();

        $("#designAdd").on('submit',(function(e) {
            var equipID = $("#equipID").val();
        e.preventDefault();
            $("#btn_add_d").prop("disabled", true);
            $("#btn_add_d").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
            $.ajax({
            url:"ajax/add_design.php",
            type: "POST",
            data:  new FormData(this),
            beforeSend: function(){},
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                $("#additemDes").modal('hide');
                if(data == 1){
                    alertMe("fa fa-check-circle","All Good!","Item Design Successfully Added","success");
                }else{
                    alert("Error");
                }
                itemDesigns();
                $("#btn_add_d").prop("disabled", false);
                $("#btn_add_d").html("<span class='fa fa-check-circle'></span> Continue ");  
            },error: function(){
                alert("Error");
            }
                     
        });
            
        }));
	});
    $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
	function additemDesignShow(){
		$("#additemDes").modal('show');
	}
	function addCat(){
		var catname = $("#catname").val();
		var action = 'add';
		$.post("ajax/item_category.php", {
			catname: catname,
			action: action
		}, function(data){
			$("#addcat").modal('hide');
			if(data > 0){
				alertMe("fa fa-check-circle","All Good!","Equipment Successfully Added","success");
			}else{
				failed_query();
			}
			category();
		});
	}
	function deleteDesign(){
        var count_checked = $('input[name="checkbox_equip"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
		
        $("#delete_e").prop("disabled", true);
        $("#delete_e").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/delete_design.php", {
        	checked_array: count_checked
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Equipment Design Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_e").prop("disabled", false);
        	$("#delete_e").html("<span class='fa fa-trash'></span> Delete");
            itemDesigns();
        });
	}
    function addColorDesign(){
        var colorDesign = $("#colorDesign").val();
       var id = $("#designID").val();
       $.post("ajax/addColorToDesign.php", {
        colorDesign: colorDesign,
        id: id
       }, function(data){
        $("#addColor").modal('hide');
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Design Color Successfully Added","success");
        }else{
            failed_query();
        }
        itemDesigns();
       });
    }
    function editRecord(id){
        $("#designID").val(id);
        $("#addColor").modal();
    }
    function showColors(id){
        $("#colorModal").modal();
        $.post("ajax/getDesignColors.php", {
            id: id
        }, function(data){
            $("#colorDiv").html(data);
        });
    }
    function deleteColor(id){
        $.post("ajax/deleteColorDesign.php", {
            id: id
        }, function(data){
            if(data > 0){
               alertMe("fa fa-check-circle","All Good!","Design Color Successfully Deleted","success"); 
            }else{
                failed_query();
            }
            window.location.reload();
        });
    }
    // function colorListDesign(){
    //     $("#itemDesigns").DataTable().destroy();
    //     $('#itemDesigns').dataTable({
    //         "processing":true
    //     });
    // }
	function itemDesigns(){
        var equipID = $("#equipID").val();
        $("#itemDesigns").DataTable().destroy();
        $('#itemDesigns').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/itemDesign.php",
            "dataSrc":"data",
            "data":{
                equipID: equipID
            },
            "type":"POST"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_equip' value='"+row.design_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"design_img"
            },
            {
                "data":"design_name"
            },
            {
                "data":"design_desc"
            },
            {
                "data":"color"
            }
            
        ]   
        });
    }
</script>