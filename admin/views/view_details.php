<?php 
$id = $_GET['id'];
$cat = $_GET['cat'];
$details = mysql_fetch_array(mysql_query("SELECT * FROM tbl_transactions WHERE trans_id = '$id'"));
?>
<style type="text/css">
     #map {
      height: 350px;;  /* The height is 400 pixels */
      width: 100%;  /* The width is the width of the web page */
     }
</style>
<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>View Details</h3>
        </div>
        <div class='row'>
            <?php 
                if($cat == 1) {  
                    $getDetails = mysql_fetch_array(mysql_query("SELECT * FROM `tbl_booking_cart` as c, tbl_transactions as t WHERE c.transaction_id = t.trans_id AND t.trans_id = '$id' AND item_type = ''"));

                    $is_package = $getDetails['is_package'];
                    $item_id = $getDetails['item_id'];
                    $item_type = $getDetails['item_type'];
                    $total_amount = $getDetails['total_amount'];

                    $sql = "SELECT * FROM tbl_package_header as p, tbl_package_header as h, tbl_themes as t, tbl_venue as v WHERE h.theme_id = t.theme_id AND p.package_header_id = '$item_id' AND p.package_header_id = h.package_header_id AND p.package_venue = v.venue_id";

                    $package_details = mysql_fetch_array(mysql_query($sql));
            ?>
            <div class="col-md-6 animated slideInLeft" style="padding: 10px">
                <img src="../assets/images/<?=$package_details['package_img']?>" style='width: 100%;border-radius: 5%'>
            </div>
            <div class="col-md-6" style="padding: 10px">
                <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    Package Details</a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <table class="table table-bordered">
                    <tbody style='text-align: center'>
                        <tr>
                            <td>Theme Name</td>
                            <td><?=$package_details['theme_name']?></td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td><?=$package_details['package_desc']?></td>
                        </tr>
                        <tr>
                            <td>Venue</td>
                            <td>
                                <?=$package_details['venue_name']?>
                                 <button class="btn btn-warning btn-sm" onclick='venuestatus(<?=$id?>,"e")'><span class="fa fa-edit"></span></button> 
                                 <button class="btn btn-success btn-sm" id="approve_v" onclick='venuestatus(<?=$id?>,"a")'><span class="fa fa-check-circle"></span></button>
                                 <button class="btn btn-danger btn-sm" onclick='venuestatus(<?=$id?>,"c")'><span class="fa fa-close"></span></button> 
                            </td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td><?=$package_details['package_price']?></td>
                        </tr>
                    </tbody>
                </table>
                  </div>
                </div>
              </div>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    Inclusions</a>
                  </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                    <label><h6>Equipments</h6></label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Quantity</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_equipment_designs as d WHERE h.package_item = d.design_id AND package_header_id = '$item_id' AND cat = 'I'");
                                while($equip_row = mysql_fetch_array($equip)){
                            ?>
                            <tr>
                                <td><?=$equip_row['design_name']?></td>
                                <td><?=$equip_row['item_qntty']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <label><h6>Invitational Cards</h6></label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Quantity</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_invitational_cards as d WHERE h.package_item = d.ic_id AND package_header_id = '$item_id' AND cat = 'IC'");
                                while($equip_row = mysql_fetch_array($equip)){
                            ?>
                            <tr>
                                <td><?=$equip_row['ic_name']?></td>
                                <td><?=$equip_row['item_qntty']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <label><h6>Giveaways</h6></label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Quantity</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_giveaways as d WHERE h.package_item = d.giveaway_id AND package_header_id = '$item_id' AND cat = 'G'");
                                while($equip_row = mysql_fetch_array($equip)){
                            ?>
                            <tr>
                                <td><?=$equip_row['giveaway_name']?></td>
                                <td><?=$equip_row['item_qntty']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <label><h6>Cakes</h6></label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Quantity</td>
                                <td>Size</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_cake as d, tbl_cake_sizes as s WHERE h.package_item = s.size_id AND package_header_id = '$item_id' AND cat = 'C' AND d.cake_id = s.cake_id");
                                while($equip_row = mysql_fetch_array($equip)){
                            ?>
                            <tr>
                                <td><?=$equip_row['cake_name']?></td>
                                <td><?=$equip_row['item_qntty']?></td>
                                <td><?=$equip_row['size']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <label><h6>freebies</h6></label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Quantity</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_freebies as d WHERE h.package_item = d.fb_id AND package_header_id = '$item_id' AND cat = 'F'");
                                while($equip_row = mysql_fetch_array($equip)){
                            ?>
                            <tr>
                                <td><?=$equip_row['fb_name']?></td>
                                <td><?=$equip_row['item_qntty']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <label><h6>Entertainments</h6></label>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td></td>
                                <td>Quantity</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $equip = mysql_query("SELECT * FROM tbl_package_details as h, tbl_entertainment as d WHERE h.package_item = d.ent_id AND package_header_id = '$item_id' AND cat = 'E'");
                                while($equip_row = mysql_fetch_array($equip)){
                            ?>
                            <tr>
                                <td><?=$equip_row['ent_type']?></td>
                                <td><?=$equip_row['item_qntty']?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            </div>

            <!-- ADDITIONAL -->
            <div class="col-md-12 animated slideInLeft" >
                <h4 style="text-align: center;">Additional Items</h4>
                <div class="row" style="padding: 10px">
                <?php 
                $additional = mysql_query("SELECT * FROM `tbl_booking_cart` as c, tbl_transactions as t WHERE c.transaction_id = t.trans_id  AND t.trans_id = '$id' AND item_type != ''");
                $count = mysql_num_rows($additional);
                if($count > 0){
                    while($addition_row = mysql_fetch_array($additional)){
                        $item_id = $addition_row['item_id'];
                        $query = ($addition_row['item_type'] == 'I')?"SELECT design_img,design_name,design_desc FROM tbl_equipment_designs WHERE design_id = '$item_id'":(($addition_row['item_type'] == 'IC')?"SELECT ic_image,ic_name FROM tbl_invitational_cards WHERE ic_id = '$item_id'":(($addition_row['item_type'] == 'G')?"SELECT giveaway_img,giveaway_name FROM tbl_giveaways WHERE giveaway_id = '$item_id'":"SELECT cake_img,cake_name,size FROM tbl_cake as c, tbl_cake_sizes as s WHERE c.cake_id = s.cake_id AND s.size_id = '$item_id'"));

                        $sql = mysql_fetch_array(mysql_query($query));

                        $desc = ($addition_row['item_type'] == 'I')?$sql[2]:"";
                        $size = ($addition_row['item_type'] == 'C')?$sql[2]:"";
                ?>
                    <div class="col-md-6" style="border: 1px solid #a2a2a26e;padding: 10px;">
                        <div class="row">
                            <div class="col-md-5">
                                <img src="../assets/images/<?=$sql[0]?>" style='width: 100%;border-radius: 5%;    height: 100px;object-fit: contain;'>
                            </div>
                            <div class="col-md-7" style="overflow: auto;">
                                <table class="table" style="width: 100%" cellpadding="5" cellspacing="5">
                                    <tbody style="text-align: center;">
                                        <tr>
                                            <td>Name:</td>
                                            <td><?=$sql[1]?></td>
                                        </tr>
                                        <tr>
                                            <td>Description:</td>
                                            <td><?=$desc?></td>
                                        </tr>
                                        <tr>
                                            <td>Price:</td>
                                            <td><?=$addition_row['item_price']?></td>
                                        </tr>
                                        <tr>
                                            <td>Size:</td>
                                            <td><?=$size?></td>
                                        </tr>
                                        <tr>
                                            <td>Quantity:</td>
                                            <td><?=$addition_row['item_qntty']?></td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td><?=$addition_row['item_price']*$addition_row['item_qntty']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                
                    <?php } ?>
                </div>
                <?php }else{ ?>
                    <div class="col-md-12">
                        <h5 style=""> No Additional Items Added. </h5>
                    </div>
                <?php } ?>
            </div>
            <?php }else{ ?>
                <div class="col-md-12" >
                    <div class="row" style="padding: 10px">
                    <?php 
                    $additional = mysql_query("SELECT * FROM `tbl_booking_cart` as c, tbl_transactions as t WHERE c.transaction_id = t.trans_id AND t.trans_id = '$id' AND item_type != ''");
                        while($addition_row = mysql_fetch_array($additional)){
                            $item_id = $addition_row['item_id'];
                            $query = ($addition_row['item_type'] == 'I')?"SELECT design_img,design_name,design_desc FROM tbl_equipment_designs WHERE design_id = '$item_id'":(($addition_row['item_type'] == 'IC')?"SELECT ic_image,ic_name FROM tbl_invitational_cards WHERE ic_id = '$item_id'":(($addition_row['item_type'] == 'G')?"SELECT giveaway_img,giveaway_name FROM tbl_giveaways WHERE giveaway_id = '$item_id'":"SELECT cake_img,cake_name,size FROM tbl_cake as c, tbl_cake_sizes as s WHERE c.cake_id = s.cake_id AND s.size_id = '$item_id'"));

                            $sql = mysql_fetch_array(mysql_query($query));

                            $desc = ($addition_row['item_type'] == 'I')?$sql[2]:"";
                            $size = ($addition_row['item_type'] == 'C')?$sql[2]:"";
                    ?>
                        <div class="col-md-6" style="border: 1px solid #a2a2a26e;padding: 10px;">
                            <div class="row">
                                <div class="col-md-5">
                                    <img src="../assets/images/<?=$sql[0]?>" style='width: 100%;border-radius: 5%;    height: 100px;object-fit: contain;'>
                                </div>
                                <div class="col-md-7">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>Name</td>
                                                <td><?=$sql[1]?></td>
                                            </tr>
                                            <tr>
                                                <td>Description</td>
                                                <td><?=$desc?></td>
                                            </tr>
                                            <tr>
                                                <td>Price</td>
                                                <td><?=$addition_row['item_price']?></td>
                                            </tr>
                                            <tr>
                                                <td>Size</td>
                                                <td><?=$size?></td>
                                            </tr>
                                            <tr>
                                                <td>Quantity</td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                           
                                                          </div>
                                                          <input type="number" id="itemqntty" value="<?=$addition_row['item_qntty']?>" readonly class="form-control">
                                                          <div class="input-group-append">
                                                           
                                                          </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Total</td>
                                                <td><?=$addition_row['item_price']*$addition_row['item_qntty']?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    
                        <?php } ?>
                    </div>
                    
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            Themes:
                                        </div>
                                          <select class="form-control" id="themeID" disabled>
                                            <option value="">&mdash; Please Choose &mdash; </option>
                                            <?php 
                                            $theme = mysql_query("SELECT * FROM tbl_themes");
                                            while($row_t = mysql_fetch_array($theme)){
                                                $selected = ($details['theme'] == $row_t['theme_id'])?"selected":"";
                                            ?>
                                            <option <?=$selected?> value="<?=$row_t['theme_id']?>"><?=$row_t['theme_name']?></option>
                                            <?php } ?>
                                          </select>
                                          <div class="input-group-addon">
                                            <span onclick='viewImage(<?=$details['theme']?>)' class="fa fa-eye" style='cursor: pointer;'></span>
                                          </div>
                                    </div>
                                </div>
                                <?php
                                if($details['venue'] == '' || $details['venue'] == 0){
                                ?>
                                <div class="col-md-6">
                                    <h4>Costumer Own Venue</h4>
                                    <div id="map">
                                        
                                    </div>
                                </div>
                                <?php } else { ?>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            Venue
                                        </div>
                                          <select class="form-control" id="venueID" disabled>
                                            <option value="">&mdash; Please Choose &mdash; </option>
                                            <?php 
                                            $v = mysql_query("SELECT * FROM tbl_venue");
                                            while($row_v = mysql_fetch_array($v)){
                                                $selected2 = ($details['venue'] == $row_v['venue_id'])?"selected":"";
                                            ?>
                                            <option <?=$selected2?> value="<?=$row_v['venue_id']?>"><?=$row_v['venue_name']?></option>
                                            <?php } ?>
                                          </select>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="view_img">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="fa fa-eye"></span> View </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="col-md-12">
            <img id="imgView" style="width: 100%">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Close </button>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">
    function venuestatus(id,action){
        if(action == 'e'){

        }else{
            update_venue_status(id,action);
        }
    }
    function update_venue_status(id,action){
        $("#action").prop("disabled", true);
        $("#action").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.post("ajax/update_venue_stat.php", {
            id: id,
            action: action
        }, function(data){
            alert(data);
            // if(data > 0){
            //     alert(data)
            // }
        });
    }
    function viewImage(themeID){
        if(themeID == ''){
            alert("No Theme Selected");
        }else{
            $.post("ajax/getThemeImg.php", {
                themeID: themeID
            }, function(data){
                $("#view_img").modal();
                $("#imgView").attr("src","../assets/images/"+data);
            })
        }
        
    }
    var position = [<?=$details['venue_lat']?>, <?=$details['venue_long']?>];

    function initialize() { 
        var latlng = new google.maps.LatLng(position[0], position[1]);
        var myOptions = {
            zoom: 20,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("map"), myOptions);

        marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Latitude:"+position[0]+" | Longitude:"+position[1]
        });

        google.maps.event.addListener(map, 'click', function(event) {
            var result = [event.latLng.lat(), event.latLng.lng()];
            transition(result);
        });
    }
</script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC232qKEVqI5x0scuj9UGEVUNdB98PiMX0&callback=initialize"></script>