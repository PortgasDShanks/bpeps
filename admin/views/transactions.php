<div class='row'>
<div class="col-md-12 content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>TRANSACTIONS</h3>
        </div>
        <div class='card-body'>
            <div class='row'>
                <div class='col-md-12' id='thumbnail_div' style='margin-top: 10px;overflow: auto;'>
                    <table id='transactions' class="table" style='margin-top:10px;width: 100%;'>
                        <thead>
                            <tr>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>CUSTOMER</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>BOOKING TYPE</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>BOOKING DATE</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>BOOKING TIME</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>TOTAL</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>MODE OF PAYMENT</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>VENUE STATUS</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>STATUS</th>
                                <th style='background-color: rgb(34 45 50);color: #ffffff;'>ACTION</th>
                               
                            </tr>
                        </thead>
                        <tbody style='text-align: center'>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
    </div>
</div>
</div>
<script>
$(document).ready(function(){
    transactions();
});
function viewDetails(id,cat){
    window.location = 'index.php?view=view-details&id='+id+'&cat='+cat;
}
function cancelBooking(id){
    var action = "cancel";
    $("#cancelBooking"+id).prop("disabled", true);
    $("#cancelBooking"+id).html("<span class='fa fa-spin fa-spinner'></span>");
    $.post("ajax/booking_status.php", {
        id: id,
        action: action
    }, function(data){
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Booking Successfully Cancelled","success");
        }else{
            failed_query();
        }
        $("#cancelBooking"+id).prop("disabled", false);
        $("#cancelBooking"+id).html("<span class='fa fa-ban'></span> Cancel");
        transactions();
    })
}
function approveBooking(id){
    var action = "approve";
    $("#approveBooking"+id).prop("disabled", true);
    $("#approveBooking"+id).html("<span class='fa fa-spin fa-spinner'></span>");
    $.post("ajax/booking_status.php", {
        id: id,
        action: action
    }, function(data){
        alert(data)
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Booking Successfully Approved","success");
        }else{
            failed_query();
        }
        $("#approveBooking"+id).prop("disabled", false);
        $("#approveBooking"+id).html("<span class='fa fa-ban'></span> Cancel");
        transactions();
    })
}
function finishBooking(id){
    var action = "finish";
    $("#finishBooking"+id).prop("disabled", true);
    $("#finishBooking"+id).html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/booking_status.php", {
        id: id,
        action: action
    }, function(data){
        if(data > 0){
            alertMe("fa fa-check-circle","All Good!","Booking Successfully Finished","success");
        }else{
            failed_query();
        }
        $("#finishBooking"+id).prop("disabled", false);
        $("#finishBooking"+id).html("<span class='fa fa-ban'></span> Cancel");
        transactions();
    })
}
function transactions(){
        $("#transactions").DataTable().destroy();
        $('#transactions').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/transactions.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_equip' value='"+row.package_header_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"view"
            },
            {
                "data":"customer"
            },
            {
                "data":"booking_type"
            },
            {
                "data":"booking_date"
            },
            {
                "data":"booking_time"
            },
            {
                "data":"total_amount"
            },
            {
                "data":"mode_of_payment"
            },
            {
                "data":"venue_stat"
            },
            {
                "data":"status"
            },
            {
                "data":"action"
            }
            
        ]   
        });
    }
</script>