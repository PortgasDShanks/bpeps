<style>
ul.dropdown-menu li {
    margin-left: 0;
    width: 100%;
    padding: 0;
    background: #444;
}
ul.dropdown-menu {
    padding: 1em;
    min-width: 200px;
    top: 101%;
}
</style>
<div class="content-top-2 card">
    <div class="agileinfo-cdr">
        <div class="card-header">
            <h3>VENUES</h3>
        </div>
        <div class="card-body">
        	<div class='row'>
            	<div class='col-md-12'>
                <button class='btn btn-sm btn-default pull-right' id='delete_e' onclick='deletefb()'><span class='fa fa-trash'></span> Delete</button>
        		<!-- <button class='btn btn-sm btn-default pull-right' id='delete_i' onclick='deleteEquip()'><span class='fa fa-trash'></span> Delete</button> -->
        		<button class='btn btn-sm btn-default pull-right' onclick='addvenueModalShow()'><span class='fa fa-plus-circle'></span> Add</button>
        	</div>
        	
        	<div class='col-md-12' style='margin-top:10px;'>
                <table id='category' class="table" style='margin-top:10px;width: 100%;'>
                    <thead>
                        <tr>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>#</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'></th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>Name</th>
                            <th style='background-color: rgb(34 45 50);color: #ffffff;'>Address</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
</div>
<?php require 'modals/add_venue_modal.php'; ?>
<?php require 'modals/update_venue_modal.php'; ?>
<script type="text/javascript">
	$(document).ready( function(){
		category();
	});
	function addvenueModalShow(){
		$("#addVenue").modal('show');
	}
	function addVenue(){
		var venueName = $("#venueName").val();
        var venueadd = $("#venueadd").val();
        $("#btn_add_v").prop("disabled", true);
        $("#btn_add_v").html("<span class='fa fa-spin fa-spinner'></span> Loading");
		$.post("ajax/venues.php", {
			venueName: venueName,
            venueadd: venueadd
		}, function(data){
			$("#addVenue").modal('hide');
			if(data > 0){
				alertMe("fa fa-check-circle","All Good!","Venue Successfully Added","success");
			}else{
				failed_query();
			}
            $("#btn_add_v").prop("disabled", false);
            $("#btn_add_v").html("<span class='fa fa-check-circle'></span> Continue");
			category();
		});
	}
    function deletefb(){
		var count_checked = $('input[name="checkbox_cat"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#delete_e").prop("disabled", true);
        $("#delete_e").html("<span class='fa fa-spin fa-spinner'></span> Deleting");
        $.post("ajax/update_delete_venues.php", {
        	checked_array: count_checked,
        	action: action
        }, function(data){
        	if(data > 0){
        		alertMe("fa fa-check-circle","All Good!","Venue Successfully Deleted","success");
        	}else{
        		failed_query();
        	}

        	$("#delete_e").prop("disabled", false);
        	$("#delete_e").html("<span class='fa fa-trash'></span> Delete");
        	category();
        });
	}
    function editRecord(id){
        $("#updateE").modal();
        $.post("ajax/getVenueData.php", {
            id: id
        }, function(data){
            var ent = JSON.parse(data);
            $("#venueIDU").val(id);
            $("#venueNameU").val(ent.venue_name);
            $("#venueAddU").val(ent.venue_address);
        });
    }

    function updateMe(){
        var venueNameU = $("#venueNameU").val();
        var venueAddU = $("#venueAddU").val();
        var venueIDU = $("#venueIDU").val();
        var action = 'update';
        $("#btn_update").prop("disabled", true);
        $("#btn_update").html("<span class='fa fa-spin fa-spinner'></span> Updating");
        $.post("ajax/update_delete_venues.php", {
            venueNameU: venueNameU,
            venueAddU: venueAddU,
            venueIDU: venueIDU,
            action: action
        }, function(data){
            $("#updateE").modal('hide');
            if(data > 0){
                alertMe("fa fa-check-circle","All Good!","Venue Successfully Updated","success");
            }else{
                failed_query();
            }
            $("#btn_update").prop("disabled", false);
        	$("#btn_update").html("<span class='fa fa-check-circle'></span> Continue");
            category();
        });
    }
	function category(){
        $("#category").DataTable().destroy();
        $('#category').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/venue.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_cat' value='"+row.venue_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"venue_name"
            },
            {
                "data":"address"
            }
            
        ]   
        });
    }
</script>