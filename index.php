<?php 
include('admin/main/configuration.php');
if(!isset($_SESSION['cust_user_id'])){
	$display = "display: none";
	$signin = "display: block";
	$viewMore = "data-toggle='modal' data-target='#signIN'";
}else{
	$display = "display: block";
	$signin = "display: none";
	$userID = $_SESSION['cust_user_id'];
	$viewMore = "onclick='viewMoreDetails()'";
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>BPEPS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<meta name="keywords" content="New Party Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<link href="assets/images/logo.png" rel="icon">
<link href="assets/images/logo.png" rel="apple-touch-icon">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
<link href="assets/customer_assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--// bootstrap-css -->
<!-- css -->
<link rel="stylesheet" href="assets/customer_assets/css/style.css" type="text/css" media="all" />
<link href="assets/customer_assets/css/jQuery.lightninBox.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="assets/customer_assets/css/flexslider.css" type="text/css" media="screen" property="" />
<!-- animation -->
<link href="assets/customer_assets/css/aos.css" rel="stylesheet" type="text/css" media="all" /><!-- //animation effects-css-->
<!-- //animation -->
<!--// css -->
<!-- font-awesome icons -->
<link href="assets/customer_assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons -->
<!-- font -->
<link href="//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Oswald:400,500,600,700" rel="stylesheet">
<link href="assets/admin_assets/css/dataTables.bootstrap4.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets/admin_assets/css/sweetalert.css">
<!-- //font -->
<script src="assets/customer_assets/js/jquery-2.2.3.min.js"></script>
<script src="assets/customer_assets/js/bootstrap.js"></script>
<script src="assets/admin_assets/js/sweetalert.min.js"></script>

</head>
<style>
	.book-appointment{
		width: 100%;
	}
	.book-appointment input[type="text"], .book-appointment input[type="email"], .book-appointment input[type="password"], .book-appointment textarea {
		width: 100%;
		color: #000;
		outline: none;
		font-size: 1em;
		padding: 10px;
		border: none;
		-webkit-appearance: none;
		margin: .3em 0em 1em;
		background: #fff;
		border: 1px solid #fff;
	}
	.book-appointment button[type=button] {
		display: block;
		color: #fff;
		margin: 0 auto;
		margin-top: 1em;
		padding: 10px 60px;
		font-size: .9em;
		/* width: 100%; */
		cursor: pointer;
		letter-spacing: 1px;
		border: none;
		outline: none;
		background: #f53275;
		transition: 0.5s all ease;
		-webkit-transition: 0.5s all ease;
		-moz-transition: 0.5s all ease;
		-o-transition: 0.5s all ease;
		-ms-transition: 0.5s all ease;
	}	
	
	.transaction-md {
		width: 70%;
		margin: 0px; 
	}
	/* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 30%;
  height: 500px;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
  color: #e708c1;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 70%;
  border-left: none;
  height: 500px;
  overflow: auto;
}
input[type="radio"], input[type="checkbox"] {
    margin: 4px 5px 0px;
    margin-top: 1px \9;
    line-height: normal;
}
.ulList {
    width: 1500px;
    height: 380px;
    margin: 0;
	padding: 0;
}
.liList {
    float: left;
}
.ulListThemes{
	width: 1500px;
    height: 250;
    margin: 0;
	padding: 0;
}
</style>
<body>
<!-- w3-banner -->
	<?php require 'customer/header.php'; ?>	
<!-- w3-banner -->
<!-- Transactions -->
	<?php 
		// if(isset($_SESSION['cust_user_id'])){
		// 	require 'customer/transactions.php'; 
		// }
	?>	
<!-- Transactions -->

<!-- services -->
	<?php require 'customer/services.php'; ?>							
<!-- //services -->

<!-- Packages -->
	<?php require 'customer/packages.php'; ?>								
<!-- Packages -->

<!-- Costumize -->
	<?php require 'customer/costumize.php'; ?>
<!-- Costumize -->

	<?php require 'customer/overall_ratings.php'; ?>
<!-- map -->
	<?php require 'customer/map.php'; ?>
<!-- //map -->
	<?php require 'customer/top_selected_package.php'; ?>
<!-- Sign Up -->
	<?php 
		if(!isset($_SESSION['cust_user_id'])){
			require 'customer/sign_up.php';
		}
	 ?>
<!-- Sign Up -->

<!-- footer -->
	<?php require 'customer/footer.php'; ?>
<!-- //footer -->

<!-- Modals -->
<?php require 'customer/signIn_modal.php'; ?>
<?php require 'customer/transactions_modal.php'; ?>
<?php require 'customer/final_step_modal.php'; ?>
<?php require 'customer/final_step_cost_modal.php'; ?>
<!-- Modals -->

<!-- js for portfolio lightbox -->
<script src="assets/customer_assets/js/jQuery.lightninBox.js"></script>
<script type="text/javascript">
	$(".lightninBox").lightninBox();
	$(".lightninBox2").lightninBox();
</script>
<!-- /js for portfolio lightbox -->
<!-- flexSlider -->
<script defer src="assets/customer_assets/js/jquery.flexslider.js"></script>
<script type="text/javascript">
	$(window).load(function(){
		$('.flexslider').flexslider({
		animation: "slide",
		start: function(slider){
			$('body').removeClass('loading');
		}
		});
	});
</script>
<!-- //flexSlider -->

<script type="text/javascript" src="assets/customer_assets/js/move-top.js"></script>
<script type="text/javascript" src="assets/customer_assets/js/easing.js"></script>

<script src="assets/customer_assets/js/jarallax.js"></script>
<script src="assets/customer_assets/js/SmoothScroll.min.js"></script>
<script src="assets/admin_assets/js/jquery.dataTables.js"></script>
<script src="assets/admin_assets/js/dataTables.bootstrap4.js"></script>

<script type="text/javascript">
	/* init Jarallax */
	$('.jarallax').jarallax({
		speed: 0.5,
		imgWidth: 1366,
		imgHeight: 768
	})
</script><!-- here stars scrolling icon -->
<script type="text/javascript">
	$(document).ready(function() {
							
		$().UItoTop({ easingType: 'easeOutQuart' });
							
		});
</script>
	<!-- //here ends scrolling icon -->
<!-- scrolling script -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script> 
<!-- //scrolling script -->
<!-- animation effects-js files-->
<script src="assets/customer_assets/js/aos.js"></script><!-- //animation effects-js-->
<script src="assets/customer_assets/js/aos1.js"></script><!-- //animation effects-js-->
<!-- animation effects-js files-->
<script>
	$(document).ready( function(){
		$("#termBtn").on("click", function(){
			$("#terms").slideToggle();
		});

	});
	function termsAgree(){
		if($("#termsandcondition").prop("checked")){
			$("#signBtn").attr("disabled", false);
		}else{
			$("#signBtn").attr("disabled", true);
		}
	}
	function addpackagebooking(){
		window.location = 'customer/index.php?view=packages';
	}
	function addcostumizebooking(){
		window.location = 'customer/index.php?view=costumize';
	}
	function getAnswer(){
		var addi = $("input[name='askCust']:checked").val();
		if(addi == 'Y'){
			$("#additionalDiv").css("display","block");
			$("#finaLStep").attr("disabled", false);
		}else{
			$("#additionalDiv").css("display","none");
			$("#finaLStep").attr("disabled", false);
		}
	}
	function getSelectedPckg(id){
		$('input.chk1').on('change', function() {
			$('input.chk1').not(this).prop('checked', false);
			if($("#chk1"+id).prop("checked")){
				$("#thmbnl"+id).css("background-color","#e709c163");
			}else{
				$("#thmbnl"+id).css("background-color","#fff");
			}
		});

	}
	function getSelectedthme(id){
		$('input.themechk1').on('change', function() {
			$('input.themechk1').not(this).prop('checked', false);
			if($("#themechk1"+id).prop("checked")){
				$("#themethmbnl"+id).css("background-color","#e709c163");
			}else{
				$("#themethmbnl"+id).css("background-color","#fff");
			}
		});
	}
	function getArrival(){
		var entID = $("#entID").val();
		var arrivalFrom = $("#arrivalFrom").val();
		var arrivato = $("#arrivato").val();
		$.post("admin/ajax/getAdditionalAMount.php", {
			entID: entID,
			arrivalFrom: arrivalFrom,
			arrivato: arrivato
		}, function(data){
			var split = data.split(",");
			$("#addAmnt").val(split[0]);
			$("#addAmntValSave").val(entID+","+arrivalFrom+","+arrivato+","+split[0]+","+split[1]);
		})
	}
	function checkPackageD(){
		$("#equip"+id).prop("checked", "checked");
	}
	function getTotalAmnt(id){
		var quantity = $("#des"+id).val();
		var price = $("#desPrice"+id).val();
		var tamnt = quantity * price;
		$("#DAmnt"+id).val(tamnt);
		$("#equipSaveVal"+id).val(id+"-"+quantity+"-"+tamnt+"-"+price);
	}
	function GAgetTotalAmnt(id){
		var quantity = $("#GA"+id).val();
		var price = $("#GPrice"+id).val();
		var tamnt = quantity * price;
		$("#gaAmnt"+id).val(tamnt);
		$("#GASaveVal"+id).val(id+"-"+quantity+"-"+tamnt+"-"+price);
	}
	function ICgetTotalAmnt(id){
		var quantity = $("#IC"+id).val();
		var price = $("#ICPrice"+id).val();
		var tamnt = quantity * price;
		$("#ICAmnt"+id).val(tamnt);
		$("#ICSaveVal"+id).val(id+"-"+quantity+"-"+tamnt+"-"+price);
	}
	function CakegetTotalAmnt(id){
		var quantity = $("#Cake"+id).val();
		var price = $("#CakePrice"+id).val();
		var tamnt = quantity * price;
		$("#CakeAmnt"+id).val(tamnt);
		$("#CakeSaveVal"+id).val(id+"-"+quantity+"-"+tamnt+"-"+price);
	}
	function getSizePrice(id){
		var cakeSize = $("#cakeSize"+id).val();
		$.post("admin/ajax/getCakeSizePrice.php", {
			cakeSize: cakeSize
		}, function(data){
			$("#CakePrice"+id).val(data);
		});
	}
	function finishBooking(){
		var checkedDataEquip = $('input[name="equipSaveVal"]').map(function() {
                            return this.value;
                        }).get();
    	arrayDataEquip = [];

		var checkedDataga = $('input[name="GASaveVal"]').map(function() {
                            return this.value;
                        }).get();
    	arrayDataga = [];

		var checkedDataic = $('input[name="ICSaveVal"]').map(function() {
                            return this.value;
                        }).get();
    	arrayDataic = [];

		var checkedDatacake = $('input[name="CakeSaveVal"]').map(function() {
                            return this.value;
                        }).get();
    	arrayDatacake = [];
		var pckg = $("input[name='chk1']:checked").val();
		var headerID = $("#headerID").val();
		var totalPaymnt = $("#totalPaymnt").val();
		var mop = $("#mop").val();
		var addi = $("input[name='askCust']:checked").val();
		$("#fnish").prop("disabled", true);
		$("#fnish").html("<span class='fa fa-spin fa-spinner'></span> Loading");
		$.post("admin/ajax/finish_booking.php", {
			headerID: headerID,
			pckg: pckg,
			arrayDataEquip: checkedDataEquip,
			arrayDataga: checkedDataga,
			arrayDataic: checkedDataic,
			arrayDatacake: checkedDatacake,
			mop: mop,
			totalPaymnt: totalPaymnt,
			addi: addi
		}, function(data){
			if(data > 0){
				$("#finalStep").modal('hide');
				alert("FINISHED");
				$("#cont_div").html("<h5 style='text-align: center;font-size: 17px;'>Booking Complete! Thank you for trusting your party to us , Please wait for the confirmation of your booking.</h5>")
			}else{
				alert("EROOR");
			}

			$("#fnish").prop("disabled", false);
			$("#fnish").html("<span class='fa fa-check-circle'></span> Finish");
		});
	}
	function checkAgreement(){
		if($("#aggremnt").prop("checked")){
			$("#fnish").attr("disabled", false);
		}else{
			$("#fnish").attr("disabled", true);
		}
	}
	function checkAgreementCost(){
		if($("#aggremntCost").prop("checked")){
			$("#fnishCost").attr("disabled", false);
		}else{
			$("#fnishCost").attr("disabled", true);
		}
	}
	function signUpClient(){
		var fname = $("#fname").val();
		var mname = $("#mname").val();
		var lname = $("#lname").val();
		var contact = $("#contactNo").val();
		var address = $("#address").val();
		var email = $("#emailadd").val();
		var un = $("#un").val();
		var password = $("#password").val();
		if($("#termsandcondition").prop("checked")){
			if(fname == '' || lname == '' || contact == '' || address == '' || email == '' || un == '' || password == ''){

				swal("Please check other fields if empty.");
			}else{
				$.post("admin/ajax/signupClient.php", {
					fname: fname,
					mname: mname,
					lname: lname,
					contact: contact,
					address: address,
					email: email,
					un: un,
					password: password
				}, function(data){
					if(data > 0){
						 swal({
				              title: "All Good!",
				              text: "Registration was successfully finished.",
				              type: "success"
				          }, function(){
				           	window.location.reload();
				          });
					}else{
						swal("Something went wrong, Please try again later.");
					}
				});
			}
			
		}else{
			swal("Please read and agree the terms and conditions");
		}
		
		
	}
	
	function signInUser(){
		var un_username = $("#un_username").val();
		var pw_password = $("#pw_password").val();
		$.post("admin/ajax/customer_auth.php", {
			un_username: un_username,
			pw_password: pw_password
		}, function(data){
			if(data > 0){
				swal({
	              title: "All Good!",
	              text: "Credentials matched",
	              type: "success"
	          	}, function(){
	           		window.location.reload();
	          	});
			}else{
				swal({
	              title: "Aw Snap!",
	              text: "Credentials not matched",
	              type: "warning"
	          	}, function(){
	           		//window.location.reload();
	          	});
			}
		});
	}
	function logout(){
		window.location = 'admin/ajax/cust_logout.php';
	}
	// function openCity(evt, cityName) {
	// 	var i, tabcontent, tablinks;
	// 	tabcontent = document.getElementsByClassName("tabcontent");
	// 	for (i = 0; i < tabcontent.length; i++) {
	// 		tabcontent[i].style.display = "none";
	// 	}
	// 	tablinks = document.getElementsByClassName("tablinks");
	// 	for (i = 0; i < tablinks.length; i++) {
	// 		tablinks[i].className = tablinks[i].className.replace(" active", "");
	// 	}
	// 	document.getElementById(cityName).style.display = "block";
	// 	evt.currentTarget.className += " active";
	// }

		// Get the element with id="defaultOpen" and click on it
		// document.getElementById("defaultOpen").click();
</script>
</body>	
</html>